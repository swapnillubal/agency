<?php
/**
 * Created by PhpStorm.
 * User: swapnil lubal
 * Date: 11/15/2016
 * Time: 2:32 PM
 */

include "config/config.php";
include "class/merc.php";
$obj = new merc();


?>
<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Regulations| MERC</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
    <!-- end: MAIN CSS -->
    <!-- start: CSS -->

    <link href="vendor/toastr/toastr.min.css" rel="stylesheet" media="screen">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style>
        td, th {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<div id="page-wrapper">

    <!-- Header -->
    <?php include("./sections/menubarservices.php"); ?>
    <!-- Banner -->
    

    <!-- Highlights -->

    <!-- Gigantic Heading -->
    <section class="wrapper style2">
        <div class="container">
            <header class="major">
                <h2>Regulations:</h2>

                <p></p>
            </header>
        </div>
    </section>

    <!-- Posts -->
    <section class="wrapper style1">
        <div class="container">
            <div class="row">
                <?php
                $num_rec_per_page = 15;
                $where = " ";
                $orderno = " ";


                if (isset($_GET["page"])) {
                    $page = $_GET["page"];
                } else {
                    $page = 1;
                };
                $start_from = ($page - 1) * $num_rec_per_page;

                $data = $obj->listregulations($start_from, $num_rec_per_page);
                //echo json_encode($data);
                ?>
                <table class="table table-bordered table-hover table-full-width"
                       id="sample_1">
                    <thead>
                    <tr>
                        <th> <b>Sr.No</b></th>
                        <th> <b>English File</b></th>
                        <th> <b>Marathi File</b></th>
                        <th> <b>Description</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($data)) {
                        $j = $start_from;
                        for ($i = 0;
                             $i < count($data);
                             $i++) {
                            $j++;
                            ?>
                            <tr><td>
                                    <input name="ord" id="ord" value="<?php echo $data[$i]['id']; ?>"
                                           hidden="hidden">
                                    <?php echo $j ?></td>
                                <td><a href="<?php echo $data[$i]['english_file']; ?>"><img
                                            src="images/download_icon.ico" width="50" height="50"></a></td>
                                <td><a href="<?php echo $data[$i]['marathi_file']; ?>"><img
                                            src="images/download_icon.ico" width="50" height="50"></a></td>
                                <td><?php echo $data[$i]['description']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    </section>

    <!-- CTA -->
    <section id="cta" class="wrapper style3">
        <div class="container">
            <header>
                <h2>Hit Counter :</h2>
                <h2> <?php include("./hitcounter/counter.php")?> </h2>
            </header>
        </div>
    </section>

    <!-- Footer -->
  

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap-tagsinput.js"></script>
<script src="vendor/modernizr/modernizr.js"></script>
<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="vendor/switchery/switchery.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS -->
<script src="assets/js/main.js"></script>
<script src="vendor/sweetalert/sweet-alert.min.js"></script>
<script src="vendor/toastr/toastr.min.js"></script>

<script>
    jQuery(document).ready(function () {
        Main.init();
    });

    $("#query").on('submit', (function (e) {
        var form = document.getElementById("query");
        e.preventDefault();
        $.ajax({
            url: "./adminapi/query/addquery.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Sent The Query", "MERC Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './regulations.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "MERC Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>
</body>
</html>