<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/1/19
 * Time: 7:15 PM
 */

include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Despatch</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>View Despatch</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <?php
                            $despatch_no = $_REQUEST['id'];

                            $data1=$obj->despatchdetails($despatch_no);

//echo json_encode($data1);







                                $prod_data = $obj->showproductlistdespatch($despatch_no);
//echo json_encode($prod_data);
                               foreach ($prod_data as $prd){
                                   $arr[] = $prd['COMMISSION_AMOUNT'];
                                   $arr1[] = $prd['DESPATCH_AMOUNT'];
                                   $total_arr = array_sum($arr);
                                   $total_arr1 = array_sum($arr1);
                               }

//echo $total_arr;




                            foreach ($data1 as $data){


                            ?>

                            <div class="x_content">

                                <form id="addPO" name="addPO" method="post"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="./adminapi/purchase_order/editpurchaseorder.php">
                                    <input id="q_id" class="form-control col-md-3 col-xs-3" name="q_id"  value="<?php echo $quotation_id; ?>" type="hidden">
                                    <input id="po_id" class="form-control col-md-3 col-xs-3" name="po_id"  value="<?php echo $po_id; ?>" type="hidden">
                                    <span class="section">Information</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="company" class="form-control col-md-3 col-xs-3" name="company" readonly type="text" value="<?php echo $data['company']; ?>">

                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="supplier" class="form-control col-md-3 col-xs-3" name="supplier" readonly type="text" value="<?php echo $data['supplier']; ?>">

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="invoiceno">Invoice No <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="invoiceno" class="form-control col-md-3 col-xs-3" name="invoiceno" readonly type="text" value="<?php echo $data['invoice_no']; ?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date1"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' name="date1" class="form-control" readonly value="<?php echo $data['invoice_date']; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="hawb">HAWB No
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="hawb" class="form-control col-md-3 col-xs-3" name="hawb" readonly type="text" value="<?php echo $data['hawb_no']; ?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" class="form-control" readonly value="<?php echo $data['hawb_date']; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="mawb">MAWB No
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="mawb" class="form-control col-md-3 col-xs-3" name="mawb" readonly type="text" value="<?php echo $data['mawb_no']; ?> " >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker3'>
                                            <input type='text' name="date3" class="form-control" readonly value="<?php echo $data['mawb_date']; ?> " />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="flight1">Flight 1
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="flight1" class="form-control col-md-3 col-xs-3" name="flight1" readonly type="text" value="<?php echo $data['flight1']; ?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                            <input type='text' name="date4" readonly class="form-control" <?php echo $data['flight1_date']; ?>/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="flight2">Flight 2
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="flight2" class="form-control col-md-3 col-xs-3" name="flight2" readonly type="text" value="<?php echo $data['flight2']; ?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker5'>
                                            <input type='text' name="date5" class="form-control" readonly value="<?php echo $data['flight2_date']; ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="upload">Upload
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <a href="<?php echo $data['upload']; ?>" target="_blank" class="btn "><i class="fa fa-print"></i>
                                                Document</a>

                                        </div>
                                    </div>


                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">PO No.</th>
                                        <th class="column-title">Item</th>
                                        <th class="column-title">Despatched Quantity </th>
                                        <th class="column-title">Net Value</th>
                                        <th class="column-title">Commission(%)</th>
                                        <th class="column-title">Commission Amount</th>
                                        <th class="column-title">Despatched Amount</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php


                                    //                                                                        echo json_encode($real_prod_data);

                                    foreach($prod_data as $item){


                                        ?>
                                        <tr>


                                            <td><?php echo $item['po_no']; ?></td>
                                            <td><?php echo $item['item']; ?></td>
                                            <td><?php echo number_format($item['quantity']); ?></td>
                                            <td><?php echo number_format($item['net_value']); ?></td>
                                            <td><?php echo $item['commission']; ?></td>
                                            <td><?php echo number_format($item['COMMISSION_AMOUNT']); ?></td>
                                            <td><?php echo number_format($item['DESPATCH_AMOUNT']); ?></td>

                                        </tr>

                                    <?php } ?>

                                    </tbody>

                                </table>
                            </div>
                            <h4>Total Commission Amount:</h4><?php echo number_format($total_arr); ?>
                            <h4>Total Invoice Despatch Amount:</h4><?php echo number_format($total_arr1); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>

                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addPO").on('submit', (function (e) {
        var form = document.getElementById("addPO");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/purchase_order/editpurchaseorder.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                //alert(data);

                /// ***issue***
                if (data != "success") {
                    toastr["success"]("Successfully Edited Purchase Order", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './purchase_order_list.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
    $(".bb1").click(function (e) {
        e.preventDefault();
        var val123 = $(this).data('id');

        var tr = $(this).closest('tr');

        if (val123 != "") {
            $.ajax({
                type: "POST",
                url: './adminapi/purchase_order/delete_productlist.php',
                data: ({idinfo: val123}),
                success: function (data) {
                    console.log(data);
                    if (data != "success") {
                        //alert(data);
                        tr.remove();
                        toastr["success"]("Successfully Deleted Product", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Product", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Product missing", "Agency Administrator");
        }
    });
</script>




<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker5').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker6').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker7').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker8').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker9').datetimepicker({
        format: 'YYYY-MM-DD'
    });


</script>
</body>
</html>

