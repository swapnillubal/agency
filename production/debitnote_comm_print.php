<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/1/19
 * Time: 6:49 PM
 */



include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();

$from = $_REQUEST['date1'];
$to = $_REQUEST['date2'];
$territory = $_REQUEST['territory'];
$current = $_REQUEST['rate'];

$date = new DateTime($from);
$date1 = new DateTime($to);

$from1=$date->format('d.m.y');
$to1=$date1->format('d.m.y');
$company=$_REQUEST['company'];
$principal=$_REQUEST['principal'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Distributor's Commission</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!---->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.css"/>
    <!-- -->
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Distributor's Commission Report</h3>
                        <h2>FROM:<?php echo $from; ?></h2>
                        <h2>TO:<?php echo $to; ?></h2>
                        <h2>Current:<?php echo $current; ?></h2>
                        <h2>Territory:<?php echo $territory; ?></h2>
                    </div>

                    <div class="title_right">
                        <div class="x_panel"> <div class="x_content">

                                <form id="addcompany" name="addcompany" method="post" action="./debitnote_print.php" class="form-horizontal form-label-left" >

                                    <span class="section">Debit Note</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="invoice_no">Invoice No.<span class="required">*</span>
                                        </label>
                                        <input id="from" name="from" type="hidden" value="<?php echo $from; ?>">
                                        <input id="to" name="to" type="hidden" value="<?php echo $to; ?>">
                                        <input id="territory" name="territory" type="hidden" value="<?php echo $territory; ?>">
                                        <input id="rate" name="rate" type="hidden" value="<?php echo $current; ?>">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="invoice_no" class="form-control col-md-7 col-xs-12" name="invoice_no" placeholder="Name" required="required" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-7">

                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <?php
                $company_name=$obj->listcompany();
                $principal_name=$obj->listprincipal();
                ?>
                <div class="clearfix"></div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Total Distributor's Commission</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">



                                <table id="example" class="display nowrap" width="100%">
                                    <?php
                                    $data1[] = $obj->distributorscomm($from,$to,$territory,$current);




                                    foreach ($data1 as $item){
                                        $principal12 = $item['supplier'];
                                        $company12 = $item['company'];
                                    $commission_Amount = $item['Commission_amount1'];
                                    }

                                    for($i=0;$i<count($data1);$i++) {
                                        $hi[] = ($data1[$i]['TOTAL_AMOUNT']);

                                    }
                                    //                                echo json_encode(array_sum($hi))


                                    ?>
                                    <thead>
                                    <tr>

                                        <th>Sr no. </th>
                                        <th>Distributor Name</th>
                                        <th>Branch </th>
                                        <th>Commission Amount</th>
                                        <th>Rate</th>
                                        <th>Commission Amount(INR)</th>
                                        <th>GST @18%</th>
                                        <th>Commission Amount(INR)</th>
                                        <th>TDS </th>
                                        <th>Net Amount </th>

                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php

//                                                        echo json_encode($data1);
                                    //                     $curr = json_encode($data1[0]['Currency']);
                                    foreach ($data1 as $item) {
                                        $curr = $item['Currency'];
                                    }
                                    $sum =  json_encode(array_sum($hi));
                                    $sum = number_format($sum,2);
                                    $i=0;

//                                    echo 'test'.!empty($data1);

                                    if($commission_Amount != 0){
                                    foreach ($data1 as $data){
                                        $i++;

                                        ?>


                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td>APEX</td>
                                            <td><?php echo $territory;?></td>
                                            <td><?php echo number_format($data['Commission_amount1']); ?></td>
                                            <td><?php echo $current ?></td>
                                            <td><?php echo number_format($data['comm_INR']); ?></td>
                                            <td><?php echo number_format($data['gst']); ?></td>
                                            <td><?php echo number_format($data['Commission_amount2']); ?></td>
                                            <td><?php echo number_format($data['tds']); ?></td>
                                            <td><?php echo number_format($data['net_amount']); ?></td>
                                        </tr>

                                    <?php }} ?>

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Detailed Distributor's Commission</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">



                                <table id="example1" class="display nowrap" width="100%">
                                    <?php
                                    $data1 = $obj->detaileddistributorscomm($from,$to,$territory);
//                                    echo json_encode($data1);
                                    foreach ($data1 as $item){
                                        $principal12 = $item['supplier'];
                                        $company12 = $item['company'];
                                    }

                                    for($i=0;$i<count($data1);$i++) {
                                        $hi[] = ($data1[$i]['TOTAL_AMOUNT']);
                                        $hi2[] = ($data1[$i]['TOTAL_COMMISSION']);
                                    }
                                    $totalamount = json_encode(array_sum($hi));
                                    $totalamount = (int)$totalamount;

                                    $totalcommission = json_encode(array_sum($hi2));
                                    $totalcommission = (int)$totalcommission;

                                    $totalamountINR = round($totalamount/$current);
                                    $totalcommissionINR = round($totalcommission/$current);

                                    ?>
                                    <thead>
                                    <tr>

                                        <th>Distributor</th>
                                        <th>Branch </th>
                                        <th>Month</th>
                                        <th>Orders</th>
                                        <th>User Name</th>
                                        <th>Invoice No</th>
                                        <th>Part Description</th>
                                        <th>Qty</th>
                                        <th>Unit Price</th>
                                        <th>Total</th>
                                        <th>Commission Rate</th>
                                        <th>Commission Amount(INR)</th>

                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php

                                    foreach ($data1 as $item) {
                                        $curr = $item['TOTAL_AMOUNT'];
                                    }
                                    $sum =  json_encode(array_sum($hi));
                                    $sum = number_format($sum,2);
                                    $i=0;
                                    foreach ($data1 as $data){
                                        $i++;
                                        $curr=array_unique($data['Currency']);
                                        $date = new DateTime($data['invoice_date']);
                                        $invoice_date = $date->format('d.m.y');
                                        $date1 = new DateTime($data['enq_date']);
                                        $enq_date = $date1->format('d.m.y');
                                        $i++;
                                        ?>


                                        <tr>


                                            <td>APEX</td>
                                            <td><?php echo $territory; ?></td>
                                            <td><?php echo $data['MONTH']; ?></td>
                                            <td><?php echo $data['po_no']; ?></td>
                                            <td><?php echo $data['customer']; ?></td>
                                            <td><?php echo $data['invoice_no']; ?></td>
                                            <td><?php echo $data['item']; ?></td>
                                            <td><?php echo $data['quantity']; ?></td>
                                            <td><?php echo $data['net_value']; ?></td>
                                            <td><?php echo number_format($data['TOTAL_AMOUNT']); ?></td>
                                            <td><?php echo $data['commission']; ?></td>
                                            <td><?php echo number_format($data['TOTAL_COMMISSION']); ?></td>

                                        </tr>

                                    <?php } ?>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Total</td>

                                        <td><?php echo number_format($totalamount);?></td>
                                        <td></td>
                                        <td><?php echo number_format($totalcommission);?></td>
                                    </tr>

                                    </tfoot>

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<script src="../vendors/moment/min/moment.min.js"></script>

<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>

<?php
$today = date("d.m.y");
$time = date("h:i:s");

?>














<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<!--<script>-->
<!--    $("#conso").on('submit', (function (e) {-->
<!--        var form = document.getElementById("conso");-->
<!--        e.preventDefault();-->
<!---->
<!--        $.ajax({-->
<!---->
<!--            url: "./adminapi/customer/add_customer.php",-->
<!--            type: "POST",-->
<!--            data: new FormData(this),-->
<!--            contentType: false,-->
<!--            cache: false,-->
<!--            processData: false,-->
<!--            success: function (data) {-->
<!--                console.log(data);-->
<!--                if (data == "success") {-->
<!--                    toastr["success"]("Successfully Added New Customer", "Agency Administrator");-->
<!--                    form.reset();-->
<!--                    setTimeout(function () {-->
<!--                        window.location = './customer_master.php';-->
<!--                    }, 2000);-->
<!--                } else {-->
<!--                    toastr["error"](data, "Agency Administrator");-->
<!--                }-->
<!--            },-->
<!--            error: function () {-->
<!--            }-->
<!--        });-->
<!--    }));-->
<!--</script>-->

<script>

    //$(document).ready(function() {
    //    // var date = today.getDate()+'.'+(today.getMonth()+1)+'.'+today.getFullYear();
    //    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //    var from = "<?php //echo $from; ?>//";
    //    var to = "<?php //echo $to; ?>//";
    //    var company = "<?php //echo $company; ?>//";
    //    var principal = "<?php //echo $principal; ?>//";
    //    $('#example').DataTable( {
    //        dom: 'Bfrtip',
    //        ajax: 'https://api.myjson.com/bins/qgcu',
    //        buttons: [
    //
    //            {
    //
    //                extend: 'pdfHtml5',
    //                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
    //
    //                // extend: 'pdfHtml5',
    //                // message: company+"                                   "+"CONSOLIDATED PERFORMANCE\n"+
    //                //     "From:"+from+" "+"To:"+to+"      "+principal+"   "+"Date:"+date+"  "+"Time:"+time,
    //                // title:'Consolidated Report'
    //
    //            }
    //        ]
    //    } );
    //} );

    //
    $(document).ready(function() {
        var from = "<?php echo $from1; ?>";
        var to = "<?php echo $to1; ?>";
        var company = "APEX PRECISION AGENCIES";
        var principal = "THK CO.,LTD";
        var company12 = "<?php echo $company12; ?>";
        var principal12 = "<?php echo $principal12; ?>";
        var time = "<?php echo $time; ?>";
        var date = "<?php echo $today; ?>";
        var current = "<?php echo $current; ?>";
        var sum = "<?php echo $sum; ?>";
        var totalamountINR = "<?php echo number_format($totalamountINR); ?>";
        var totalcommissionINR = "<?php echo $totalcommissionINR; ?>";
//alert(totalamountINR);
        $('#example').DataTable( {
            "scrollX": true,
            dom: 'Bfrtip',

            buttons:[
                {
                    extend: 'pdfHtml5',
                    footer: true,
                    title:'',

                    orientation: 'landscape',
                    filename:'Distributed_Commission',
                    message: company+""+"\n"+
                    "From: "+from+"   "+"To: "+to+"              "+principal+"                                 "+"Date:  "+date+"  "+"Time:  "+time,
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }



                },
                {
                    extend: 'excel',
                    filename:"Distributed_Commission",
                    title:"Distributed Commission for Period :       From:-"+from+"        To:-"+to

                },
                {
                    extend: 'csv'

                }
            ]
        } );
        $('#example1').DataTable( {
            dom: 'Bfrtip',
            "scrollX": true,

            buttons:[
                {
                    extend: 'pdfHtml5',
                    footer: true,
                    title:"Detailed Distributed Commission for Period :       From:-"+from+"        To:-"+to,

                    orientation: 'landscape',
                    filename:"Detailed_Distributed_Commission",
                    message: company+""+"\n"+
                    "From: "+from+"   "+"To: "+to+"              "+principal+"                                 "+"Date:  "+date+"  "+"Time:  "+time,
                    messageBottom: "Total in INR@"+current+"         "+totalamountINR,

                    exportOptions: {
                        stripHtml:false,
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                    }



                },
                {
                    extend: 'excel',
                    footer: true,
                    title:"Detailed Distributed Commission for Period :       From:-"+from+"        To:-"+to,
                    filename:"Detailed Distributed Commission",
                    messageBottom: "                                                       Total in INR@"+current+"                                                                                                           "+totalamountINR,
                    exportOptions: {
                        stripHtml:false,

                    }
                },
                {
                    extend: 'csv'

                }
            ]
        } );

    } );


    //
    //
    // $('#myDatepicker1').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });
    // $('#myDatepicker2').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });




</script>
</body>
</html>




