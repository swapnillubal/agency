<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/11/18
 * Time: 7:38 PM
 */
include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Despatch </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php
                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Despatch - Step 2</h3>

                    </div>


                </div>

                <div class="clearfix"></div>


                <?php



                $some = $_SESSION['po_id'];
                foreach ($some as $somedata){

                    $data1[] = $obj->swaplatestfunction($somedata);

                }
                //                echo json_encode($data1);
                ?>



                <div class="col-md-12 col-sm-12 col-xs-12">




                    <form id="adddespatch2" name="adddespatch2" method="post" action="./adminapi/despatch/add_despatch2.php" class="form-horizontal form-label-left" >


                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">

                                    <div class="x_title">
                                        <h2> Insert Products from PO</h2>
                                        <br><br>
                                        <div id="result"><h4>Total Number of Items Selected = <span id="selected">0</span></h4></div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">


                                        <?php

                                        ?>
                                        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Check box</th>
                                                <th>PO No</th>
                                                <th>Prdct Description</th>
                                                <th>Base Qty.</th>
                                                <th>Del. Quantity</th>
                                                <th>Actual Rates</th>
                                                <th>Dispatch Rate</th>
                                                <th>Qt.fr delivery</th>
                                                <th>Commission(%)</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php foreach ($data1 as $data){
                                                for($i=0;$i<sizeof($data);$i++) {
                                                    ?>
                                                    <tr>
                                                        <td><input type="checkbox" name="check[]" value="<?php echo $data[$i]['po_pl_id'];?>"></td>
                                                        <td><?php echo $data[$i]['po_no']; ?></td>
                                                        <td><?php echo $data[$i]['itemname']; ?></td>
                                                        <td><?php echo $data[$i]['quantity']; ?></td>
                                                        <td><?php echo $data[$i]['delivered_quantity']; ?></td>
                                                        <td><?php echo $data[$i]['net_value']; ?></td>
                                                        <td><input type="net_value" name="net_value[]"</td>


                                                        <?php
                                                        $base = (int)$data[$i]['quantity'];
                                                        $del = (int)$data[$i]['delivered_quantity'];
                                                        $limit =$base-$del?>
                                                        <td><input type="number" name="quant[]" max="<?php echo $limit;?>"</td>
                                                        <td><input type="text" name="commission[]" </td>


                                                    </tr>
                                                    <?
                                                }
                                            }?>


                                            </tbody>
                                        </table>


                                    </div>

                                </div>

                            </div>



                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-10">
                                <button type="submit" class="btn btn-primary">Cancel</button>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>


                    </form>


                </div>

            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>

    $(document).ready(function(){

        $("#loading").hide();


        $("#adddespatch2").on('submit', (function (e) {
            $("#loading").show();

            var form = document.getElementById("adddespatch2");
            e.preventDefault();

            $.ajax({

                url: "./adminapi/despatch/add_despatch2.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    if (data != "") {
                        toastr["success"]("Added New Despatch", "Agency Administrator");
                        form.reset();
                        setTimeout(function () {
                            window.location = './despatch.php';
                        }, 2000);
                    } else {
                        toastr["error"](data, "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        }));


    });



</script>
<script>
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker5').datetimepicker({
        format: 'YYYY-MM-DD'
    });




</script>

<script>
const selectedElm = document.getElementById('selected');

function showChecked(){
  selectedElm.innerHTML = document.querySelectorAll('input[name="check[]"]:checked').length;
}

document.querySelectorAll("input[name='check[]']").forEach(i=>{
 i.onclick = () => showChecked();
});


</script>




</body>
</html>
