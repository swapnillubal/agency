<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/7/18
 * Time: 10:19 PM
 */


include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <script src="../jquery-3.3.1.min.js"></script>
    <title>Indent</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Indent</h3>

                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addindent2">Add New</button>
                    </div>


                </div>

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="x_title">
                                    <h2>Sent Purchase Orders</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">


                                    <table id="datatable-responsive1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>PO No.</th>
                                            <th>PO Date</th>
                                            <th>Enq No.</th>
                                            <th>Principal qtn Ref</th>
                                            <th>Principal qtn Ref Date</th>
                                            <th>Supplier</th>
                                            <th>Manage</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $data1 = $obj->listingpo();
                                        //                                        echo json_encode($data1);
                                        foreach ($data1 as $data){  ?>
                                            <tr>
                                                <td><?php echo $data['customer']; ?></td>
                                                <td><?php echo $data['po_no']; ?></td>
                                                <td><?php echo $data['po_date']; ?></td>
                                                <td><?php echo $data['enq_no']; ?></td>
                                                <td><?php echo $data['principal_qtn_ref']; ?></td>
                                                <td><?php echo $data['principal_qtn_ref_date']; ?></td>
                                                <td><?php echo $data['supplier']; ?></td>
                                                <td><a href="#" data-id="<?php echo $data['po_no']; ?>" id="detail1" class="btn"><i class="fa fa-plus"></i>
                                                        Add </a>
                                                    <!--                                            <a href="#" id="bb1" class="btn bb1"><i class="fa fa-trash"></i>-->
                                                    <!--                                                DELETE</a>-->
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="x_title">
                                    <h2>Sent Indents</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">


                                    <table id="datatable-responsive2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Indent No</th>
                                            <th>Indent Date</th>
                                            <th>PO No.</th>
                                            <th>PO Date</th>
                                            <th>Banker</th>
                                            <th>Manage</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $signature_name=$obj->listsignature();
                                        $data1 = $obj->listingindent();
//                                        echo json_encode($data1);
                                        foreach ($data1 as $data){
                                            ?>
                                            <tr>
                                                <td><?php echo $data['customer']; ?></td>
                                                <td><?php echo $data['indent_no']; ?></td>
                                                <td><?php echo $data['indent_date']; ?></td>
                                                <td><?php echo $data['po_no']; ?></td>
                                                <td><?php echo $data['po_date']; ?></td>
                                                <td><?php echo $data['banker']; ?></td>

                                                <td>
                                                    <a href="#my_modal" data-toggle="modal" data-book-id="<?php echo $data['indent_id']; ?>" class="btn"><i class="fa fa-print"></i>
                                                        Print</a>
                                                    <!--                                            <a href="#" id="bb1" class="btn bb1"><i class="fa fa-trash"></i>-->
                                                    <!--                                                DELETE</a>-->
                                                </td>
                                            </tr>

                                        <?php } ?>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>


</div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>

            </footer>
            <!-- /footer content -->
        </div>
    </div>
    <!--modal starts here-->
    <div id="addindent2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Indent</h4>
                </div>
                <div class="modal-body">
                    <form id="addindent23" name="addindent23"  method="post" class="form-horizontal form-label-left" novalidate>

                        <span class="section">Purchase Order No.</span>


                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="po_no">Enter Purchase Order No.<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="po_no" class="form-control col-md-7 col-xs-12" name="po_no" required="required" type="text">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <!--Modal ends here-->
<!--Print modal starts here-->
<div id="my_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Indent</h4>
            </div>
            <div class="modal-body">
                <form target="__blank" action="./indent_print.php" id="print" name="print"  method="post" class="form-horizontal form-label-left" novalidate>


                    <input id="id" class="form-control col-md-7 col-xs-12" name="id" required="required" type="hidden">



                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="margin">Enter Margin<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="margin" class="form-control col-md-7 col-xs-12" name="margin" required="required" type="text">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="desc1">Description 1
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <input id="desc1" type="checkbox" class="col-md-3 col-xs-3" name="desc1" value="yes" checked >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="desc2">Description 2
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <input id="desc2" type="checkbox" class="col-md-3 col-xs-3" name="desc2" value="yes" checked >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="desc3">Description 3
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <input id="desc3" type="checkbox" class="col-md-3 col-xs-3" name="desc3"  value="yes" checked >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sign">Signature <span class="required">*</span>
                        </label>
                        <select class="col-md-6 col-sm-6 col-xs-6" name="sign">
                            <option value="">Select Signature </option>
                            <?php
                            foreach ($signature_name as $curr_name){
                                ?>
                                <option value="<?php echo $curr_name['sign_id']; ?>"><?php echo $curr_name['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>
<!--Print modal ends here-->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/toastr/toastr.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


<script>
    $('#datatable-responsive1').dataTable();
    $('#datatable-responsive2').dataTable();
</script>

    <script>
        $('#addindent23').submit(function() {
            var myBookId = document.getElementById("po_no").value;
            setTimeout(function () {
                window.location = './new_indent.php?id=' + myBookId;
            }, 1);
        });
    </script>
<script>
    $(document).on("click", "#detail1", function () {
        var myBookId = $(this).data('id');

        setTimeout(function () {

            window.location = './new_indent.php?id=' + myBookId;
            // window.location = './testpdf.php?id=' + myBookId;
        }, 1);
    });
</script>

<script>
    $('#my_modal').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('bookId');
        $(e.currentTarget).find('input[name="id"]').val(id);
    });
</script>
</body>
</html>

