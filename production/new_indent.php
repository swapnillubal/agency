<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/7/18
 * Time: 10:37 PM
 */



include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Indent Tables</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">


                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <?php
                                $po_no = $_REQUEST['id'];
//                                echo $po_no;

                                $latest_indent_no = $obj->getlatestindent();
                                $data=$obj->podetails($po_no);
//                                echo json_encode($data);

                                foreach ($data as $dataitem) {
                                    $po_id = $dataitem['po_id'];
                                    $cust_address1 = $dataitem['customer'];
                                }

                                  $cust_address=$obj->showaddress($cust_address1);

                                $prods=$obj->showproductlistpo($po_id);

//                                echo json_encode($prods);



//       11

                                foreach ($data as $things){
                                    $po_id = $things['po_id'];
                                }

                                ////////new stufff

                                foreach($data as $test){
                                   $prod_data = $obj->showproductlistpo($po_id);
//                                echo json_encode($prod_data);                                      ////////remaining part
                                    foreach ($prod_data as $something) {

                                        $so[] = $something['product_id'];
                                    }

                                    $comma_separated = implode(",", $so);

                                    $alltheprods = $obj->listproduct1($comma_separated);

//                                echo json_encode($alltheprods);

                                    foreach ($prod_data as $things){
                                        $quotation_pl_id=$things['quotation_pl_id'];
                                        $product_id=$things['product_id'];
                                        $quantity=$things['quantity'];
                                        $value1 =$things['net_value'];
                                        $delivery1 =$things['delivery'];

                                        $real_prod_data []= $obj->showproductdetailspurchaseorder($product_id,$quantity,$value1,$delivery1,$quotation_pl_id);
                                    }
                                }


                                //      11




                                foreach ($data as $data1){

                                    ?>


                                    <form id="addindent" name="addindent" method="post" enctype="multipart/form-data" action="./adminapi/indent/add_indent.php" class="form-horizontal form-label-left">
                                        <input id="po_id" class="form-control col-md-3 col-xs-3" value="<?php echo $po_id; ?>" name="po_id"  type="hidden">
                                        <span class="section"><h3>Send Indent</h3></span>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="indent_no">Indent No. <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                Last indent no:- <?php echo $latest_indent_no; ?>
                                                <input id="indent_no" class="form-control col-md-3 col-xs-3" name="indent_no" required="required" type="text" onkeyup="checkname();" ><span id="name_status"></span>
                                            </div>

                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                            </label>
                                            <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                                <input type='text' name="date1"  class="form-control" />
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="po_no">Purchase Order No. <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="po_no" class="form-control col-md-3 col-xs-3" name="po_no" value="<?php echo $data1['po_no']; ?>" required="required" type="text">
                                            </div>

                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                            </label>
                                            <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                                <input type='text' name="date2" value="<?php echo $data1['po_date']; ?>" class="form-control" />
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="customer">Customer <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="customer" class="form-control col-md-3 col-xs-3" name="customer"  required="required" type="text" value="<?php echo $data1['customer']; ?>" >                                            </div>
                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="cust_address">Address
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <textarea id="cust_address" name="cust_address" class="form-control col-md-3 col-xs-3"><?php echo $cust_address; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="prices">Prices <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="prices" class="form-control col-md-3 col-xs-3" name="prices"  required="required" type="text">                                            </div>
                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="shipping_address">Shipping Address <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <textarea id="shipping_address" required="required" name="shipping_address" class="form-control col-md-3 col-xs-3"></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="banker">Banker
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="banker" class="form-control col-md-3 col-xs-3" name="banker"   type="text">                                            </div>
                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="banker_address">Banker Address
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <textarea id="banker_address" name="banker_address" class="form-control col-md-3 col-xs-3"></textarea>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="payment">Payment <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="payment" class="form-control col-md-3 col-xs-3" name="payment"  required="required" type="text" value="<?php echo $data1['payment']; ?>" >
                                            </div>

                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="despatch">Despatch <span class="required">*</span>
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="despatch" class="form-control col-md-3 col-xs-3" name="despatch" value="<?php echo $data1['despatch']; ?>" required="required" type="text">
                                            </div>
                                        </div>




                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="cif">CIF Charges
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="cif" class="form-control col-md-7 col-xs-12" name="cif" type="text">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="agent">Agent
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="agent" class="form-control col-md-3 col-xs-3" name="agent" type="text">                                            </div>
                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="agent_address">Agent Address
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <textarea id="agent_address" name="agent_address" class="form-control col-md-3 col-xs-3"></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="note">Note
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <textarea id="note" name="note" class="form-control col-md-3 col-xs-3"></textarea>
                                            </div>

                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="start_p">Start Production?
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input type="radio" name="start_p" value="yes" > Yes<br>
                                                <input type="radio" name="start_p" value="no"> No<br>
                                            </div>

                                            <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                            </label>
                                            <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                                <input type='text' name="date4" class="form-control" />
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="upload">Upload
                                            </label>
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input id="report" class="form-control col-md-7 col-xs-12" name="report" type="file">
                                            </div>
                                        </div>



                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Value</th>
                                        <th class="column-title">Delivery</th>
<!--                                        <th class="column-title">Manage</th>-->

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php


                                    //                                    echo json_encode($real_prod_data);

                                    foreach($real_prod_data as $item){


                                        ?>
                                        <tr>


                                            <td>
                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $item[0]['pl_id']; ?>" type="hidden">
                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $item[0]['product_id']; ?>" type="hidden">
                                                <?php echo $item[0]['make']; ?></td>
                                            <td><?php echo $item[0]['item']; ?></td>
                                            <td><?php echo $item[0]['unit']; ?></td>
                                            <td><?php echo $item[0]['currency']; ?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" readonly class="form-control col-md-12 col-xs-12" name="quantity[]" value="<?php echo $item[0]['quantity']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" readonly class="form-control col-md-12 col-xs-12" name="value[]" value="<?php echo $item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" readonly class="form-control col-md-12 col-xs-12" name="delivery[]" value="<?php echo $item[0]['delivery']; ?>" type="text">
                                                </div>
                                            </td>
<!--                                            <td><a href="#" data-id="--><?php //echo $item[0]['pl_id']; ?><!--" id="bb1"-->
<!--                                                   class="btn bb1"><i class="fa fa-trash"></i> Delete</a>-->
<!--                                            </td>-->
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>

                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-10">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addindent").on('submit', (function (e) {
        var form = document.getElementById("addindent");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/indent/add_indent.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Added New Indent", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './indent.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>
<script>
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>

<script type="text/javascript">

    function checkname()
    {
        var indent_no=document.getElementById( "indent_no" ).value;

        if(indent_no)
        {
            $.ajax({
                type: 'post',
                url: './adminapi/indent/check_indent.php',
                data: {
                    indent_no:indent_no
                },
                success: function (response) {
                    $( '#name_status' ).html(response);
                    if(response=="OK")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        }
        else
        {
            $( '#name_status' ).html("");
            return false;
        }
    }
</script>


</body>
</html>



