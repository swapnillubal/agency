<?php
/**
 * Created by PhpStorm.
 * User: swapnil lubal
 * Date: 5/20/2017
 * Time: 1:42 PM
 */



include "config/config.php";
include "class/merc.php";
$obj = new merc();


?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Hearing Schedule| MERC</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
    <link href="vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
    <!-- end: MAIN CSS -->
    <!-- start: CSS -->

    <link href="vendor/toastr/toastr.min.css" rel="stylesheet" media="screen">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style>
        td, th {
            border: 1px solid black;
            text-align: left;
            padding: 2px;
            font-size:14px;
            font-weight:bold;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<div id="page-wrapper">

    <!-- Header -->
    <div id="header">

        <!-- Logo -->
        <div class="amblem"><img src="assets/images/logomerc.png" width="125px" height="125px"/></div>
        <h1><a href="index.php" id="logo">Electricity Ombudsman(Mumbai)</a></h1>
		<h1 class="logo_second"><span style="margin-left:-100px; "><i>Maharashtra Electricity</span><br/><span style="margin-left:277px;">Regulatory Commission</span></i></h1>

        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li><a href="index.php">Home</a>
                    <ul>
                        <li><a href="contactus.php">Contact Us</a></li>
                        <li><a href="profile.php">Profile</a></li>
                    </ul>
                </li>
                <li><a href="aboutus.php">About Us</a></li>
                <li><a href="http://mercombudsman.org.in/merctest/mercweb/">Online Representation</a></li>

                <li class="current"><a href="hearingschedule.php">Schedule Of Hearings</a></li>
                <li><a href="orders.php">Orders Passed </a></li>
                <li>
                    <a href="#">Services</a>
                    <ul>
                        <li><a href="extracts.php">Extracts</a></li>
                        <li><a href="regulations.php">Regulations</a></li>
                        <li><a href="rules.php">Rules</a></li>
                        <li><a href="consumer.php">Consumer Advocacy</a></li>
                        <li><a href="rti.php">RTI,2005</a></li>
                        <li><a href="http://mercombudsman.org.in/merctest/files/mh7ugsfir0o6gudjlyki.pdf">FAQ</a></li>

                    </ul>
                </li>
                <li><a href="photogallery.php">Photo Gallery</a></li>
                <li><a href="links.php">Important Links</a></li>

            </ul>
        </nav>

    </div>
    <!-- Banner -->
    

    <!-- Highlights -->

    <!-- Gigantic Heading -->
<br>
    

    <!-- Posts -->
    <section class="wrapper style1">
        <div class="container">
             <div style="overflow-x:auto;">
                <?php
                $num_rec_per_page = 15;
                $where = " ";
                $orderno = " ";


                if (isset($_GET["page"])) {
                    $page = $_GET["page"];
                } else {
                    $page = 1;
                };
                $start_from = ($page - 1) * $num_rec_per_page;

                $con = mysqli_connect('www.mercombudsman.org.in', 'swapnil2', 'swapnil2', 'mercadmin') or die("<br/>Could not connect to MySQL server");
                $data = array();

               //$sql = "SELECT * FROM hearing_schedule" ;
                  $sql = "SELECT * , DATE_FORMAT(dt_dateofHearing ,    '%d.%m.%Y') AS nicedate from hearing_schedule where dt_dateofHearing >= CURRENT_DATE ORDER BY hearing_schedule.dt_dateofHearing ASC";

                $result = mysqli_query($con, $sql);
                while ($row = mysqli_fetch_array($result, 1)) {
                    array_push($data, $row);
                }

                //$data = $obj->listrules($start_from, $num_rec_per_page);
               //                echo json_encode($data)."hello";

                ?>
                <table class="table table-bordered table-hover table-full-width"
                       id="sample_1">
                    <thead>
                    <tr>

                        <th bgcolor="#37c0fb"><b> Rep. No.</b></th>
                        <th bgcolor="#37c0fb"><b> Appellant</b></th>
                        <th bgcolor="#37c0fb"><b>Respondent</b></th>
                       <!-- <th bgcolor="#37c0fb"><b>Date of Receipt</b></th>-->
                        <th bgcolor="#37c0fb"><b>In Matter Of</b></th>
                        <!--<th bgcolor="#37c0fb"><b>Date Of Notice</b></th>-->
			<th bgcolor="#37c0fb"><b>Date Of Hearing</b></th>
                        <th bgcolor="#37c0fb"><b>Time Of Hearing</b></th>
                        <th bgcolor="#37c0fb"><b>Place</b></th>

                    </tr>
                    </thead>
                    <tbody>


                    <?php

                    if (!empty($data)) {
                        $j = $start_from;
                        for ($i = 0;
                             $i < count($data);
                             $i++) {
                            $j++;
                            ?>
                            <tr>

                                <td> <?php echo $data[$i]['vch_receiptNo']; ?></td>
                                <td> <?php echo $data[$i]['vch_appellantAddress']; ?></td>
                                <td> <?php echo $data[$i]['vch_resAddress']; ?></td>
								
							    <!--<td> <?php 
								   
							       // echo $data[$i]['dt_dateofReceipt'];
							         $date = date_create($data[$i]['dt_dateofReceipt']);
							        echo date_format($date,"F d Y");

							  	  ?>   
																   
								</td> -->

								
                                <td> <?php echo $data[$i]['vch_intheMatterOf']; ?></td>
                              <!--  <td> <?php
							        //echo $data[$i]['dt_dateofNotice'];
							         $date = date_create($data[$i]['dt_dateofNotice']);
							        echo date_format($date,"F d Y"); 


								?></td> -->
                                <td> <?php 
									
							       
						$date = date_create($data[$i]['dt_dateofHearing']);
							        echo date_format($date,"F d Y   l  ");
																
								?></td>
                                <td> <?php echo $data[$i]['vch_timeofHearing']; ?></td>
                                <td> <?php echo $data[$i]['vcrPlace']; ?></td>

                            </tr>


                            <?php
                        }
                    }
                    ?>


                    </tbody>
                </table>


            </div>
        </div>
    </section>

    <!-- CTA -->
    <section id="cta" class="wrapper style3">
        <div class="container">
            <header>
                <h2>Hit Counter :</h2>

                <h2> <?php include("./hitcounter/counter.php") ?> </h2>
            </header>
        </div>
    </section>

    <!-- Footer -->
    

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap-tagsinput.js"></script>
<script src="vendor/modernizr/modernizr.js"></script>
<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="vendor/switchery/switchery.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS -->
<script src="assets/js/main.js"></script>
<script src="vendor/sweetalert/sweet-alert.min.js"></script>
<script src="vendor/toastr/toastr.min.js"></script>

<script>
    jQuery(document).ready(function () {
        Main.init();
    });

    $("#query").on('submit', (function (e) {
        var form = document.getElementById("query");
        e.preventDefault();
        $.ajax({
            url: "./adminapi/query/addquery.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Sent The Query", "MERC Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './rules.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "MERC Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>
</body>
</html>