<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/12/18
 * Time: 2:03 AM
 */



include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <script src="../jquery-3.3.1.min.js"></script>
    <title>Customer Master</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Customer Master</h3>
                        <a href="add_customer.php"> <button type="button" class="btn btn-success">Add New</button> </a>
                        <!--                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addenq">Add New</button>-->
                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_title">
                                <h2>Customer List</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">


                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Person Name</th>
                                        <th>Telephone</th>
                                        <th>Manage</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $data1 = $obj->listcustomer();
                                    //                                                                        echo json_encode($data1);

                                    foreach ($data1 as $data){

                                        ?>
                                        <tr>



                                            <td>
                                                <input name="ord" id="ord" value="<?php echo $data['cust_id']; ?>"
                                                       hidden="hidden">
                                                <?php echo $data['name']; ?></td>
                                            <td> <?php echo $data['email']; ?></td>
                                            <td> <?php echo $data['person_name']; ?></td>
                                            <td><?php echo $data['Tel']; ?></td>


                                            <td>
                                                <a href="#" data-id="<?php echo $data['cust_id']; ?>" id="detail1"
                                                   class="btn"><i class="fa fa-search"></i> EDIT</a>
                                                <a href="#" data-id1="<?php echo $data['cust_id']; ?>" id="bb1" class="btn"><i class="fa fa-trash"></i>
                                                    DELETE</a>

                                            </td>
                                        </tr>
                                    <?php } ?>


                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<!--modal starts here-->

<!--Modal ends here-->
<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>



<script>

    $(document).on("click", "#bb1", function (e) {
        e.preventDefault();
        var myBookId1 = $(this).data('id1');
//        alert(myBookId1);


        var tr = $(this).closest('tr');
        if (myBookId1 != "") {

            $.ajax({
                type: "POST",
                url: './adminapi/customer/delete_customer.php',
                data: ({idinfo: myBookId1}),
                success: function (data) {
                    console.log(data);
                    if (data == "success") {

                        tr.remove();
                        toastr["success"]("Successfully Deleted Customer Entry", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Customer Entry", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Customer missing", "Agency  Administrator");
        }
    });
</script>


<script>
    $(document).on("click", "#detail1", function () {
        var myBookId = $(this).data('id');
        setTimeout(function () {
            window.location = './customer_masterdetails.php?id=' + myBookId;
        }, 1);
    });
</script>


</body>
</html>

