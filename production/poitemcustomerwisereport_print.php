<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/1/19
 * Time: 6:43 AM
 */


include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();


$from=$_REQUEST['date3'];
$to=$_REQUEST['date4'];
$customer=$_REQUEST['customer'];

$date = new DateTime($from);
$date1 = new DateTime($to);

$from1=$date->format('d.m.y');
$to1=$date1->format('d.m.y');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quoted Item Customer wise Report</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!---->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.css"/>
    <!-- -->
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Purchased Item Customerwise Report</h3>

                    </div>


                </div>


                <?php
                $company_name=$obj->listcompany();
                $principal_name=$obj->listprincipal();
                ?>
                <div class="clearfix"></div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Information Table</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">


                                <table id="example1" class="display nowrap" width="100%">
                                    <h2>Customer Name :- <?php echo $customer ;?></h2>
                                    <?php
                                    $data1 = $obj->poitemcustomerwiseprintreport($from,$to,$customer);
                                    //echo json_encode($data1);


                                    for($i=0;$i<count($data1);$i++) {
                                        $hi[] = ($data1[$i]['total_amount']);

                                    }
                                    //                                echo json_encode(array_sum($hi))


                                    ?>
                                    <thead>
                                    <tr>

                                        <th>Sr.No. </th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Net Value</th>
                                        <th>Territory</th>
                                        <th>Currency</th>

                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php

                                    //echo json_encode($data1);
                                    //                     $curr = json_encode($data1[0]['Currency']);
                                    foreach ($data1 as $item) {
                                        $curr = $item['Currency'];
                                    }
                                    $sum =  json_encode(array_sum($hi));
                                    $sum = number_format($sum);
                                    $i=0;
                                    foreach ($data1 as $data){
                                        $i++;
//echo  json_encode($result);
//                         echo          $curr=array_unique($data['Currency']);
                                        $date = new DateTime($data['q_date']);
                                        $q_date = $date->format('d.m.y');
                                        $date1 = new DateTime($data['enq_date']);
                                        $enq_date = $date1->format('d.m.y');

                                        ?>


                                        <tr>

                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $data['item']; ?></td>
                                            <td><?php echo $data['quantity']; ?></td>
                                            <td><?php echo number_format($data['net_value']); ?></td>
                                            <td><?php echo $data['Territory']; ?></td>
                                            <td><?php echo $data['Currency']; ?></td>

                                        </tr>

                                    <?php } ?>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<script src="../vendors/moment/min/moment.min.js"></script>

<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>

<?php
$today = date("d.m.y");
$time = date("h:i:s");

?>














<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<!--<script>-->
<!--    $("#conso").on('submit', (function (e) {-->
<!--        var form = document.getElementById("conso");-->
<!--        e.preventDefault();-->
<!---->
<!--        $.ajax({-->
<!---->
<!--            url: "./adminapi/customer/add_customer.php",-->
<!--            type: "POST",-->
<!--            data: new FormData(this),-->
<!--            contentType: false,-->
<!--            cache: false,-->
<!--            processData: false,-->
<!--            success: function (data) {-->
<!--                console.log(data);-->
<!--                if (data == "success") {-->
<!--                    toastr["success"]("Successfully Added New Customer", "Agency Administrator");-->
<!--                    form.reset();-->
<!--                    setTimeout(function () {-->
<!--                        window.location = './customer_master.php';-->
<!--                    }, 2000);-->
<!--                } else {-->
<!--                    toastr["error"](data, "Agency Administrator");-->
<!--                }-->
<!--            },-->
<!--            error: function () {-->
<!--            }-->
<!--        });-->
<!--    }));-->
<!--</script>-->

<script>

    //$(document).ready(function() {
    //    // var date = today.getDate()+'.'+(today.getMonth()+1)+'.'+today.getFullYear();
    //    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //    var from = "<?php //echo $from; ?>//";
    //    var to = "<?php //echo $to; ?>//";
    //    var company = "<?php //echo $company; ?>//";
    //    var principal = "<?php //echo $principal; ?>//";
    //    $('#example').DataTable( {
    //        dom: 'Bfrtip',
    //        ajax: 'https://api.myjson.com/bins/qgcu',
    //        buttons: [
    //
    //            {
    //
    //                extend: 'pdfHtml5',
    //                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
    //
    //                // extend: 'pdfHtml5',
    //                // message: company+"                                   "+"CONSOLIDATED PERFORMANCE\n"+
    //                //     "From:"+from+" "+"To:"+to+"      "+principal+"   "+"Date:"+date+"  "+"Time:"+time,
    //                // title:'Consolidated Report'
    //
    //            }
    //        ]
    //    } );
    //} );

    //
    $(document).ready(function() {
        var from = "<?php echo $from1; ?>";
        var to = "<?php echo $to1; ?>";
        var company = "<?php echo 'APEX PRECISION AGENCIES'; ?>";
        var principal = "<?php echo 'THK CO.,LTD'; ?>";
        var customer = "<?php echo $customer ; ?>";
        var time = "<?php echo $time; ?>";
        var date = "<?php echo $today; ?>";
        var curr = "<?php echo $curr; ?>";
        var sum = "<?php echo $sum; ?>";

        $('#example1').DataTable( {
            dom: 'Bfrtip',

            buttons:[
                {
                    extend: 'pdfHtml5',
                    title:'',

                    orientation: 'landscape',
                    filename:'poitem_customerwise_report',
                    message: company+""+"               Customer Name :- "+customer+"\n"+
                    "From: "+from+"   "+"To: "+to+"              "+principal+"                                 "+"Date:  "+date+"  "+"Time:  "+time,
                    messageBottom:'Net:-             '+curr,
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                    customize : function(doc) {
                        // doc.defaultStyle.alignment = 'center';
                        doc.styles.tableHeader.alignment = 'left';
                        doc.content[1].table.widths = [ '5%', '25%', '15%','15%','10%','15%'];
                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        italics: true,
                                        text: 'dataTables',
                                        fontSize: 18,
                                        margin: [8,0]
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 14,
                                        text: 'Ordered Items Customerwise Report'
                                    }
                                ],
                                margin: 20
                            }
                        });

                    }


                },
                {
                    extend: 'excel'

                },
                {
                    extend: 'csv'

                }
            ]
        } );

    } );


    //
    //
    // $('#myDatepicker1').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });
    // $('#myDatepicker2').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });




</script>
</body>
</html>




