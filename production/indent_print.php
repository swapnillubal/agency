<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/7/18
 * Time: 6:54 PM
 */


include "config/config.php";
include "class/agency.php";

$obj = new agency();

$desc1=$_POST['desc1'];
$desc2=$_POST['desc2'];
$desc3=$_POST['desc3'];


//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);



require('./fpdf/fpdf.php');

$id=$_POST['id'];
$margin=$_POST['margin'];
$sign=$_POST['sign'];


if(isset($sign)) {
    $signdetails = $obj->signdetails($sign);

    foreach ($signdetails as $sign_items) {
        $sign_image = $sign_items['upload'];
    }
}
$data1=$obj->indentprint($id);
//echo json_encode($data1);

$prod_data_indent = $obj->indentprodprint($id);
//echo json_encode($prod_data_indent);




$merge_array=array();
for($i=0;$i<count($prod_data_indent);$i++){
    $merge_array[$i] =
        ['product_id'=>$prod_data_indent[$i]['product_id'],
            'quantity'=>$prod_data_indent[$i]['quantity'],
            'value'=>$prod_data_indent[$i]['net_value'],
            'delivery'=>$prod_data_indent[$i]['delivery'],

        ];
}

//echo json_encode($merge_array);

foreach ($merge_array as $things){
    $product_id = $things['product_id'];
    $quantity = $things['quantity'];
    $value = $things['value'];
    $delivery = $things['delivery'];

    $real_prod_data[] = $obj->printtheprodsquotation($product_id,$quantity, $value,$delivery );

}

//echo json_encode($real_prod_data);


//echo json_encode($data1);
foreach ($data1 as $item) {
//    $company = $item['company'];
//    $supplier = $item['supplier'];

    $enq_no= $item['enq_no'];
    $q_no= $item['quotation_no'];
    $q_date= $item['q_date'];
    $enq_date= $item['enq_date'];
    $prod[] = $item['product_list'];
    $shipping_address = $item['shipping_address'];
    $po_no= $item['po_no'];
    $po_date= $item['po_date'];
    $prices= $item['prices'];
    $banker= $item['banker'];
    $despatch= $item['despatch'];
    $cif= $item['cif_charges'];
    $payment_term= $item['payment'];
    $agent= $item['agent'];
    $agent_address= $item['agent_address'];
    $indent_no= $item['indent_no'];
    $indent_date= $item['indent_date'];
    $start_p= $item['start_production'];
}
//echo $po_no;
//echo json_encode($data1);
//
//echo $customer;
//echo $supplier;
//echo $company;


$podetails = $obj->podetails1($po_no);

//echo json_encode($podetails);

foreach ($podetails as $podetails2) {
    $princi_quotation_number = $podetails2['principal_qtn_ref'];
    $q_no=$podetails2['quotation_no'];
    $comp_name= $podetails2['company'];
    $supplier = $podetails2['supplier'];
    $customer = $podetails2['customer'];
    $enq_no = $podetails2['enq_no'];
}


//echo $company;
$data2=$obj->listcompany1($comp_name);
//echo json_encode($data2);
foreach ($data2 as $item2) {
    $comp_address = $item2['address'];
    $comp_email = $item2['email'];
    $comp_website= $item2['website'];
    $comp_Tel= $item2['telephone'];
    $comp_fax= $item2['fax'];

}



$data3=$obj->listprincipal1($supplier);
//echo json_encode($data3);

foreach ($data3 as $item3) {
    $principal_name=$item3['name'];
    $net=$item3['Currency'];
    $principal_address=$item3['address'];
    $principal_department=$item3['Department'];
}
$data4=$obj->listcustomer1($customer);
foreach ($data4 as $item4) {
    $customer_name=$item4['name'];
    $customer_address=$item4['Address'];
    $customer_iec=$item4['cust_iec'];
}
//echo json_encode($data4);

class PDF extends FPDF{




    // Page header
    public function Header()
    {



        // Logo
        $this->SetFont('Arial','B',20);

//Cell(width , height ,text,border ,end line , [align])
        $this->Cell(40 ,5,'',0,0);
        $this->Cell(150 ,8,$GLOBALS['comp_name'],0,1);
//$pdf->Cell(20 ,8,'',1,0);
        $this->SetFont('Arial','',10);
        $this->Cell(20 ,5,'',0,0);

        $this->Cell(190 ,5,$GLOBALS['comp_address'],0,1);

        if(strlen($GLOBALS['comp_email'])<30) {

            $this->Cell(60, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }else{
            $this->Cell(42, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }
        if(strlen($GLOBALS['comp_website'])<30){
            $this->Cell(60 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');
        }else{
            $this->Cell(40 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');

        }
        $strlen = strlen($GLOBALS['comp_fax']);
        if(0<$strlen && $strlen<20) {
            $this->Cell(42, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'] . ', Fax: ' . $GLOBALS['comp_fax'], 0, 1);
        }
        else{
            $this->Cell(62, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'], 0, 1);
        }
        $this->Cell(180 ,5,'',0,1);

        $x = $this->GetX();
        $y = $this->GetY();

        $this->SetXY($x + 149, $y);

        $this->Line($x, $y, $x+180,$y);

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}







$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,25);

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->Cell(10 ,5,'',0,1);

$pdf->SetFont('Arial','BU',18);
$pdf->Cell(180 ,5,'INDENT',0,1,'C');

$pdf->SetXY($x + 149, $y+5);


$pdf->Cell(10 ,5,'',0,1);



$pdf->SetFont('Arial','',9);
$pdf->Cell(180 ,5,'',0,1);

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);
$pdf->Cell(80 ,5,'The Manager',0,1);
$pdf->Cell(80 ,5,$principal_department,0,1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(180 ,5,$principal_name,0,1);
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(90,5,$principal_address,0);
$x = $pdf->GetX();
//$y = $pdf->GetY();
$pdf->SetXY($x , $y);
//$pdf->Ln(0);
$date = new DateTime($indent_date);
$pdf->Cell(120 ,5,'',0,0);
$pdf->Cell(60 ,5,'Our Ref: '.$indent_no,0,1,'L');
$pdf->Cell(120 ,5,'',0,0);

$pdf->Cell(60 ,5,'Date: '.$date->format('d.m.Y'),0,1,'L');
$pdf->Cell(120 ,5,'',0,0);

$pdf->Cell(60 ,5,'Your Ref: '.$princi_quotation_number,0,1,'L');

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y+15);

//


$pdf->Cell(180 ,5,'Dear Sir, ',0,1,'L');
$pdf->Cell(180 ,5,'Kindly register our order as follows:-  ',0,1,'L');
//$pdf->Cell(180 ,5,'',0,1);
$pdf->Ln(2);
$pdf->SetFont('Arial','BU',9);
$pdf->Cell(180 ,5,'Customer :-  ',0,1,'L');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(120 ,5,$customer,0,0,'L');
$pdf->SetFont('Arial','BU',10);

//$pdf->Cell(110 ,5,'',1,0,'R');
$pdf->Cell(80 ,5,'Shipping Address:-  ',0,1,'L');
$pdf->SetFont('Arial','B',9);
$pdf->SetFont('Arial','',9);


$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x + 120, $y);
$pdf->MultiCell(60,5,$shipping_address,0,'L');
$pdf->Cell(80 ,5,'',0,1);

$pdf->SetXY($x + 149, $y);
$pdf->Ln(0);

$pdf->MultiCell(100,5,$customer_address,0,'L');
$pdf->MultiCell(90,5,"IEC No.:-  ".$customer_iec,0,'L');
//$pdf->MultiCell(90,5,'',0);
$pdf->Ln(2);
$date2 = new DateTime($po_date);

$pdf->Cell(90 ,5,"Customer's PO REF:-  ".$po_no.'   Date:-  '.$date2->format('d.m.Y'),0,1,'L');

if(!empty($banker)){
$pdf->Cell(90 ,5,"Banker :-  ".$banker,0,1,'L');
}
$pdf->MultiCell(90,5,'',0);
$pdf->SetFont('Arial','B',9);

$pdf->Cell(10 ,5,'No.',1,0);
$pdf->Cell(80 ,5,'Product Description',1,0);
$pdf->Cell(20 ,5,'Quantity',1,0);
$pdf->Cell(10 ,5,'Unit',1,0);
$pdf->Cell(30 ,5,'Rate Per Unit',1,0);
$pdf->Cell(30 ,5,'Delivery',1,1);

$pdf->SetFont('Arial','',9);

$i=0;

//
foreach ($real_prod_data as $prod1){
    $i++;
    if(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2']) && !empty($prod1[0]['description3']) && $desc1=="yes" && $desc2=="yes" && $desc3=="yes") {
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-20);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80, 5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n". $string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-20);
        $pdf->MultiCell(20, 5, $prod1[0]['quantity']."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-20);
        $pdf->MultiCell(10, 5, $prod1[0]['unit']."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-20);
        $pdf->MultiCell(30, 5, number_format($prod1[0]['value'])."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-20);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n\n\n", 1);
    }
    elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description3']) && $desc1=="yes" && $desc3=="yes" ){
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-15);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80, 5,$prod1[0]['description1']."\n". $string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-15);
        $pdf->MultiCell(20, 5, $prod1[0]['quantity']."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-15);
        $pdf->MultiCell(10, 5, $prod1[0]['unit']."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-15);
        $pdf->MultiCell(30, 5, number_format($prod1[0]['value'])."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-15);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n\n", 1);
    }
    elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2']) && $desc1=="yes" && $desc2=="yes" ) {

        $y = $pdf->GetY();

        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->SetXY($x+10, $y-15);
        $pdf->MultiCell(80 ,5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n".$string,1);

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetXY($x+90 , $y-15);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+110 , $y-15);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n\n",1);
//
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+120 , $y-15);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+150 , $y-15);
        $pdf->MultiCell(30 ,5,$prod1[0]['delivery']." \n\n\n",1);



    }
    elseif(!empty($prod1[0]['description1']) && $desc1=="yes" ){
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-10);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80 ,5,$prod1[0]['description1']."\n".$string."\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-10);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-10);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-10);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-10);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n", 1);
    }elseif(!empty($prod1[0]['description2']) && $desc2=="yes" ){
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-10);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80 ,5,$prod1[0]['description2']."\n".$string."\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-10);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-10);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-10);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-10);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n", 1);
    }
    elseif(!empty($prod1[0]['description3']) && $desc3=="yes" ){
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-10);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80 ,5,$prod1[0]['description3']."\n".$string."\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-10);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-10);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-10);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-10);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n", 1);
    }
    else{
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->Cell(10, 5, $i, 1);
        $pdf->Cell(80, 5, $prod1[0]['item'], 1, 0);
        $pdf->Cell(20, 5, $prod1[0]['quantity'], 1, 0);
        $pdf->Cell(10, 5, $prod1[0]['unit'], 1, 0);
        $pdf->Cell(30, 5, number_format($prod1[0]['value']), 1, 0);
        $pdf->Cell(30, 5, $prod1[0]['delivery'], 1, 1);

    }
    $total[] = $prod1[0]['quantity']*$prod1[0]['value'];
//
//foreach ($real_prod_data as $prod1){
//    $i++;
//    $pdf->Cell(10 ,5,$i,1,0);
//    $pdf->Cell(80 ,5,$prod1[0]['item'],1,0);
//    $pdf->Cell(10 ,5,$prod1[0]['unit'],1,0);
//    $pdf->Cell(20 ,5,$prod1[0]['quantity'],1,0);
//    $pdf->Cell(30 ,5,number_format($prod1[0]['value']),1,0);
//    $pdf->Cell(30 ,5,$prod1[0]['delivery'],1,1);
//    $total[] = $prod1[0]['quantity']*$prod1[0]['value'];
//
}
$grand_total = array_sum($total);
//$grand_total = 121629400;
$words_val = $obj->convert_number_to_words($grand_total);
//$words_val =  $f->format($grandtotal);
if(strlen($words_val)>70)
{
    $pdf->SetFont('Arial','B',7);

}else{

    $pdf->SetFont('Arial','B',9);
}
$pdf->Cell(180 ,5,'In Words:-'.$words_val.'  NET: '.$net.'  Grand total: '.number_format($grand_total),1,1,'R');
$pdf->SetFont('Arial','',9);

for($i=0;$i<$margin;$i++)
{

    $pdf->Cell(180 ,5,'',0,1);
}

$pdf->Cell(180 ,5,'',0,1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->Line($x, $y, $x+180,$y);
//$pdf->Cell(180 ,5,'',0,1);
$pdf->Ln(2);
$pdf->Cell(90 ,5,'Prices                    :'.$prices,0,0,'L');
$pdf->Cell(90 ,5,'Mode of Despatch: '.$despatch,0,1,'L');
$pdf->Cell(90 ,5,'CIF charges          :'.$cif,0,0,'L');
$pdf->Cell(90 ,5,'Payment Terms    :'.$payment_term,0,1,'L');
$pdf->Cell(90 ,5,'Forwading Agent :'.$agent,0,1,'L');
$pdf->Cell(90 ,5,'Address :',0,1,'L');
$pdf->MultiCell(70,5,$agent_address,0,'L');
$pdf->Ln(5);

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->Line($x, $y, $x+180,$y);
if($start_p == 'yes'){
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(90 ,5,'Please Start Production On Our Guarantee',0,1,'L');
    $pdf->SetFont('Arial','',9);
}
$pdf->Cell(180 ,5,'',0,1);

$pdf->Cell(90 ,5,'For '.$comp_name,0,1,'L');
if(isset($sign_image)) {
    $x = $pdf->GetX();
    $y = $pdf->GetY();
    $pdf->Image($sign_image, $x, $y, 55, 22);
}
$pdf->Cell(180 ,20,'',0,1);

$pdf->Cell(90 ,5,'Authorised Signatory',0,1,'L');
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->Line($x, $y, $x+180,$y);




$pdf->Output();



?>
