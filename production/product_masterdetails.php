<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/8/18
 * Time: 12:09 AM
 */

include "config/config.php";
include "section/checksession.php";
include "class/agency.php";

$obj = new agency();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Product Tables</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Edit Product</h3>

                    </div>


                </div>


                <?php
                $currency_name=$obj->listcurrency();
                $make_name=$obj->listmake();
                $subgroup_name=$obj->listsubgroup();

                ?>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
                                <?php
                                $productid = $_REQUEST['id'];

                                $data1 = $obj->productdetails($productid);
//                                echo json_encode($data1);
                                foreach ($data1 as $data ){ ?>
                                    <form id="editproduct1" name="editproduct1"  method="post" class="form-horizontal form-label-left" novalidate>
                                        <input name="pid" id="pid" value="<?php echo $data['product_id']; ?>"
                                               hidden="hidden">
                                        <span class="section">Information</span>


                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="make">Make <span class="required">*</span>
                                            </label>
                                            <select class="col-md-6 col-sm-6 col-xs-12" name="make">
                                                <?php
                                                foreach ($make_name as $m_name){
                                                    ?>
                                                    <option
                                                        <?php if ($m_name['make_name'] == $data['make'] ) echo 'selected' ;?>
                                                            value="<?php echo $m_name['make_name']; ?>"><?php echo $m_name['make_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Item">Item <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Item" class="form-control col-md-7 col-xs-12" name="Item" required="required" type="text" value="<?php echo $data['item'];?>" >
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Unit">Unit <span class="required">*</span>
                                            </label>
                                            <select class="col-md-6 col-sm-6 col-xs-12" name="Unit">
                                                <option value="">Select Unit</option>

                                                <option
                                                        <?php if ($data['unit']=='KG' ) echo 'selected' ;?>

                                                            value="KG">KG</option>
                                                <option
                                                        <?php if ($data['unit']=='NO' ) echo 'selected' ;?>
                                                value="NO">NO</option>
                                                <option
                                                    <?php if ($data['unit']=='SET' ) echo 'selected' ;?>
                                                        value="SET">SET</option>

                                            </select>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subgroup">Subgroup<span class="required">*</span>
                                            </label>
                                            <select class="col-md-6 col-sm-6 col-xs-12" name="subgroup">
                                                <option value="">Select Subgroup</option>
                                                <?php


                                                    foreach ($subgroup_name as $subg_name){
                                                        ?>
                                                        <option
                                                            <?php if ($subg_name['subg_name'] == $data['subgroup'] ) echo 'selected' ;?>
                                                                value="<?php echo $subg_name['subg_name']; ?>"><?php echo $subg_name['subg_name']; ?></option>
                                                    <?php } ?>

                                            </select>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="currency">Currency <span class="required">*</span>
                                            </label>
                                            <select class="col-md-6 col-sm-6 col-xs-12" name="currency">
                                                <option value="">Select Currency</option>
                                                <?php
                                                foreach ($currency_name as $curr_name){
                                                    ?>
                                                    <option
                                                        <?php if ($curr_name['CURRENCY_CODE'] == $data['currency'] ) echo 'selected' ;?>
                                                            value="<?php echo $curr_name['CURRENCY_CODE']; ?>"><?php echo $curr_name['CURRENCY_CODE'].'-'.$curr_name['CURRENCY_NAME'].'-'.$curr_name['CURRENCY_SYMBOL']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description1">Description1 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Description1"  name="Description1" class="form-control col-md-7 col-xs-12"><?php echo $data['description1'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description2">Description2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Description2" name="Description2" class="form-control col-md-7 col-xs-12"><?php echo $data['description2'];?> </textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description3">Description3 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Description3" name="Description3" class="form-control col-md-7 col-xs-12"><?php echo $data['description3'];?> </textarea>
                                            </div>
                                        </div>



                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<script src="../vendors/toastr/toastr.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>
<!--<script src="../jquery-3.3.1.min.js"></script>-->


<script>
    $("#editproduct1").on('submit', (function (e) {
        var form = document.getElementById("editproduct1");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/product/edit_product.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data != "success") {
                    toastr["success"]("Successfully Updated product", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './product_master.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>

</body>
</html>


