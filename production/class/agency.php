<?php


date_default_timezone_set("Asia/Kolkata");

class agency
{
    public $con;

    public function __construct()
    {

        $this->con = mysqli_connect(HOST, USER, PASS, DB) or die("<br/>Could not connect to MySQL server");


        return $this->con;
    }

    public function __destruct()
    {
        mysqli_close($this->con);
    }

    //Admin Login
    public function checkadmin($username, $password)
    {
        $con = $this->__construct();
        $sql = "SELECT * FROM admin where username = '$username' AND password = '$password'";
        $execute = mysqli_query($con, $sql);
        $rows = mysqli_num_rows($execute);
        if ($rows == 1) {
            return true;
        } else {
            return false;
        }
    }
    //
    public function changepassword($username, $newpassword, $oldpassword)
    {


        $con = $this->__construct();
        $sql = "UPDATE `admin` SET `password` = '$newpassword' WHERE `username` = '$username' AND `password` = '$oldpassword'";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function adduser($username, $password)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO admin(username, password,created) VALUES ('$username', '$password', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function convert_number_to_words($number) {
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    public function addcompany($name,$Address,$email,$telephone,$fax,$website,$Code,$gst,$pan,$iec,$Purchase_Order,$Indent_Order,$Quotation_No,$Order_Confirmation,$Despatch,$Enquiry,$Action_Paper,$Drawing,$bank,$ac_no,$ifsc)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO company(name, address, email,telephone,fax, website, code,gst, pan, iec,bank_details,ac_no,ifsc, purchase_order, intent_order, quotation_no, order_confirmation, despatch, enquiry, actionpaper, drawing,created) VALUES ('$name', '$Address', '$email','$telephone','$fax','$website','$Code','$gst','$pan','$iec','$bank','$ac_no','$ifsc','$Purchase_Order', '$Indent_Order', '$Quotation_No', '$Order_Confirmation', '$Despatch', '$Enquiry', '$Action_Paper', '$Drawing',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addsign($name,$report)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO signature(name, upload,created) VALUES ('$name', '$report',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addterritory($code,$name,$gst,$address)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO territory(territory_full,name, territory_gst , address, visibility, created) VALUES ('$code','$name','$gst','$address','Enable',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addlc($customer,$company,$supplier,$lc_no,$lc_date,$value,$expiry,$po_no,$po_date)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO `lc_details`( `lc_no`, `lc_date`, `expiry_date`, `value`, `po_no`, `po_date`, `company`, `customer`, `supplier`, `created`) VALUES ('$lc_no', '$lc_date', '$expiry','$value','$po_no','$po_date', '$company', '$customer', '$supplier',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function editlc($lc_id,$customer,$company,$supplier,$lc_no,$lc_date,$value,$expiry,$po_no,$po_date)
    {


        $con = $this->__construct();
        $sql = "UPDATE `lc_details` SET `lc_no`='$lc_no',`lc_date`='$lc_date',`expiry_date`='$expiry',`value`='$value',`po_no`='$po_no',`po_date`='$po_date',`company`='$company',`customer`='$customer',`supplier`='$supplier' WHERE `lc_id` = '$lc_id'";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addenq($enq_no,$date,$customer,$company,$supplier,$subgroup,$remark,$actual_date,$product_list)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO new_enq(enq_no,date, customer,company,supplier,subgroup,remark,actual_date,product_list,created) VALUES ('$enq_no','$date','$customer','$company','$supplier','$subgroup','$remark','$actual_date','$product_list', NOW())";
        $execute = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($execute) {
            return $last_id;
        } else {
            return false;
        }
    }
    public function addprincipal($name,$Address,$Tel,$Fax,$confirm_email,$Name_person,$Department,$Banker_Details,$Currency,$Note_Q,$Note_OC,$Conditions_OC,$Country,$Decimal)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO principal(name, address, Tel, Fax, email, Name_person, Department, Banker_Details, Currency, Note_Q,Note_OC,Conditions_OC,Country,Decimal1,Created) VALUES ('$name', '$Address', '$Tel', '$Fax', '$confirm_email', '$Name_person', '$Department', '$Banker_Details', '$Currency', '$Note_Q','$Note_OC','$Conditions_OC', '$Country','$Decimal', NOW())";

        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function editproduct($id,$make,$Item,$Unit,$subgroup,$currency,$Description1,$Description2,$Description3)
    {
        $con = $this->__construct();
        $sql = "UPDATE `product` SET `make`='$make',`item`='$Item',`unit`='$Unit',`subgroup`='$subgroup',`currency`='$currency',`description1`='$Description1',`description2`='$Description2',`description3`='$Description3',visibility = 'Enable' WHERE `product_id`='$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }
    public function editmake($id,$make)
    {
        $con = $this->__construct();
        $sql = "UPDATE `make_master` SET `make_name`='$make',`visibility`='Enable' WHERE `make_id`='$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }
    public function editcurrency($currency_id,$code,$name,$symbol)
    {
        $con = $this->__construct();
        $sql = "UPDATE `currency_master` SET `CURRENCY_CODE`='$code',`CURRENCY_NAME`='$name',`CURRENCY_SYMBOL`='$symbol',`visibility`='Enable' WHERE `CURRENCY_ID`='$currency_id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function editsubg($subgid,$subg)
    {
        $con = $this->__construct();
        $sql = "UPDATE `subgroup_master` SET `subg_name`='$subg',`visibility`='Enable' WHERE `subg_id`='$subgid'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function addproduct($make,$Item,$Unit,$subgroup,$currency,$Description1,$Description2,$Description3)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO `product`(`make`, `item`, `unit`, `subgroup`, `currency`, `description1`, `description2`, `description3`, `visibility`, `Created`) VALUES ('$make','$Item', '$Unit', '$subgroup','$currency','$Description1', '$Description2', '$Description3','1', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addmake($make)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO `make_master`(`make_name`,`Created`) VALUES ('$make', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addsubg($subg)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO `subgroup_master`(`subg_name`,`Created`) VALUES ('$subg', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addproductdetails($enq_id, $product_id,$product_quantity)
    {
//        echo "product_id".$product_id;
        $con = $this->__construct();
        $sql = "INSERT INTO enq_pl(enq_id, product_id, quantity,created) VALUES ('$enq_id','$product_id','$product_quantity', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addproductdetails1($enq_id, $quotation_id,$product_id,$product_quantity,$product_value,$product_delivery)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO quotation_pl(enq_id, quotation_id, product_id, quantity, net_value, delivery, created) VALUES ('$enq_id','$quotation_id','$product_id','$product_quantity','$product_value','$product_delivery', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addproductdetailspo($po_id, $quotation_id,$product_id,$product_quantity,$product_value,$delivery)
    {

        $con = $this->__construct();
        $delivery = mysqli_real_escape_string($con, $delivery);
        $sql = "INSERT INTO po_pl(po_id, quotation_id, product_id, quantity, net_value, delivery,created) VALUES ('$po_id','$quotation_id','$product_id','$product_quantity','$product_value', '$delivery',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addproductdetailspo123($po_id, $quotation_id,$product_id,$product_quantity,$product_value,$delivery,$del_qty,$poplid)
    {

        $con = $this->__construct();
        $delivery = mysqli_real_escape_string($con, $delivery);
        // $sql = "INSERT INTO po_pl(po_id, quotation_id, product_id, quantity, net_value, delivery,delivered_quantity,created) VALUES ('$po_id','$quotation_id','$product_id','$product_quantity','$product_value', '$delivery','$del_qty',NOW())";
        if($poplid !=""){
        $sql = "UPDATE po_pl SET po_id='$po_id', quotation_id ='$quotation_id' , product_id= '$product_id', quantity= '$product_quantity', net_value='$product_value', delivery='$delivery' ,delivered_quantity='$del_qty' ,created=NOW() WHERE po_pl_id='$poplid'" ;
}else{
  $sql = "INSERT INTO po_pl(po_id, quotation_id, product_id, quantity, net_value, delivery,created) VALUES ('$po_id','$quotation_id','$product_id','$product_quantity','$product_value', '$delivery',NOW())" ;

}
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addproductdetailsoc($oc_id, $indent_id,$product_id,$product_quantity,$product_value,$delivery)
    {

        $con = $this->__construct();
        $delivery = mysqli_real_escape_string($con, $delivery);
        $sql = "INSERT INTO oc_pl(oc_id, indent_id, product_id, quantity, net_value, delivery,created) VALUES ('$oc_id','$indent_id','$product_id','$product_quantity','$product_value', '$delivery',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addproductdetailsindent($indent_id, $po_id,$product_id,$product_quantity,$product_value,$delivery)
    {
//echo $indent_id;
        $con = $this->__construct();
        $delivery = mysqli_real_escape_string($con, $delivery);
        $sql = "INSERT INTO indent_pl(indent_id, po_id, product_id, quantity, net_value, delivery,created) VALUES ('$indent_id','$po_id','$product_id','$product_quantity','$product_value', '$delivery',NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addcustomer($name,$Address,$Tel,$Fax,$confirm_email,$person_name,$Department,$Territory,$Shipping_Address,$city,$pincode)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO customer(name, address, Tel, Fax, email, person_name, Department, Territory, Shipping_Address, city, pincode, Created) VALUES ('$name', '$Address', '$Tel', '$Fax', '$confirm_email', '$person_name', '$Department','$Territory', '$Shipping_Address','$city','$pincode',  NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addcurrency($currency_code,$currency_name,$currency_symbol)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO currency_master(CURRENCY_CODE, CURRENCY_NAME, CURRENCY_SYMBOL, CREATION_DATE) VALUES ('$currency_code', '$currency_name', '$currency_symbol', NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addquotationprinci($qtn_no,$date,$enq_no,$date2,$customer,$company,$supplier,$despatch,$payment,$remark)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO new_quotation(quotation_no, enq_no, enq_date, principal_Qtn_Ref,principal_qtn_ref_date, validity, customer, company, supplier, despatch, payment, remark,note, project, status, created)VALUES ('','$enq_no','$date2','$qtn_no','$date','','$customer','$company','$supplier','$despatch','$payment','$remark','','','0', NOW())";
        $result = mysqli_query($con, $sql);
        $last_id = mysqli_insert_id($con);
        if ($result) {
            return $last_id;
        } else {
            return false;
        }

    }
    public function addquotationprincilog($qid,$qtn_no,$date,$enq_no,$date2,$customer,$company,$supplier,$despatch,$payment,$remark)
    {

        $con = $this->__construct();
        $sql = "INSERT INTO q_from_princi_log(`qid`,`principal_Qtn_Ref`, `principal_qtn_ref_date`, `enq_no`, `enq_date`, `customer`, `company`, `supplier`, `despatch`, `payment`, `remark`, `visibility`, `created`)VALUES ('$qid','$qtn_no','$date','$enq_no','$date2','$customer','$company','$supplier','$despatch','$payment','$remark','Enabled', NOW())";
        $result = mysqli_query($con, $sql);
        $last_id = mysqli_insert_id($con);
        if ($result) {
            return $last_id;
        } else {
            return false;
        }

    }
    public function editquotationprinci($oldquotation_ref,$quotation_ref,$date,$enq_no,$date2,$customer,$company,$supplier,$despatch,$payment,$remark)
    {

        $con = $this->__construct();
        $sql = "UPDATE `new_quotation` SET `principal_Qtn_Ref`='$quotation_ref',`principal_qtn_ref_date`='$date',`enq_no`='$enq_no',`enq_date`='$date2', `customer`= '$customer',`company`='$company',`supplier`='$supplier',`despatch`='$despatch',`payment`='$payment',`remark`='$remark' WHERE `principal_Qtn_Ref` = '$oldquotation_ref'";
        $result = mysqli_query($con, $sql);

        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function adddrawing($enq_no,$item,$file)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO drawing(enq_no, file_upload, item_name, created) VALUES ('$enq_no', '$file', '$item',  NOW())";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function addPO($po_no,$po_date,$enq_no,$enq_date,$princi_q_ref,$princi_q_ref_date,$q_no,$q_date,$customer,$company,$supplier,$validity,$despatch,$payment,$remark,$actual,$intimation,$upload,$etd1,$etd2,$etd3)
    {


        $con = $this->__construct();
        $sql = "INSERT INTO purchase_order(po_no, po_date, enq_no, enq_date, principal_qtn_ref, principal_qtn_ref_date, quotation_no, q_date, customer, company, supplier, validity, despatch, payment, actual_receipt_date, intimation_date, remark, upload, etd_1, etd_2, etd_3, created) VALUES ('$po_no', '$po_date', '$enq_no','$enq_date','$princi_q_ref','$princi_q_ref_date','$q_no','$q_date','$customer','$company','$supplier','$validity','$despatch','$payment'," . ($actual==NULL ? "NULL" : "'$actual'") . "," . ($intimation==NULL ? "NULL" : "'$intimation'") . ",'$remark','$upload'," . ($etd1==NULL ? "NULL" : "'$etd1'") . "," . ($etd2==NULL ? "NULL" : "'$etd2'") . "," . ($etd3==NULL ? "NULL" : "'$etd3'") . ",NOW())";
        $result = mysqli_query($con, $sql);
        $last_id = mysqli_insert_id($con);
        if ($result) {
            return $last_id;
        } else {
            return false;
        }

    }

    public function editPO($po_no,$po_id,$po_date,$enq_no,$enq_date,$princi_q_ref,$princi_q_ref_date,$q_no,$q_date,$customer,$company,$supplier,$validity,$despatch,$payment,$remark,$actual,$intimation,$upload,$etd1,$etd2,$etd3)
    {


        $con = $this->__construct();
        if($upload!="") {
            $sql = "UPDATE `purchase_order` SET `po_no`='$po_no',`po_date`='$po_date',`enq_no`='$enq_no',`enq_date`='$enq_date',`principal_qtn_ref`='$princi_q_ref',`principal_qtn_ref_date`='$princi_q_ref_date',`quotation_no`='$q_no',`q_date`='$q_date',`customer`='$customer',`company`='$company',`supplier`='$supplier',`validity`='$validity',`despatch`='$despatch',`payment`='$payment',`actual_receipt_date`='$actual',`intimation_date`='$intimation',`remark`='$remark',`upload`='$upload',`etd_1` = " . ($etd1==NULL ? "NULL" : "'$etd1'") . ",`etd_2` = " . ($etd2==NULL ? "NULL" : "'$etd2'") . ",`etd_3` = " . ($etd3==NULL ? "NULL" : "'$etd3'") . " WHERE `po_id`='$po_id'";
        }else {
            $sql = "UPDATE `purchase_order` SET `po_no`='$po_no',`po_date`='$po_date',`enq_no`='$enq_no',`enq_date`='$enq_date',`principal_qtn_ref`='$princi_q_ref',`principal_qtn_ref_date`='$princi_q_ref_date',`quotation_no`='$q_no',`q_date`='$q_date',`customer`='$customer',`company`='$company',`supplier`='$supplier',`validity`='$validity',`despatch`='$despatch',`payment`='$payment',`actual_receipt_date`='$actual',`intimation_date`='$intimation',`remark`='$remark',`etd_1` = " . ($etd1==NULL ? "NULL" : "'$etd1'") . ",`etd_2` = " . ($etd2==NULL ? "NULL" : "'$etd2'") . ",`etd_3` = " . ($etd3==NULL ? "NULL" : "'$etd3'") . " WHERE `po_id`='$po_id'";
        }$result = mysqli_query($con, $sql);

        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function editindent($indent_id,$indent_no,$indent_date,$po_no,$po_date,$customer,$cust_address,$prices,$shipping_address,$banker,$banker_address,$payment,$despatch,$cif,$agent,$agent_address,$note,$start_p,$date4,$report)
    {

//        $indent_id;
        $con = $this->__construct();
        if($date4!="" && $report!="") {
            $sql = "UPDATE `indent` SET `indent_no`='$indent_no',`indent_date`='$indent_date',`po_no`='$po_no',`po_date`='$po_date',`customer`='$customer',`customer_address`='$cust_address',`shipping_address`='$shipping_address',`banker`='$banker',`banker_address`='$banker_address',`despatch`='$despatch',`prices`='$prices',`payment`='$payment',`cif_charges`='$cif',`agent`='$agent',`agent_address`='$agent_address',`note`='$note',`start_production`='$start_p',`date`='$date4',`upload`='$report' WHERE `indent_id`='$indent_id'";
        }elseif($date4!="" && $report=="") {
            $sql = "UPDATE `indent` SET `indent_no`='$indent_no',`indent_date`='$indent_date',`po_no`='$po_no',`po_date`='$po_date',`customer`='$customer',`customer_address`='$cust_address',`shipping_address`='$shipping_address',`banker`='$banker',`banker_address`='$banker_address',`despatch`='$despatch',`prices`='$prices',`payment`='$payment',`cif_charges`='$cif',`agent`='$agent',`agent_address`='$agent_address',`note`='$note',`start_production`='$start_p',`date`= NULL  WHERE `indent_id`='$indent_id'";
        }elseif($date4=="" && $report!="") {
            $sql = "UPDATE `indent` SET `indent_no`='$indent_no',`indent_date`='$indent_date',`po_no`='$po_no',`po_date`='$po_date',`customer`='$customer',`customer_address`='$cust_address',`shipping_address`='$shipping_address',`banker`='$banker',`banker_address`='$banker_address',`despatch`='$despatch',`prices`='$prices',`payment`='$payment',`cif_charges`='$cif',`agent`='$agent',`agent_address`='$agent_address',`note`='$note',`start_production`='$start_p',`upload`='$report' WHERE `indent_id`='$indent_id'";
        }
        elseif($date4=="" && $report==""){
        $sql = "UPDATE `indent` SET `indent_no`='$indent_no',`indent_date`='$indent_date',`po_no`='$po_no',`po_date`='$po_date',`customer`='$customer',`customer_address`='$cust_address',`shipping_address`='$shipping_address',`banker`='$banker',`banker_address`='$banker_address',`despatch`='$despatch',`prices`='$prices',`payment`='$payment',`cif_charges`='$cif',`agent`='$agent',`agent_address`='$agent_address',`note`='$note',`start_production`='$start_p' WHERE `indent_id`='$indent_id'";
    }
            $result = mysqli_query($con, $sql);

        if ($result) {
            return $indent_id;

        } else {
            return false;
        }

    }
    public function addindent($indent_no,$indent_date,$po_no,$po_date,$customer,$cust_address,$prices,$shipping_address,$banker,$banker_address,$payment,$despatch,$cif,$agent,$agent_address,$note,$start_p,$date4,$report)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO indent(indent_no, indent_date, po_no, po_date, customer, customer_address, shipping_address, banker, banker_address, despatch,
prices, payment, cif_charges, agent, agent_address, note, start_production, date ,upload, created) VALUES (
'$indent_no','$indent_date','$po_no','$po_date','$customer','$cust_address','$shipping_address','$banker','$banker_address','$despatch','$prices','$payment','$cif','$agent','$agent_address','$note','$start_p'," . ($date4==NULL ? "NULL" : "'$date4'") . ",'$report',NOW())";
        $execute = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($execute) {
            return $last_id;
        } else {
            return false;
        }
    }
    public function addOCprinci($order_no,$order_date,$enq_no,$enq_date,$po_no,$po_date,$customer,$company,$supplier,$validity,$payment,$despatch,$remark,$report)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO order_confirmation(order_no, order_date, enq_no, enq_date, po_no, po_date, validity, despatch, customer, company, supplier, payment,remark, upload, status, conditions,created)
 VALUES ('$order_no','$order_date','$enq_no','$enq_date','$po_no','$po_date','$validity','$despatch','$customer','$company','$supplier','$payment','$remark','$report','0', '',NOW())";
        $execute = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($execute) {
            return $last_id;
        } else {
            return false;
        }
    }
    public function editOCprinci($order_no,$order_date,$enq_no,$enq_date,$po_no,$po_date,$customer,$company,$supplier,$validity,$payment,$despatch,$remark,$report,$oc_id)
    {
        $con = $this->__construct();
        if($report !='') {
            $sql = "UPDATE `order_confirmation` SET `order_no`='$order_no',`order_date`='$order_date',`enq_no`='$enq_no',`enq_date`='$enq_date',`po_no`='$po_no',`po_date`='$po_date',`validity`='$validity',`despatch`='$despatch',`customer`='$customer',`company`='$company',`supplier`='$supplier',`payment`='$payment',`remark`='$remark',`upload`='$report' WHERE oc_id ='$oc_id'";
        }else{
            $sql = "UPDATE `order_confirmation` SET `order_no`='$order_no',`order_date`='$order_date',`enq_no`='$enq_no',`enq_date`='$enq_date',`po_no`='$po_no',`po_date`='$po_date',`validity`='$validity',`despatch`='$despatch',`customer`='$customer',`company`='$company',`supplier`='$supplier',`payment`='$payment',`remark`='$remark' WHERE oc_id ='$oc_id'";

        }
        $execute = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($execute) {
            return $last_id;
        } else {
            return false;
        }
    }
    public function editOCcust($our_ref_no,$our_ref_date,$order_no,$order_date,$enq_no,$enq_date,$po_no,$po_date,$customer,$company,$supplier,$validity,$payment,$despatch,$note,$condition,$oc_id)
    {
        $con = $this->__construct();

        $sql = "UPDATE `order_confirmation` SET `our_ref_no`= '$our_ref_no',`our_ref_date`='$our_ref_date',`order_no`='$order_no',`order_date`='$order_date',`enq_no`='$enq_no',`enq_date`='$enq_date',`po_no`='$po_no',`po_date`='$po_date',`validity`='$validity',`despatch`='$despatch',`customer`='$customer',`company`='$company',`supplier`='$supplier',`payment`='$payment',`note`='$note',`conditions` ='$condition' WHERE oc_id ='$oc_id'";

        $execute = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($execute) {
            return $last_id;
        } else {
            return false;
        }
    }

    public function addOCcust($our_ref_no,$our_ref_date,$order_no,$order_date,$enq_no,$enq_date,$po_no,$po_date,$customer,$company,$supplier,$validity,$payment,$despatch,$note,$condition)
    {

        $con = $this->__construct();
        $sql = "UPDATE order_confirmation SET our_ref_no='$our_ref_no', our_ref_date='$our_ref_date',order_no='$order_no',order_date='$order_date',enq_no='$enq_no',enq_date='$enq_date',po_no='$po_no',po_date='$po_date',validity='$validity',despatch='$despatch',customer='$customer',company='$company',supplier='$supplier',payment='$payment',status='1',note='$note',conditions='$condition' WHERE order_no='$order_no'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

    public function addquotationcustomer($enq_no,$q_no,$principal_qtn_ref ,$principal_qtn_ref_date,$validity ,$note,$project,$due_date)
    {
//         echo $q_no;
        $con = $this->__construct();
        $sql = "UPDATE `new_quotation` SET `status` = '1', `principal_Qtn_Ref` ='$principal_qtn_ref', `principal_qtn_ref_date` = '$principal_qtn_ref_date',`validity` = '$validity' , `note` = '$note',`project` = '$project',`due_date` = '$due_date' WHERE `enq_no` = '$enq_no' AND `quotation_no` = '$q_no'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function editquotationcustomer($enq_no,$enq_date,$q_no,$q_date,$principal_qtn_ref ,$principal_qtn_ref_date,$validity ,$despatch,$payment,$cust,$company,$supplier,$note,$remark,$project,$due_date)
    {
//         echo $q_no;
        $con = $this->__construct();
        if(empty($due_date)){
            $sql = "UPDATE `new_quotation` SET `q_date`='$q_date',`enq_date`='$enq_date',`principal_Qtn_Ref` ='$principal_qtn_ref', `principal_qtn_ref_date` = '$principal_qtn_ref_date',`validity` = '$validity' ,`customer`='$cust',`company`='$company',`supplier`='$supplier',`despatch`='$despatch',`payment`='$payment',`remark`='$remark', `note` = '$note',`project` = '$project',`due_date` = NULL WHERE `enq_no` = '$enq_no' AND `quotation_no` = '$q_no'";
        }else{
            $sql = "UPDATE `new_quotation` SET `q_date`='$q_date',`enq_date`='$enq_date',`principal_Qtn_Ref` ='$principal_qtn_ref', `principal_qtn_ref_date` = '$principal_qtn_ref_date',`validity` = '$validity' ,`customer`='$cust',`company`='$company',`supplier`='$supplier',`despatch`='$despatch',`payment`='$payment',`remark`='$remark', `note` = '$note',`project` = '$project',`due_date` = '$due_date' WHERE `enq_no` = '$enq_no' AND `quotation_no` = '$q_no'";
        }
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function addquotationcustomer123($enq_no,$q_no,$q_date,$principal_qtn_ref ,$principal_qtn_ref_date,$validity ,$note,$project,$due_date)
    {
//         echo $q_no;
        $con = $this->__construct();
        if($due_date=''){
            $sql = "UPDATE `new_quotation` SET `status` = '1', `quotation_no` = '$q_no',`q_date` = '$q_date', `principal_qtn_ref_date` = '$principal_qtn_ref_date',`validity` = '$validity' , `note` = '$note',`project` = '$project',`due_date` = '$due_date' WHERE `enq_no` = '$enq_no' AND `principal_Qtn_Ref` = '$principal_qtn_ref'";
        }else{
            $sql = "UPDATE `new_quotation` SET `status` = '1', `quotation_no` = '$q_no',`q_date` = '$q_date', `principal_qtn_ref_date` = '$principal_qtn_ref_date',`validity` = '$validity' , `note` = '$note',`project` = '$project' WHERE `enq_no` = '$enq_no' AND `principal_Qtn_Ref` = '$principal_qtn_ref'";
        }
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

    //edit function starts here
    public function editcompany($id,$name,$Address,$email,$telephone,$fax,$website,$Code,$gst,$pan,$iec,$Purchase_Order,$Indent_Order,$Quotation_No,$Order_Confirmation,$Despatch,$Enquiry,$Action_Paper,$Drawing,$bank,$ac_no,$ifsc)
    {


        $con = $this->__construct();
        $sql = "UPDATE company SET name = '$name', address = '$Address',email = '$email',telephone='$telephone',fax = '$fax', website = '$website', code = '$Code', gst = '$gst', pan = '$pan',iec = '$iec',bank_details = '$bank',ac_no = '$ac_no',ifsc = '$ifsc', purchase_order = '$Purchase_Order',intent_order='$Indent_Order',quotation_no='$Quotation_No',order_confirmation='$Order_Confirmation',despatch='$Despatch',enquiry='$Enquiry',actionpaper='$Action_Paper',drawing='$Drawing',visibility = 'Enable' WHERE c_id = '$id '";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function editsign($id,$name,$upload)
    {


        $con = $this->__construct();
        $sql = "UPDATE signature SET name = '$name', upload='$upload' ,visibility = 'Enabled' WHERE sign_id = '$id '";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function editterritory($id,$code,$name,$gst,$address)
    {


        $con = $this->__construct();
        $sql = "UPDATE territory SET territory_full = '$code', name = '$name', territory_gst = '$gst', address = '$address', visibility = 'Enable' WHERE t_id = '$id '";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
//    public function edituser($id,$username,$pass,$epass)
//    {
//
//
//        $con = $this->__construct();
//        if($pass == "")
//        $sql = "UPDATE admin SET username = '$username', password = '$pass' , address = '$address', visibility = 'Enable' WHERE t_id = '$id '";
//        $result = mysqli_query($con, $sql);
//        if ($result) {
//            return true;
//        } else {
//            return false;
//        }
//
//    }
    public function editprincipal($id,$name,$Address,$Tel,$Fax,$Email,$Name_person,$Department,$Banker_Details,$Currency,$Note_Q,$Note_OC,$Conditions_OC,$Country,$Decimal)
    {

        $con = $this->__construct();
        $sql = "UPDATE `principal` SET `name`='$name',`address`='$Address',`Tel`='$Tel',`Fax`='$Fax',`email`='$Email',`Name_person`='$Name_person',`Department`='$Department',`Banker_Details`='$Banker_Details',`Currency`='$Currency',`Note_Q`='$Note_Q',`Note_OC`='$Note_OC',`Conditions_OC`='$Conditions_OC',`Country`='$Country',`Decimal1`='$Decimal',`visibility` = 'Enable' WHERE `p_id` = '$id'";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    public function editcustomer($id,$name,$Address,$Tel,$Fax,$confirm_email,$person_name,$Department,$Territory,$Shipping_Address,$city,$pincode,$gst,$pan,$iec)
    {


        $con = $this->__construct();
        $sql = "UPDATE `customer` SET `name`='$name',`Address`='$Address',`Tel`='$Tel',`Fax`='$Fax',`email`='$confirm_email',`person_name`='$person_name',`Department`='$Department',`Territory`='$Territory' ,`Shipping_Address`='$Shipping_Address',`city`= '$city',`pincode` = '$pincode',`cust_gst` = '$gst', `cust_pan`= '$pan',`cust_iec` = '$iec'  WHERE `cust_id`='$id' ";
        $result = mysqli_query($con, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }


    //edit function starts Ends

    public function listcompany()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM company where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function giveproduct1()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listterritory()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM territory where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listuser()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM admin where visibility='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listcurrency()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM currency_master where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function sendenqid($enqno)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT enq_id FROM new_enq WHERE enq_no = '$enqno'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }

        foreach($response as $some){

            $response1 = $some['enq_id'];
        }

        return $response1;


    }
    public function sendqid($qno)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT quotation_id FROM new_quotation WHERE quotation_no = '$qno'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }

        foreach($response as $some){

            $response1 = $some['quotation_id'];
        }

        return $response1;


    }
    public function getcompany()
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM company WHERE c_id = 10";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listcompany1($company)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM company WHERE name LIKE '%$company' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listterritory1($territory)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM territory WHERE name ='$territory' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listprincipal1($principal)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM principal WHERE name LIKE '%$principal' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listcustomer1($customer)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM customer WHERE name LIKE '%$customer' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function listingenq()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq WHERE visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingdespatch()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.despatch_id,t1.invoice_date,t1.invoice_no,t4.customer,t1.supplier FROM despatch t1 JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id JOIN purchase_order t4 ON t3.po_id = t4.po_id JOIN customer t6 ON t4.customer = t6.name WHERE t1.visibility ='Enabled' GROUP BY t1.invoice_no; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingquotation()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where visibility= 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingenquiries2()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq WHERE status = '1'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingquotation2()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where status='1' AND visibility='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingpo()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function actionpaperlisting()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t4.po_no,t4.po_date,t4.po_id,t4.principal_qtn_ref,t1.despatch_id,t1.invoice_no,t1.invoice_date,t4.customer
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                WHERE t1.visibility = 'Enabled'
                GROUP BY t4.po_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function actionpaperlistingnew()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_no,t1.po_date,t1.po_id,t1.customer
                FROM purchase_order t1
                WHERE t1.visibility = 'Enabled'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function detaileddistributorscomm($from,$to,$territory)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT EXTRACT( YEAR_MONTH FROM t1.invoice_date ) AS MONTH ,t5.po_no,t5.customer,t1.invoice_no,t4.item,t2.quantity,t2.net_value,ROUND(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS TOTAL_COMMISSION
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN product t4 ON t3.product_id = t4.product_id
                JOIN purchase_order t5 ON t3.po_id = t5.po_id
                JOIN customer t6 ON t5.customer = t6.name
                WHERE '$from'<=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t6.Territory = '$territory' AND t1.visibility = 'Enabled'
                ORDER BY t1.invoice_date";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function distributorscomm($from,$to,$territory,$current)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS TOTAL_COMMISSION
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN product t4 ON t3.product_id = t4.product_id
                JOIN purchase_order t5 ON t3.po_id = t5.po_id
                JOIN customer t6 ON t5.customer = t6.name
                WHERE '$from'<=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t6.Territory = '$territory' AND t1.visibility = 'Enabled'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $total = $row['TOTAL_COMMISSION'];
        }
        $total = (int)$total;
        $current = (float)$current;
        $final = round($total/$current);
        $gst = round($final*18/100);
        $finalcomm = $gst+$final;
        $finalcomm = (int)$finalcomm;
        $tds = round($final*5/100);
        $tds = (int)$tds;
        $netamount = $finalcomm-$tds;
        $response['Commission_amount1']= $total;
        $response['rate']= $current;
        $response['comm_INR']= $final;
        $response['gst']= $gst;
        $response['Commission_amount2']= $finalcomm;
        $response['tds']= $tds;
        $response['net_amount']= $netamount;
        return $response;

    }
    public function debitnotecommissionamount($from,$to,$territory,$current)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS TOTAL_COMMISSION
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN product t4 ON t3.product_id = t4.product_id
                JOIN purchase_order t5 ON t3.po_id = t5.po_id
                JOIN customer t6 ON t5.customer = t6.name
                WHERE '$from'<=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t6.Territory = '$territory' AND t1.visibility = 'Enabled'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $total = $row['TOTAL_COMMISSION'];
            array_push($response, $row);
        }
        $total = (int)$total;
        $current = (float)$current;
        $final = round($total/$current);
        return $final;

    }
    public function listingoc()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingoc1()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation WHERE status = '1' AND visibility = 'Enabled';  ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingocprinci()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation WHERE status = '0' AND visibility = 'Enabled';  ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listinglc()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM lc_details WHERE visibility = 'Enabled'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingindent()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent WHERE visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listingdrawing()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM drawing ; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function listprincipal()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM principal where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function listcustomer()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM customer where visibility='Enable' ORDER BY `customer`.`name` ASC; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listsignature()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM signature where visibility='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function listmake()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM make_master where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listsubgroup()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM subgroup_master where visibility='Enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function listproduct()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product where visibility='Enable' ORDER BY `product`.`item` ASC ; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listpurchaseorder()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order ; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listpurchaseorder123()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE visibility = 'Enabled' ORDER BY purchase_order . customer  ASC";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listexistingpurchaseorder($despatch_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT po_pl_id FROM despatch_pl where despatch_id='$despatch_id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listexistingpurchaseorderdetails($po_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order where po_id='$po_id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function getpurchaseorderid($po_pl_id)
    {
//        $response = array();
        $con = $this->__construct();
        $sql = "SELECT po_id FROM po_pl where po_pl_id='$po_pl_id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $response = $row;
        }
        return $response;

    }

    public function listdespatchdetail($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t4.po_id
                FROM
                despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                WHERE t1.visibility = 'Enabled' AND t1.despatch_id = '$id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function realpodetail($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * from purchase_order WHERE visibility = 'Enabled' AND  po_id= '$id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listdespatchdetailoriginal($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM despatch WHERE visibility = 'Enabled' AND despatch_id = '$id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function listcountry()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM `country_master`";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function listproduct1($so)
    {

        $response = array();
        $con = $this->__construct();

        if ($so == '') {

            $sql = "SELECT * FROM product WHERE visibility = 'Enable'";
        } else {

            $sql = "SELECT * FROM product WHERE visibility = 'Enable'";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function listpo1($so)
    {

        $response = array();
        $con = $this->__construct();

        if ($so == '') {

            $sql = "SELECT * FROM purchase_order WHERE visibility = 'Enabled'";
        } else {

            $sql = "SELECT * FROM purchase_order WHERE po_id NOT IN ($so) AND visibility = 'Enabled'";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function showproductmake()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT make FROM product WHERE visibility = 'enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductpo($po_id)
    {

        $response = array();
        $response1 = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM po_pl WHERE po_id = $po_id; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }



        return $response;

    }
    public function despatchdetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM despatch WHERE despatch_id = '$id' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function productdetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function makedetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM make_master WHERE make_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function subgroupdetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM subgroup_master WHERE subg_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function signdetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM signature WHERE sign_id = '$id' AND visibility='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function currencydetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM currency_master WHERE CURRENCY_ID = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function swapnilshowproductpo($po_id)
    {

        $response = array();
        $response1 = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM po_pl WHERE po_id = $po_id; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {

            $po_id = $row['po_id'];
            $pro_id[] = $row['product_id'];

            array_push($response, $row);
        }

        foreach ($pro_id as $proo_id){

            $yes[] = $this->productdetails1($proo_id);
        }
        $sql1 = "SELECT po_no FROM purchase_order WHERE po_id = $po_id; ";
        $result = mysqli_query($con, $sql1);
        while ($row = mysqli_fetch_array($result, 1)) {

            $po_no = $row['po_no'];
            $row['po_details'] = $response;
            array_push($response1, $row);
        }


//        print_r($yes);
        // echo json_encode($response1);
//        echo $po_no;
//        return $response;

//        $finalarray=array();
//
//        for($i=0;$i<count($product_id);$i++){
//            $merge_array[$i] =
//                ['product_id'=>$product_id[$i],
//                    'quantity'=>$quantity[$i]
//                ];
//        }


    }

    public function checkingdespfunction($poid)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t4.po_no,t4.po_id,t3.po_pl_id,t3.product_id,t3.quantity,t2.net_value,t3.delivered_quantity,t2.quantity,t5.item AS item,t5.make AS make,CONCAT_WS('-', make, item) AS itemname
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN product t5 ON t3.product_id = t5.product_id
                WHERE t4.po_id = '$poid'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        if(!empty($response)){
            return $response;
        }else{
            return $poid;
        }
    }


    public function swaplatestfunction($poid)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_no, t1.po_id, t2.po_id, t2.po_pl_id, t2.product_id, t2.quantity,t2.net_value,t2.delivered_quantity,t3.item as item,t3.make as make,CONCAT_WS('-', make, item) AS itemname
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.po_id = $poid";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }




    public function productdetails1($poid)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT make AS make
     , item AS item
     , CONCAT_WS('-', make, item) AS itemname
  FROM product WHERE product_id = $poid; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function showproductdetailsenq($product_id,$quantity,$pl_id)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }

//    public function showproductdetails($product_id,$quantity,$pl_id)
//    {
//        $response = array();
//        $con = $this->__construct();
//        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
//        $result = mysqli_query($con, $sql);
//
//        while ($row = mysqli_fetch_array($result, 1)) {
//            $row['pl_id'] = $pl_id;
//            $row['quantity'] = $quantity;
//            $row['value'] = $value;
//            $row['delivery'] = $delivery;
//
//
//            array_push($response,$row);
//        }
////        echo "<pre>";
////        print_r($response);echo "</pre>";
//        return $response;
//
//    }
    public function showproductdetails($product_id,$quantity,$value,$delivery,$pl_id)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }
    public function showproductdetailsquotation($product_id,$quantity,$value,$delivery,$pl_id)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }
    public function showproductdetailspurchaseorder($product_id,$quantity,$value,$delivery,$pl_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }
    public function showproductdetailspurchaseorder123($product_id,$quantity,$value,$delivery,$pl_id,$del_qty)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;
            $row['delivered_quantity'] = $del_qty;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }
    public function showproductdetailsindent($product_id,$quantity,$value,$delivery,$pl_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }
    public function showproductdetailsqprint($product_id,$quantity,$value,$delivery,$pl_id)
    {
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_array($result, 1)) {
            $row['pl_id'] = $pl_id;
            $row['quantity'] = $quantity;
            $row['value'] = $value;
            $row['delivery'] = $delivery;


            array_push($response,$row);
        }
//        echo "<pre>";
//        print_r($response);echo "</pre>";
        return $response;

    }

    public function showenquiry($enq_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq WHERE enq_no = '$enq_no' AND visibility = 'Enabled' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showenquiry1($enq_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq WHERE enq_id = '$enq_no' AND visibility = 'Enabled' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlist($enq_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM enq_pl WHERE enq_id = '$enq_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function showproductlistquotation($q_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM quotation_pl WHERE quotation_id = '$q_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistquotation1($q_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT *,quantity*net_value AS TOTAL_AMOUNT FROM quotation_pl WHERE quotation_id = '$q_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function showproductlistpo($po_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM po_pl WHERE po_id = '$po_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistpo1($po_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT *,quantity*net_value AS TOTAL_AMOUNT FROM po_pl WHERE po_id = '$po_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistdespatch($despatch_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t4.po_no,t4.po_date,t5.item,t2.quantity,t2.net_value,t2.commission, ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT, t2.quantity*t2.net_value AS DESPATCH_AMOUNT
                      FROM despatch t1
                      JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                      JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                      JOIN purchase_order t4 ON t3.po_id = t4.po_id
                      JOIN product t5 ON t3.product_id = t5.product_id
                      WHERE t1.despatch_id ='$despatch_id' AND t1.visibility = 'Enabled '";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyenq($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT enq_no,customer FROM `new_enq` WHERE DATE(created) = '$date'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyqp($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.quotation_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t1.enq_no
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                WHERE DATE(t1.created) = '$date' AND t1.status = '0'
                GROUP BY t1.quotation_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyqc($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.quotation_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t1.enq_no
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                WHERE DATE(t1.created) = '$date' AND t1.status = '1'
                GROUP BY t1.quotation_no ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailypo($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t1.principal_qtn_ref
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                WHERE DATE(t1.created) = '$date'
                GROUP BY t1.po_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyindent($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.indent_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t1.po_no
                FROM indent t1
                JOIN indent_pl t2 ON t1.indent_id = t2.indent_id
                WHERE DATE(t1.created) = '$date'
                GROUP BY t1.indent_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyoc_p($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.order_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t3.po_no
                FROM order_confirmation t1
                JOIN oc_pl t2 ON t1.oc_id = t2.oc_id
                JOIN purchase_order t3 ON t1.po_no = t3.po_no
                WHERE DATE(t1.created) = '$date' AND t1.status = '0'
                GROUP BY t1.order_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailyoc_c($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.order_no,t1.customer, SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t3.po_no
                FROM order_confirmation t1
                JOIN oc_pl t2 ON t1.oc_id = t2.oc_id
                JOIN purchase_order t3 ON t1.po_no = t3.po_no
                WHERE DATE(t1.created) = '$date' AND t1.status = '1'
                GROUP BY t1.order_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailylc($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT lc_no,customer,value,po_no FROM `lc_details` WHERE DATE(created) = '$date'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function dailydespatch($date)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.invoice_no,t4.customer,SUM(t2.quantity*t2.net_value) AS TOTAL_AMOUNT,t4.po_no
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                WHERE DATE(t1.created)= '$date'
                GROUP BY t1.invoice_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistindent1($indent_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT *,quantity*net_value AS TOTAL_AMOUNT FROM indent_pl WHERE indent_id = '$indent_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistindent($indent_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent_pl WHERE indent_id = '$indent_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistoc($oc_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM oc_pl WHERE oc_id = '$oc_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showproductlistoc1($oc_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT *,quantity*net_value as TOTAL_AMOUNT FROM oc_pl WHERE oc_id = '$oc_id' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function showaddress($cust_name)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT address FROM customer WHERE name = '$cust_name' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);

            $response = $row['address'];
        }

        return $response;

    }
    public function showdrawing($enq_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM drawing WHERE enq_no = '$enq_no' ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function enqtoprincipal($enq_no,$date,$customer,$company,$supplier)
    {

        $con = $this->__construct();


        $sql = "UPDATE `new_enq` SET `status` = '1' , `date` = '$date', `customer` = '$customer', `company` = '$company', `supplier`= '$supplier' WHERE `enq_id` = '$enq_no'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }



    public function showquotation($enq_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq where enq_no = '$enq_no' WHERE visibility = 'enable'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationdetailspref($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation WHERE principal_Qtn_Ref = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationdetails($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where quotation_no = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationdetails1($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where principal_Qtn_Ref = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationfrompdetails($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where quotation_id = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationtocview($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation where quotation_id = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function podetails($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_no = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function podetailsedit($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_id = '$q_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function canpodetailsedit($q_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_id = '$q_no' AND visibility = 'Cancelled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function getlatestindent()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT LAST_INSERT_ID(indent_id),indent_no FROM `indent` ORDER BY `indent`.`indent_no` DESC LIMIT 1";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $no = $row['indent_no'];
        }
        return $no;

    }



    public function indentdetails($indent_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent WHERE indent_no = '$indent_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function indentdetails1($indent_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent WHERE indent_id = '$indent_no' AND visibility='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function ocdetails($po_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation where po_no = '$po_no'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function ocdetails1($oc_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation WHERE order_no = '$oc_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function ocdetailsprinci($oc_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation WHERE oc_id = '$oc_id' AND visibility ='Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function podetails1($po_no)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_no = '$po_no' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function lcdetails1($lc_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM lc_details WHERE lc_id = '$lc_id' AND visibility = 'Enabled'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function companydetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM company WHERE c_id='$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function territorydetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM territory WHERE t_id='$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
//    public function userdetails($id)
//    {
//
//        $response = array();
//        $con = $this->__construct();
//        $sql = "SELECT * FROM admin WHERE id='$id'; ";
//        $result = mysqli_query($con, $sql);
//        while ($row = mysqli_fetch_array($result, 1)) {
//            array_push($response, $row);
//        }
//        return $response;
//
//    }
    public function principaldetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM principal WHERE p_id='$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function customerdetails($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM customer WHERE cust_id='$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
// print pdf function starts

    public function enqprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_enq WHERE enq_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function enqprodprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM enq_pl WHERE enq_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function actionpaper_report_print($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t9.name,t9.Territory,t8.enq_no,t8.date,t7.principal_Qtn_Ref,t7.principal_qtn_ref_date,t7.quotation_no,t7.q_date,t4.po_no,t4.po_date,t5.indent_no,t5.indent_date,t5.start_production,t6.our_ref_no,t6.our_ref_date,t6.order_no,t6.order_date,t10.lc_no,t10.lc_date,t10.expiry_date,t10.value,t1.invoice_no,t1.invoice_date,t11.Currency,
                (t3.quantity*t3.net_value) AS PO_AMOUNT,
                SUM(((t2.quantity*t2.net_value)/t2.commission)+t2.quantity*t2.net_value) AS DESPATCH_AMOUNT,
                SUM((t2.quantity*t2.net_value)/t2.commission) AS COMMISSION_AMOUNT
                FROM despatch t1
                LEFT JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                LEFT JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                LEFT JOIN purchase_order t4 ON t3.po_id = t4.po_id
                LEFT JOIN indent t5 ON t4.po_no = t5.po_no
                LEFT JOIN order_confirmation t6 ON t4.po_no = t6.po_no
                LEFT JOIN new_quotation t7 ON t4.quotation_no = t7.quotation_no
                LEFT JOIN new_enq t8 ON t7.enq_no = t8.enq_no
                LEFT JOIN customer t9 ON t4.customer = t9.name
                LEFT JOIN lc_details t10 ON t4.po_no = t10.po_no
                LEFT JOIN principal t11 on t4.supplier = t11.name
                WHERE t1.despatch_id = '$id'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function actionpaper_report_print_new($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t11.name,t11.Territory,t2.enq_no,t2.date,t3.principal_Qtn_Ref,t3.principal_qtn_ref_date,t3.quotation_no,t3.q_date,t1.po_no,t1.po_date,t1.actual_receipt_date,t1.intimation_date,
                t4.indent_no,t4.indent_date,t4.start_production,t5.our_ref_no,t5.our_ref_date,t5.order_no,t5.order_date,
                t6.lc_no,t6.lc_date,t6.expiry_date,t6.value,t9.invoice_no,t9.invoice_date,t10.Currency

                FROM purchase_order t1
                LEFT JOIN new_enq t2 ON t1.enq_no = t2.enq_no
                LEFT JOIN new_quotation t3 ON t1.quotation_no = t3.quotation_no
                LEFT JOIN indent t4 ON t1.po_no = t4.po_no
                LEFT JOIN order_confirmation t5 ON t1.po_no = t5.po_no
                LEFT JOIN lc_details t6 ON t1.po_no = t6.po_no
                LEFT JOIN po_pl t7 ON t1.po_id = t7.po_id
                LEFT JOIN despatch_pl t8 ON t7.po_pl_id = t8.po_pl_id
                LEFT JOIN despatch t9 ON t8.despatch_id = t9.despatch_id
                LEFT JOIN principal t10 ON t1.supplier = t10.name
                LEFT JOIN customer t11 ON t1.customer = t11.name
                WHERE t1.po_id = '$id' AND t1.visibility = 'Enabled'
                GROUP BY t7.po_id";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function actionpaper_invoice_details1($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t9.invoice_no,t9.invoice_date,
                SUM(t8.quantity*t8.net_value) AS DESPATCH_AMOUNT,
                SUM((t8.quantity*t8.net_value)/t8.commission) AS COMMISSION_AMOUNT
				        FROM purchase_order t1
                JOIN po_pl t7 ON t1.po_id = t7.po_id
                JOIN despatch_pl t8 ON t7.po_pl_id = t8.po_pl_id
                JOIN despatch t9 ON t8.despatch_id = t9.despatch_id

                WHERE t1.po_id = '$id' AND t1.visibility = 'Enabled'
                GROUP BY t9.despatch_id";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    /*latest action paper function starts here*/
    public function actionpaper_report_print_final($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t11.name,t11.Territory,t2.enq_no,t2.date,t3.principal_Qtn_Ref,t3.principal_qtn_ref_date,t3.quotation_no,t3.q_date,t1.po_no,t1.po_date,t1.actual_receipt_date,t1.intimation_date,
                t4.indent_no,t4.indent_date,t4.start_production,t5.our_ref_no,t5.our_ref_date,t5.order_no,t5.order_date,
                t6.lc_no,t6.lc_date,t6.expiry_date,t6.value,t9.invoice_no,t9.invoice_date,t10.Currency


                FROM purchase_order t1
                LEFT JOIN new_enq t2 ON t1.enq_no = t2.enq_no
                LEFT JOIN new_quotation t3 ON t1.quotation_no = t3.quotation_no
                LEFT JOIN indent t4 ON t1.po_no = t4.po_no
                LEFT JOIN order_confirmation t5 ON t1.po_no = t5.po_no
                LEFT JOIN lc_details t6 ON t1.po_no = t6.po_no
                LEFT JOIN po_pl t7 ON t1.po_id = t7.po_id
                LEFT JOIN despatch_pl t8 ON t7.po_pl_id = t8.po_pl_id
                LEFT JOIN despatch t9 ON t8.despatch_id = t9.despatch_id
                LEFT JOIN principal t10 ON t1.supplier = t10.name
                LEFT JOIN customer t11 ON t1.customer = t11.name
                WHERE t1.po_id = '$id' AND t1.visibility = 'Enabled'";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    /*latest action paper function ends here*/

    public function actionpaper_calculation($id)
    {

        $response = array();
        $response1 = array();
        $response2 = array();
        $response3 = array();
        $con = $this->__construct();
        $sql = "SELECT t7.quantity*t7.net_value AS PO_AMOUNT,
                t8.quantity*t8.net_value AS DESPATCH_AMOUNT,

                ROUND((((t8.quantity*t8.net_value)*t8.commission)/100)) AS COMMISSION_AMOUNT


                FROM purchase_order t1
                LEFT JOIN new_enq t2 ON t1.enq_no = t2.enq_no
                LEFT JOIN new_quotation t3 ON t1.quotation_no = t3.quotation_no
                LEFT JOIN indent t4 ON t1.po_no = t4.po_no
                LEFT JOIN order_confirmation t5 ON t1.po_no = t5.po_no
                LEFT JOIN lc_details t6 ON t1.po_no = t6.po_no
                LEFT JOIN po_pl t7 ON t1.po_id = t7.po_id
                LEFT JOIN despatch_pl t8 ON t7.po_pl_id = t8.po_pl_id
                LEFT JOIN despatch t9 ON t8.despatch_id = t9.despatch_id
                LEFT JOIN principal t10 ON t1.supplier = t10.name
                LEFT JOIN customer t11 ON t1.customer = t11.name
                WHERE t1.po_id = '$id' AND t1.visibility = 'Enabled'
                GROUP BY t7.product_id";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response1, $row['PO_AMOUNT']);
            array_push($response2, $row['DESPATCH_AMOUNT']);
            array_push($response3, $row['COMMISSION_AMOUNT']);
        }

//        echo json_encode($response1);
//        echo json_encode($response2);
//        echo json_encode($response3);



        for($i=0;$i<count($response1);$i++){

           $response= array('po_amount'=>$response1,
                    'despatch_amount'=>$response2,
                    'comm_amount'=>$response3


            );
        }
//        echo json_encode($response);
        return $response;

    }




    public function qtocprint($from,$to,$company,$principal)
    {

//        echo $from.'till'.$to;
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.customer,t1.quotation_no,t1.q_date,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE t1.company='$company' AND t1.supplier='$principal' GROUP BY t1.customer";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function poitemprint($from,$to,$territory,$customer)
    {
        if ($from == '' && $to == '' && $territory == '' && $customer == '') {
            $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                JOIN principal t4 ON t1.supplier = t4.name
                JOIN customer t5 ON t1.customer = t5.name
                WHERE t1.visibility = 'Enabled'";

        }else{
            $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                JOIN principal t4 ON t1.supplier = t4.name
                JOIN customer t5 ON t1.customer = t5.name
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "";
        }

//        if ($from == '' && $to == '' && $territory == '') {
//            $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                GROUP BY t3.item";
//
//        }if ($from == '' && $to == '' && $territory != '') {
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE t5.Territory = '$territory'
//                GROUP BY t3.item";
//
//    }elseif($from != '' && $to != '' && $territory == ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.po_date) AND DATE(t1.po_date)<='$to'
//                GROUP BY t3.item";
//    }elseif($from != '' && $to != '' && $territory != ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.po_date) AND DATE(t1.po_date)<='$to' AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//    }elseif ($from !='' && $to == '' && $territory == ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.po_date)
//                GROUP BY t3.item";
//    }elseif ($from !='' && $to == '' && $territory != ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.po_date) AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//    }elseif ($from =='' && $to != '' && $territory == ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE DATE(t1.po_date)<='$to'
//                GROUP BY t3.item";
//    }elseif ($from =='' && $to != '' && $territory != ''){
//        $sql = "SELECT t3.item,t1.po_no,t5.name,t1.po_date,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                FROM purchase_order t1
//                JOIN po_pl t2 ON t1.po_id = t2.po_id
//                JOIN product t3 ON t2.product_id = t3.product_id
//                JOIN principal t4 ON t1.supplier = t4.name
//                JOIN customer t5 ON t1.customer = t5.name
//                WHERE DATE(t1.po_date)<='$to' AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//    }

        $response = array();
        $con = $this->__construct();
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quotationitemprint($from,$to,$territory,$customer)
    {

        if ($from == '' && $to == '' && $territory == '' && $customer =='') {
            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN principal t4 ON t1.supplier = t4.name
                    JOIN customer t5 ON t1.customer = t5.name
                    WHERE t1.visibility = 'Enabled'";
        }else{
            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN principal t4 ON t1.supplier = t4.name
                    JOIN customer t5 ON t1.customer = t5.name
                    WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.q_date)") . " " . ($to=='' ? "" : "AND DATE(t1.q_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "";

        }

//
//
//        if ($from == '' && $to == '' && $territory == '') {
//            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                    GROUP BY t3.item";
//
//        }if ($from == '' && $to == '' && $territory != '') {
//        $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                    WHERE t5.Territory = '$territory'
//                    GROUP BY t3.item";
//
//        }elseif($from != '' && $to != '' && $territory != ''){
//            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//        }elseif($from != '' && $to != '' && $territory == ''){
//            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.q_date) AND DATE(t1.q_date)<='$to'
//                GROUP BY t3.item";
//    }elseif ($from !='' && $to == '' && $territory == ''){
//            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.q_date)
//                GROUP BY t3.item";
//        }elseif ($from !='' && $to == '' && $territory != ''){
//        $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE '$from'<=DATE(t1.q_date) AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//    }elseif ($from =='' && $to != '' && $territory == ''){
//            $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE DATE(t1.q_date)<='$to'
//                GROUP BY t3.item";
//        }elseif ($from =='' && $to != '' && $territory != ''){
//        $sql = "SELECT t3.item,t1.quotation_no,t5.name,t1.q_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
//                    FROM new_quotation t1
//                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
//                    JOIN product t3 ON t2.product_id = t3.product_id
//                    JOIN principal t4 ON t1.supplier = t4.name
//                    JOIN customer t5 ON t1.customer = t5.name
//                WHERE DATE(t1.q_date)<='$to' AND t5.Territory = '$territory'
//                GROUP BY t3.item";
//    }
        $response = array();
        $con = $this->__construct();
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function despatchitemprint($from,$to,$territory,$customer)
    {

        if ($from == '' && $to == '' && $territory == '' && $customer == '') {
            $sql = "SELECT t5.item,t4.po_no,t4.po_date,t1.invoice_no,t1.invoice_date,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
                    JOIN product t5 ON t5.product_id = t3.product_id
                    JOIN principal t6 ON t1.supplier = t6.name
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE t1.visibility = 'Enabled' ";
        }else{
            $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
                    JOIN product t5 ON t5.product_id = t3.product_id
                    JOIN principal t6 ON t1.supplier = t6.name
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.invoice_date)") . " " . ($to=='' ? "" : "AND DATE(t1.invoice_date)<='$to'") . " " . ($territory=='' ? "" : "AND t7.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t4.customer= '$customer'") . "";






        }



//        if ($from == '' && $to == '' && $territory == '') {
//            $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    GROUP BY t5.item";
//        }if ($from == '' && $to == '' && $territory != '') {
//        $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE t7.Territory = '$territory'
//                    GROUP BY t5.item";
//    } elseif ($from != '' && $to != '' && $territory == '') {
//            $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE '$from'<=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to'
//                    GROUP BY t5.item";
//        }elseif ($from != '' && $to != '' && $territory != '') {
//        $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE '$from'<=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t7.Territory = '$territory'
//                    GROUP BY t5.item";
//    } elseif ($from != '' && $to == '' && $territory == '') {
//            $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE '$from'<=DATE(t1.invoice_date)
//                    GROUP BY t5.item";
//        }elseif ($from != '' && $to == '' && $territory != '') {
//        $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency,t4.customer
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE '$from'<=DATE(t1.invoice_date) AND t7.Territory = '$territory'
//                    GROUP BY t5.item";
//    } elseif ($from == '' && $to != '' && $territory == ''){
//            $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE DATE(t1.invoice_date)<='$to'
//                    GROUP BY t5.item";
//        }elseif ($from == '' && $to != '' && $territory != ''){
//        $sql = "SELECT t5.item,t4.po_no,t1.invoice_no,t2.quantity,t2.net_value,(t2.quantity*t2.net_value) AS total_amount,t6.Currency
//                    FROM despatch t1
//                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
//                    JOIN po_pl t3 ON t2.po_pl_id= t3.po_pl_id
//                    JOIN purchase_order t4 ON t4.po_id = t3.po_id
//                    JOIN product t5 ON t5.product_id = t3.product_id
//                    JOIN principal t6 ON t1.supplier = t6.name
//                    JOIN customer t7 ON t4.customer = t7.name
//                    WHERE DATE(t1.invoice_date)<='$to' AND t7.Territory = '$territory'
//                    GROUP BY t5.item";
//    }


        $response = array();
        $con = $this->__construct();
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function pocustomerwiseprint($from,$to,$territory)
    {
        $response = array();
        $con = $this->__construct();
        if($from !='' && $to !='' && $territory != '') {
            $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE '$from' <=DATE(t1.po_date) AND DATE(t1.po_date)<='$to' AND t3.Territory = '$territory'
                GROUP BY t3.name";
        }if($from !='' && $to !='' && $territory == '') {
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE '$from' <=DATE(t1.po_date) AND DATE(t1.po_date)<='$to'
                GROUP BY t3.name";
    }elseif ($from =='' && $to =='' && $territory != ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE t3.Territory = '$territory'
                GROUP BY t3.name";
    }elseif ($from =='' && $to =='' && $territory == ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name

                GROUP BY t3.name";
    }elseif ($from !='' && $to =='' && $territory != ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE '$from' <=DATE(t1.po_date) AND t3.Territory = '$territory'
                GROUP BY t3.name";
    }elseif ($from !='' && $to =='' && $territory == ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE '$from' <=DATE(t1.po_date)
                GROUP BY t3.name";
    }elseif ($from =='' && $to !='' && $territory != ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE DATE(t1.po_date)<='$to' AND t3.Territory = '$territory'
                GROUP BY t3.name";
    }elseif ($from =='' && $to !='' && $territory == ''){
        $sql = "SELECT t3.name,t3.Territory,(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1 JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier = t4.name
                WHERE DATE(t1.po_date)<='$to'
                GROUP BY t3.name";
    }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function despatchcustomerwiseprint($from,$to,$territory)
    {
        $response = array();
        $con = $this->__construct();
        if($from !='' && $to !='' && $territory != '') {
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                JOIN customer t6 ON t4.customer = t6.name
                WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to'  AND t6.Territory = '$territory'
                GROUP BY t4.customer";
        }elseif($from !='' && $to !='' && $territory == '') {
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to'
                GROUP BY t4.customer";
        }elseif ($from =='' && $to =='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                JOIN customer t6 ON t4.customer = t6.name
                WHERE t6.Territory = '$territory'
                GROUP BY t4.customer";
        }elseif ($from =='' && $to =='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                GROUP BY t4.customer";
        }elseif ($from !='' && $to =='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                JOIN customer t6 ON t4.customer = t6.name
                WHERE '$from' <=DATE(t1.invoice_date) AND t6.Territory = '$territory'
                GROUP BY t4.customer";
        }elseif ($from !='' && $to =='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                WHERE '$from' <=DATE(t1.invoice_date)
                GROUP BY t4.customer";
        }elseif ($from =='' && $to !='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                JOIN customer t6 ON t4.customer = t6.name
                WHERE DATE(t1.invoice_date)<='$to' AND t6.Territory = '$territory'
                GROUP BY t4.customer";
        }elseif ($from =='' && $to !='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))+t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                WHERE DATE(t1.invoice_date)<='$to'
                GROUP BY t4.customer";
        }

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function commstatementcustomerwiseprint($from,$to,$territory)
    {
        $response = array();
        $con = $this->__construct();
        if($from !='' && $to !='' && $territory != '') {
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t7.Territory = '$territory'
                    GROUP BY t4.customer";
        }elseif($from !='' && $to !='' && $territory == '') {
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to'
                    GROUP BY t4.customer";
        }elseif ($from =='' && $to =='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE t7.Territory = '$territory'
                    GROUP BY t4.customer";
        }elseif ($from =='' && $to =='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    GROUP BY t4.customer";
        }elseif ($from !='' && $to =='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND t7.Territory = '$territory'
                    GROUP BY t4.customer";
        }elseif ($from !='' && $to =='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date)
                    GROUP BY t4.customer";
        }elseif ($from =='' && $to !='' && $territory != ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE DATE(t1.invoice_date)<='$to' AND t7.Territory = '$territory'
                    GROUP BY t4.customer";
        }elseif ($from =='' && $to !='' && $territory == ''){
            $sql = "SELECT t4.customer,SUM(t2.net_value*t2.quantity) AS total_amount, SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100))) AS Commission,t5.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN principal t5 ON t4.supplier = t5.name
                    JOIN product t6 ON t3.product_id = t6.product_id
                    JOIN customer t7 ON t4.customer = t7.name
                    WHERE DATE(t1.invoice_date)<='$to'
                    GROUP BY t4.customer";
        }

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function summaryorderreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from == '' && $to== '' && $territory == '' && $customer == ''){
            $sql = "    SELECT
                        t1.po_id,
                        t1.po_no,
                        t1.po_date,
                        SUM(t2.quantity * t2.net_value) AS PO_AMOUNT,
                        SUM(t2.delivered_quantity * t2.net_value) AS DESPATCH_AMOUNT,
                        SUM(t2.quantity * t2.net_value) - SUM(t2.delivered_quantity * t2.net_value) AS ORDER_BALANCE,
                        t1.customer,
                        t4.Currency
                        FROM
                        purchase_order t1
                        JOIN po_pl t2 ON
                        t1.po_id = t2.po_id
                        JOIN customer t3 ON
                        t1.customer = t3.name
                        JOIN principal t4 ON
                        t1.supplier = t4.name
                        GROUP BY
                        t1.po_id";
        }else{
            $sql = "SELECT
                        t1.po_id,
                        t1.po_no,
                        t1.po_date,
                        SUM(t2.quantity * t2.net_value) AS PO_AMOUNT,
                        SUM(t2.delivered_quantity * t2.net_value) AS DESPATCH_AMOUNT,
                        SUM(t2.quantity * t2.net_value) - SUM(t2.delivered_quantity * t2.net_value) AS ORDER_BALANCE,
                        t1.customer,
                        t4.Currency
                        FROM
                        purchase_order t1
                        JOIN po_pl t2 ON
                        t1.po_id = t2.po_id
                        JOIN customer t3 ON
                        t1.customer = t3.name
                        JOIN principal t4 ON
                        t1.supplier = t4.name
                       WHERE t1.visibility='Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t3.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                       GROUP BY t1.po_id";
        }

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }





    public function qtocprintreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from == '' && $to== '' && $territory == '' && $customer == ''){
            $sql = "SELECT t1.quotation_id,t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE t1.status='1'
                GROUP BY t1.quotation_no";
        }else{
            $sql = "SELECT t1.quotation_id,t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE t1.status='1' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.q_date)") . " " . ($to=='' ? "" : "AND DATE(t1.q_date)<='$to'") . " " . ($territory=='' ? "" : "AND t3.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "
                GROUP BY t1.quotation_no";
        }

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function poitemcustomerwiseprintreport($from,$to,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $customer == '') {
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE t1.visibility = 'Enabled'";
        }elseif($from =='' && $to =='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE t1.customer = '$customer' AND t1.visibility = 'Enabled'";

        }elseif($from !='' && $to !='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE '$from' <=DATE(t1.po_date) AND DATE(t1.po_date)<='$to' AND t1.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to !='' && $customer == ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE '$from' <=DATE(t1.po_date) AND DATE(t1.po_date)<='$to' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to =='' && $customer == ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE '$from' <=DATE(t1.po_date) AND t1.visibility = 'Enabled'";
        }elseif($from !='' && $to =='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE '$from' <=DATE(t1.po_date) AND t1.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        elseif($from =='' && $to !='' && $customer == ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE DATE(t1.po_date)<='$to'AND t1.visibility = 'Enabled'";
        }elseif($from =='' && $to !='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE DATE(t1.po_date)<='$to' AND t1.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function despatcheditemcustomerwiseprintreport($from,$to,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $customer == '') {
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE t1.visibility = 'Enabled'";
        }elseif($from =='' && $to =='' && $customer != ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE t4.customer = '$customer' AND t1.visibility = 'Enabled'";

        }elseif($from !='' && $to !='' && $customer != ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t4.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to !='' && $customer == ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND DATE(t1.invoice_date)<='$to' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to =='' && $customer == ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND t1.visibility = 'Enabled'";
        }elseif($from !='' && $to =='' && $customer != ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE '$from' <=DATE(t1.invoice_date) AND t4.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        elseif($from =='' && $to !='' && $customer == ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE DATE(t1.invoice_date)<='$to' AND t1.visibility = 'Enabled'";
        }elseif($from =='' && $to !='' && $customer != ''){
            $sql = "SELECT t5.item,t2.quantity,t2.net_value,t2.commission,ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS COMMISSION_AMOUNT,t6.Territory,t7.Currency
                    FROM despatch t1
                    JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                    JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                    JOIN purchase_order t4 ON t3.po_id = t4.po_id
                    JOIN product t5 ON t3.product_id = t5.product_id
                    JOIN customer t6 ON t4.customer = t6.name
                    JOIN principal t7 ON t4.supplier = t7.name
                    WHERE DATE(t1.invoice_date)<='$to' AND t4.customer = '$customer' AND t1.visibility = 'Enabled'";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function quoteditemcustomerwiseprintreport($from,$to,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $customer == '') {
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE t1.visibility = 'Enabled'";
        }elseif($from =='' && $to =='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE t1.customer = '$customer' AND t1.visibility = 'Enabled'";

        }elseif($from !='' && $to !='' && $customer != ''){
            $sql = "SELECT t3.item,t2.quantity,t2.net_value,t4.Territory,t5.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN customer t4 ON t1.customer = t4.name
                    JOIN principal t5 ON t1.supplier = t5.name
                    WHERE t1.visibility = 'Enabled'
                    WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to !='' && $customer == ''){
            $sql = "SELECT t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t1.visibility = 'Enabled'";
        }
        elseif($from !='' && $to =='' && $customer == ''){
            $sql = "SELECT t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE '$from' <=DATE(t1.q_date) AND t1.visibility = 'Enabled'";
        }elseif($from !='' && $to =='' && $customer != ''){
            $sql = "SELECT t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE '$from' <=DATE(t1.q_date) AND t1.visibility = 'Enabled' AND t1.customer= '$customer'
                GROUP BY t1.quotation_no";
        }
        elseif($from =='' && $to !='' && $customer == ''){
            $sql = "SELECT t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE DATE(t1.q_date)<='$to' AND t1.visibility = 'Enabled'";
        }elseif($from =='' && $to !='' && $customer != ''){
            $sql = "SELECT t1.q_date,t1.quotation_no,t1.q_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM new_quotation t1
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE DATE(t1.q_date)<='$to' AND t1.visibility = 'Enabled' AND t1.customer = '$customer'";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function qfrompprintreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '' && $customer == '') {
            $sql = "SELECT t1.quotation_id,t1.q_date,t1.principal_Qtn_Ref,t1.principal_Qtn_Ref,t1.principal_qtn_ref_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM q_from_princi_log t11
                JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE t1.visibility = 'Enabled'
                GROUP BY t1.quotation_no";
        }else{
            $sql = "SELECT t1.quotation_id,t1.q_date,t1.principal_Qtn_Ref,t1.principal_Qtn_Ref,t1.principal_qtn_ref_date,t3.name,t3.Territory,SUM(t2.quantity*t2.net_value) AS total_amount ,t1.enq_no,t1.enq_date,t4.Currency,t1.company,t1.supplier
                FROM q_from_princi_log t11
                JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                JOIN customer t3 ON t1.customer = t3.name
                JOIN principal t4 ON t1.supplier =t4.name
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.q_date)") . " " . ($to=='' ? "" : "AND DATE(t1.q_date)<='$to'") . " " . ($territory=='' ? "" : "AND t3.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "
                GROUP BY t1.quotation_no";
        }

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function qtoccustomerwiseprintreport($from,$to,$territory)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '') {
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE t1.status= '1'
                    GROUP BY t3.name";
        }if($from =='' && $to =='' && $territory != '') {
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE t1.status= '1' AND t3.Territory = '$territory'
                    GROUP BY t3.name";
    }elseif($from !='' && $to !='' && $territory != ''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t1.status = '1' AND t3.Territory = '$territory'
                    GROUP BY t3.name";
    }elseif($from !='' && $to !='' && $territory == ''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t1.status = '1'
                    GROUP BY t3.name";
    }
    elseif($from !='' && $to =='' && $territory ==''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND t1.status = '1'
                    GROUP BY t3.name";
    }elseif($from !='' && $to =='' && $territory !=''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND t1.status = '1' AND t3.Territory = '$territory'
                    GROUP BY t3.name";
    }
    elseif($from =='' && $to !='' && $territory == ''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE DATE(t1.q_date)<='$to' AND t1.status = '1'
                    GROUP BY t3.name";
    }elseif($from =='' && $to !='' && $territory != ''){
        $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM new_quotation t1
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE DATE(t1.q_date)<='$to' AND t1.status = '1' AND t3.Territory = '$territory'
                    GROUP BY t3.name";
    }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function qfrompcustomerwiseprintreport($from,$to,$territory)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '') {
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }elseif($from =='' && $to =='' && $territory != '') {
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE t3.Territory = '$territory' AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }elseif($from !='' && $to !='' && $territory !=''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t3.Territory = '$territory' AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }elseif($from !='' && $to !='' && $territory ==''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND DATE(t1.q_date)<='$to' AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }
        elseif($from !='' && $to =='' && $territory == ''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }elseif($from !='' && $to =='' && $territory != ''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE '$from' <=DATE(t1.q_date) AND t3.Territory = '$territory' AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }
        elseif($from =='' && $to !='' && $territory == ''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE DATE(t1.q_date)<='$to' AND  t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }elseif($from =='' && $to !='' && $territory != ''){
            $sql = "SELECT t3.name,SUM(t2.quantity*t2.net_value) AS total_amount ,t4.Currency
                    FROM q_from_princi_log t11
                    JOIN new_quotation t1 ON t11.principal_Qtn_Ref = t1.principal_Qtn_Ref
                    JOIN quotation_pl t2 ON t1.quotation_id = t2.quotation_id
                    JOIN customer t3 ON t1.customer = t3.name
                    JOIN principal t4 ON t1.supplier =t4.name
                    WHERE DATE(t1.q_date)<='$to' AND t3.Territory = '$territory' AND t1.visibility = 'Enabled'
                    GROUP BY t3.name";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function purchaseorderreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '' && $customer == '') {
              $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t5.Territory,SUM(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                  FROM purchase_order t1
                  LEFT JOIN po_pl t2 ON t1.po_id = t2.po_id
                  LEFT JOIN product t3 ON t2.product_id = t3.product_id
                  LEFT JOIN principal t4 ON t1.supplier = t4.name
                  LEFT JOIN customer t5 ON t1.customer = t5.name
                  WHERE t1.visibility = 'Enabled'
                  GROUP BY t1.po_no";
        }else{
         $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t5.Territory,SUM(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                    FROM purchase_order t1
                    LEFT JOIN po_pl t2 ON t1.po_id = t2.po_id
                    LEFT JOIN product t3 ON t2.product_id = t3.product_id
                    LEFT JOIN principal t4 ON t1.supplier = t4.name
                    LEFT JOIN customer t5 ON t1.customer = t5.name
                    WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "
                    GROUP BY t1.po_no";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function cancelledpurchaseorderreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '' && $customer == '') {
            $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t5.Territory,SUM(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                JOIN principal t4 ON t1.supplier = t4.name
                JOIN customer t5 ON t1.customer = t5.name
                WHERE t1.visibility = 'Cancelled'
                GROUP BY t1.po_no";
        }else{
            $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t5.Territory,SUM(t2.quantity*t2.net_value) AS total_amount,t4.Currency
                    FROM purchase_order t1
                    JOIN po_pl t2 ON t1.po_id = t2.po_id
                    JOIN product t3 ON t2.product_id = t3.product_id
                    JOIN principal t4 ON t1.supplier = t4.name
                    JOIN customer t5 ON t1.customer = t5.name
                    WHERE t1.visibility= 'Cancelled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.Customer = '$customer'") . "
                    GROUP BY t1.po_no";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function despatchreport($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();

        if($from =='' && $to =='' && $territory == '' && $customer == '') {
            $sql = "SELECT t1.despatch_id,t1.invoice_date,t1.invoice_no,t4.customer,SUM(t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                LEFT JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                LEFT JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                LEFT JOIN customer t6 ON t4.customer = t6.name
                WHERE t4.visibility ='Enabled'
                GROUP BY t1.invoice_no";
        }else{
            $sql = "SELECT t1.despatch_id,t1.invoice_date,t1.invoice_no,t4.customer,SUM(t2.quantity*t2.net_value) AS TOTAL_NETVALUE,t5.Currency
                FROM despatch t1
                LEFT JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                LEFT JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                LEFT JOIN customer t6 ON t4.customer = t6.name
                WHERE t4.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.invoice_date)") . " " . ($to=='' ? "" : "AND DATE(t1.invoice_date)<='$to'") . " " . ($territory=='' ? "" : "AND t6.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t4.customer= '$customer'") . "
                GROUP BY t1.invoice_no";
        }


        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function commissionstatement($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        if($from =='' && $to =='' && $territory == '' && $customer == '') {
            $sql = "SELECT t1.invoice_no,
  t1.invoice_date,
  t4.po_no,
  t4.customer,
  t6.item,
  t2.net_value,
  t2.quantity,
  SUM(t2.net_value * t2.quantity) AS total_amount,
  Sum(ROUND((((t2.quantity * t2.net_value) * t2.commission) / 100))) AS
  Commission,
  t5.Currency
FROM despatch t1
  LEFT JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
  LEFT JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
  RIGHT JOIN purchase_order t4 ON t3.po_id = t4.po_id
  LEFT JOIN principal t5 ON t4.supplier = t5.name
  LEFT JOIN product t6 ON t3.product_id = t6.product_id
WHERE t4.visibility = 'Enabled'
GROUP BY t6.item";
        }else{
            $sql = "SELECT t1.invoice_no,
  t1.invoice_date,
  t4.po_no,
  t4.customer,
  t6.item,
  t2.net_value,
  t2.quantity,
  SUM(t2.net_value * t2.quantity) AS total_amount,
  Sum(ROUND((((t2.quantity * t2.net_value) * t2.commission) / 100))) AS
  Commission,
  t5.Currency
FROM despatch t1
  LEFT JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
  LEFT JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
  RIGHT JOIN purchase_order t4 ON t3.po_id = t4.po_id
  LEFT JOIN principal t5 ON t4.supplier = t5.name
  LEFT JOIN product t6 ON t3.product_id = t6.product_id
WHERE t4.visibility = 'Enabled' ". ($from=='' ? "" : "AND '$from'<=DATE(t1.invoice_date)") . " " . ($to=='' ? "" : "AND DATE(t1.invoice_date)<='$to'") . " " . ($territory=='' ? "" : "AND t6.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t4.customer= '$customer'") . "
                GROUP BY t6.item";
        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function consolidatedprint($from,$to,$company,$principal)
    {

//        echo $from.'till'.$to;
        $response = array();
        $con = $this->__construct();
        if ($from == '' && $to == '' && $company == '' && $principal == '') {
            $sql = "SELECT t1.customer AS Customer,
  Sum(t2.net_value * t2.quantity) AS TOTAL_PO,
  Sum((SELECT Sum(despatch_pl.quantity * despatch_pl.net_value) AS `''`
  FROM despatch_pl
  WHERE despatch_pl.po_pl_id = t2.po_pl_id)) AS `TOTAL_DESPATCH`

FROM purchase_order t1
  LEFT JOIN po_pl t2 ON t1.po_id = t2.po_id
  LEFT JOIN product t3 ON t2.product_id = t3.product_id
  where t1.visibility = 'Enabled'
GROUP BY t1.customer
  ORDER BY t1.customer,
  t2.po_id,
  t2.po_pl_id,
  t2.product_id
";
        } elseif ($from == '' && $to == '' && $company != '' && $principal != '') {
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
              FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.company = '$company' AND t1.supplier = '$principal' GROUP BY t1.customer";
        } elseif ($from == '' && $to == '' && $company != '' && $principal == '') {
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.company = '$company' GROUP BY t1.customer";
        } elseif ($from == '' && $to == '' && $company == '' && $principal != ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.supplier = '$principal' GROUP BY t1.customer";
        }elseif($from != '' && $to != '' && $company == '' && $principal==''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                  FROM purchase_order t1
                  JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                  JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                  WHERE '$from'< DATE(t1.po_date) AND DATE(t1.po_date) < '$to' GROUP BY t1.customer";
        }elseif ($from !='' && $to != '' && $company !='' && $principal == ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
               JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.company = '$company' AND '$from' < DATE(t1.po_date) AND DATE(t1.po_date) < '$to' GROUP BY t1.customer";
        }elseif ($from !='' && $to != '' && $company =='' && $principal != ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.supplier = '$principal' AND '$from'< DATE(t1.po_date) AND DATE(t1.po_date) < '$to' GROUP BY t1.customer";
        }elseif($from =='' && $to !='' && $company !='' && $principal == ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t4.net_value * t4.quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t4.po_pl_id = t2.po_pl_id
                WHERE t1.company = '$company' AND DATE(t1.po_date) < '$to' GROUP BY t1.customer";
                }
        elseif($from == '' && $to != '' && $company == '' && $principal != ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t2.net_value * t2.delivered_quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.supplier = '$principal' AND DATE(t1.po_date)<'$to' GROUP BY t1.customer";
        }
        elseif($from != '' && $to == '' && $company != '' && $principal == ''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t2.net_value * t2.delivered_quantity) AS TOTAL_DESPATCH
              FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.company = '$company' AND '$from'<DATE(t1.po_date)  GROUP BY t1.customer";
                }
        elseif($from != '' && $to =='' && $company =='' && $principal !=''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t2.net_value * t2.delivered_quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.supplier = '$principal' AND '$from'<DATE(t1.po_date) GROUP BY t1.customer";
        }
        elseif($from != '' && $to !='' && $company != '' && $principal !=''){
        $sql = "SELECT t1.customer AS Customer,SUM(t2.net_value * t2.quantity) AS TOTAL_PO,SUM(t2.net_value * t2.delivered_quantity) AS TOTAL_DESPATCH
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.company = '$company' AND t1.supplier = '$principal' AND '$from'<DATE(t1.po_date) AND DATE(t1.po_date)<'$to' GROUP BY t1.customer";
}
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function checkdiff($product_id,$product_quantity,$product_value,$product_delivery,$product_delivered_quantity)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT po_id,
                       quantity,
                       net_value,
                       delivery
                       FROM po_pl
                       WHERE po_pl_id = 34

                       EXCEPT

                       SELECT po_id,
                       quantity,
                       net_value,
                       delivery
                       FROM po_pl
                       WHERE po_pl_id = 35";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function qprodprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM quotation_pl WHERE quotation_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function ocprodprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM oc_pl WHERE oc_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

    public function indentprodprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent_pl WHERE indent_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function invoicewisecommprint()
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.invoice_no,t1.invoice_date,t4.customer,t6.item,t2.net_value,t2.quantity,t2.net_value*t2.quantity AS total_amount, t2.commission,SUM(ROUND((((t2.quantity*t2.net_value)*t2.commission)/100)) AS commission_amount,t5.Currency
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                JOIN product t6 ON t3.product_id = t6.product_id
                GROUP BY t6.item,t1.invoice_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function annualreportpo($month1,$tag,$territory)
    {
//        echo $month1;
        $response = array();
        $response1 = array();
        $con = $this->__construct();
        $date = $month1;
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));


        $prevtotal = date( "m-Y", strtotime( $month1."-1 month" ) );

        $dateFormat = 'm-Y';
        $stringDate = $prevtotal;
        $date = DateTime::createFromFormat($dateFormat, $stringDate);
        $prevyear1 = $date->format('Y');
        $prevmonth1 = $date->format('m');


        $prevmonth= date('F', mktime(0, 0, 0, $prevmonth1, 10)); // March
        $monthwords= date('F', mktime(0, 0, 0, $month, 10)); // March


        $sql456 = "SELECT closing from balance WHERE month = '$prevmonth' AND year='$prevyear1' " . ($territory=='' ? "" : "AND territory = '$territory'") . " ";
        $result456 = mysqli_query($con, $sql456);
        while ($row456 = mysqli_fetch_array($result456, 1)) {
            $rowclosing=$row456['closing'];
        }
//        echo $rowclosing;


//      Numericals for order
        $sql = "SELECT (t2.quantity*t2.net_value) AS total_amount
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                JOIN customer t5 ON t1.customer = t5.name
                WHERE YEAR(t1.po_date) = '$year' AND MONTH(t1.po_date) = '$month' " . ($territory=='' ? "" : "AND territory = '$territory'") . "
                GROUP BY t1.po_no";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        foreach ($response as $data){
            $something[] = (int)$data['total_amount'];
        }

        $sumpo =  array_sum($something);
//
        //      Numericals for despatch
        $sqldespatch = "SELECT SUM(t2.quantity*t2.net_value) AS TOTAL_NETVALUE
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                JOIN principal t5 ON t4.supplier = t5.name
                WHERE YEAR(t1.invoice_date) = '$year' AND MONTH(t1.invoice_date) = '$month' " . ($territory=='' ? "" : "AND territory = '$territory'") . "
                GROUP BY t1.invoice_no ";
        $resultdespatch = mysqli_query($con, $sqldespatch);
        while ($rowdespatch = mysqli_fetch_array($resultdespatch, 1)) {
            array_push($response1, $rowdespatch);
        }
//echo json_encode($response1);



        foreach ($response1 as $datadespatch){
            $something1[] = (int)$datadespatch['TOTAL_NETVALUE'];
        }

        $sumdespatch =  array_sum($something1);


        $closing = $rowclosing + $sumpo -$sumdespatch;

        $final=array();
        $final['month'] = $month1;
        $final['opening'] = $rowclosing;
        $final['total_po'] = $sumpo;
        $final['total_despatch'] = $sumdespatch;
        $final['closing']= $closing;

//        echo json_encode($final);
        $sql45678 = "SELECT * from balance WHERE month = '$monthwords' AND year='$year' " . ($territory=='' ? "" : "AND territory = '$territory'") . " ";
        $execute = mysqli_query($con, $sql45678);
        $rrows = mysqli_num_rows($execute);
        if($rrows == 0){
            $sqlinsert = "INSERT INTO balance(`tag`,`month`,`year`,`territory`,`opening`,`total_po`,`total_despatch`,`closing`,`created`)VALUES ('$tag','$monthwords','$year'," . ($territory== '' ? "NULL" : "'$territory'") . ",'$rowclosing','$sumpo','$sumdespatch','$closing',NOW())";
            $result = mysqli_query($con,$sqlinsert);
        }else{
            $sqlupdate = "UPDATE `balance` SET `opening`='$rowclosing',`total_po`=$sumpo,`total_despatch`='$sumdespatch',`closing`='$closing' WHERE `month` = '$monthwords' AND `year` = '$year' " . ($territory=='' ? "" : "AND territory = '$territory'") . " " ;
            $result = mysqli_query($con,$sqlupdate);}
        return $final;

    }



    public function annualreportprint($tag,$territory)
    {

        $response = array();
        $con = $this->__construct();
        if($territory == ''){
            $sql = "SELECT * FROM balance WHERE tag = '$tag' AND territory IS NULL";
        }
        else{
            $sql = "SELECT * FROM balance WHERE tag = '$tag' AND territory ='$territory' ";

        }
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }


    public function invoicewisecommprint1($from,$to,$customer,$territory)
    {

        $response = array();
        $con = $this->__construct();

          $sql = "SELECT
          t4.invoice_no AS          inv_no,
          t4.invoice_date AS        invoice_date,
          t1.despatch_pl_id,
          t5.customer AS            customer,
          t5.company AS             company,
          t5.supplier AS 			supplier,
          t6.name AS 				cust_name,
          t6.Territory AS 			territoiry,
          t1.despatch_id   as       despatch_id,
          t3.item AS 					item,
          t3.make  AS					 make,
          t1.quantity AS 				QUANTITY,
          t1.net_value AS RATE,
          t1.commission AS commRate,
          ROUND((t1.quantity * t1.net_value)) AS despatch_VALUE,
          ROUND(((t1.quantity * t1.net_value) * t1.commission) / 100) AS
          Total_Commission
        FROM despatch_pl t1
          LEFT JOIN po_pl t2 ON t1.po_pl_id = t2.po_pl_id
          LEFT JOIN product t3 ON t2.product_id = t3.product_id
          RIGHT JOIN despatch t4 ON t4.despatch_id = t1.despatch_id
          LEFT JOIN purchase_order t5 ON t2.po_id = t5.po_id
          INNER JOIN customer t6 ON t5.customer = t6.name

      WHERE t5.visibility='Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t4.invoice_date)") . "
              " . ($to=='' ? "" : "AND DATE(t4.invoice_date)<='$to'") . " " . ($territory=='' ? "" : "AND t6.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t5.customer = '$customer'") . "




        ORDER BY inv_no,
          t1.despatch_pl_id,
          t5.customer";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function pendingpodetailedprint1($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t2.territory
                FROM purchase_order t1
                JOIN customer t2 ON t1.customer = t2.name
                JOIN po_pl t3 ON t1.po_id = t3.po_id
                WHERE t1.visibility= 'Enabled' AND t3.quantity != t3.delivered_quantity " . ($from=='' ? "" : "'$from'<=DATE(t1.po_date) AND") . " " . ($to=='' ? "" : "DATE(t1.po_date)<='$to' AND") . " " . ($territory=='' ? "" : "t5.Territory = '$territory' AND") . " " . ($customer=='' ? "" : "t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.po_id";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function podetailedprint1($from,$to,$territory,$customer)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_id,t1.po_no,t1.po_date,t1.customer,t2.territory
                FROM purchase_order t1
                JOIN customer t2 ON t1.customer = t2.name
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.customer ASC";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function podetailedprint2($from,$to,$customer,$territory,$things)
    {
//        echo json_encode($things);

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.*,
                SUM(t2.quantity*t2.net_value) AS PO_AMOUNT,
                SUM(t3.delivered_quantity*t3.net_value) AS DESPATCH_AMOUNT,
                SUM(t2.quantity*t2.net_value)-SUM(t3.delivered_quantity*t3.net_value) AS BALANCED_AMOUNT
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN despatch_pl t3 ON t2.po_pl_id = t2.po_pl_id
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.customer ASC ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
//         echo json_encode($response);
        for($i=0;$i<count($things);$i++){
             $response[$i]['product'] = $things[$i];
//        echo json_encode($response);
        }
        return $response;

    }

/* New combined function starts here*/
    public function podetailedprintfinal($from,$to,$customer,$territory)
    {
//        echo json_encode($things);

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.*,
                SUM(t2.quantity*t2.net_value) AS PO_AMOUNT,
                SUM(t3.quantity*t3.net_value) AS DESPATCH_AMOUNT,
                SUM(t2.quantity*t2.net_value)-SUM(t2.delivered_quantity*t3.net_value) AS BALANCED_AMOUNT
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN despatch_pl t3 ON t2.po_pl_id = t2.po_pl_id
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.customer ASC ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $po_id[] = $row['po_id'];
            array_push($response, $row);
        }
//         echo json_encode($response);
//        echo count($po_id);
        foreach($po_id as $po_id2){

            $item_info[] = $this->detailedpoitem($po_id2);
//            $response[$i]['product'] = $things[$i];
        }
        for($i1=0;$i1<count($item_info);$i1++){
            $response[$i1]['product'] = $item_info[$i1];
        }
//        echo json_encode($response);
        return $response;

    }
/* New combined function end here*/

    /* New combined function for pending order starts here*/
    public function pendingpodetailedprintfinal($from,$to,$customer,$territory)
    {
//        echo json_encode($things);

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.*,
                SUM(t2.quantity*t2.net_value) AS PO_AMOUNT,
                SUM(t3.quantity*t2.net_value) AS DESPATCH_AMOUNT,
                SUM(t2.quantity*t2.net_value)-SUM(t3.quantity*t3.net_value) AS BALANCED_AMOUNT
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN despatch_pl t3 ON t2.po_pl_id = t2.po_pl_id
                WHERE t2.quantity-t2.delivered_quantity !=0 AND t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.customer ASC ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $po_id[] = $row['po_id'];
            array_push($response, $row);
        }
//         echo json_encode($response);
//        echo count($po_id);
        foreach($po_id as $po_id2){

            $item_info[] = $this->pendingdetailedpoitem($po_id2);
//            $response[$i]['product'] = $things[$i];
        }
        for($i1=0;$i1<count($item_info);$i1++){
            $response[$i1]['product'] = $item_info[$i1];
        }
//        echo json_encode($response);
        return $response;

    }
    /* New combined function for pending order end here*/






    public function pendingpodetailedprint2($from,$to,$customer,$territory,$things)
    {
//        echo json_encode($things);

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.*,
                SUM(t2.quantity*t2.net_value) AS PO_AMOUNT,
                SUM(t3.delivered_quantity*t3.net_value) AS DESPATCH_AMOUNT,
                SUM(t2.quantity*t2.net_value)-SUM(t3.delivered_quantity*t3.net_value) AS BALANCED_AMOUNT
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN despatch_pl t3 ON t2.po_pl_id = t2.po_pl_id
                WHERE t1.visibility= 'Enabled' " . ($from=='' ? "" : "AND '$from'<=DATE(t1.po_date)") . " " . ($to=='' ? "" : "AND DATE(t1.po_date)<='$to'") . " " . ($territory=='' ? "" : "AND t5.Territory = '$territory'") . " " . ($customer=='' ? "" : "AND t1.customer = '$customer'") . "
                GROUP BY t1.po_id ORDER BY t1.po_id ASC ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        // echo json_encode($response);
        // echo count($things);
        // echo count($response);
        for($i=0;$i<count($things);$i++){
            $response[$i]['product'] = $things[$i];
        }
        return $response;

    }
    public function invoicewisecommprint2($things)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.*,t4.customer
                FROM despatch t1
                JOIN despatch_pl t2 ON t1.despatch_id = t2.despatch_id
                JOIN po_pl t3 ON t2.po_pl_id = t3.po_pl_id
                JOIN purchase_order t4 ON t3.po_id = t4.po_id
                GROUP BY t1.invoice_no
                ORDER BY t1.despatch_id ASC";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        // echo json_encode($response);
        // echo count($response);
        // echo 'things'.count($things);
        for($i=0;$i<count($things);$i++){
            $response[$i]['product'] = $things[$i];
        }
        return $response;

    }

    public function invoicewisecommprodprint($despatch_id)
    {
//echo $despatch_id;
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.despatch_id,t3.item as item,t3.make,t1.quantity*t1.net_value AS AMOUNT,t1.commission,SUM(ROUND(((t1.quantity*t1.net_value)*t1.commission)/100)) AS Commission
                FROM despatch_pl t1
                JOIN po_pl t2 ON t1.po_pl_id = t2.po_pl_id
                JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.despatch_id = '$despatch_id'
                GROUP BY t2.po_pl_id
                ORDER BY t1.despatch_id ASC";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    //    New Function for invoice wise comm ends here. Changes made to the GROUP BY clause from t3.item to t2.po_pl_id




    public function detailedpoitem($po_id)
    {
//        echo $po_id."\n";
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_id,t3.item AS item, t3.make, t2.quantity,t2.net_value,t2.delivered_quantity,t2.quantity-t2.delivered_quantity AS BALANCED_QUANTITY
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                WHERE t1.po_id = '$po_id'";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
//        echo json_encode($response);
        return $response;

    }
    public function pendingdetailedpoitem($po_id)
    {
//        echo $po_id."\n";
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_id,t3.item AS item, t3.make, t2.quantity,t2.net_value,t2.delivered_quantity, t2.quantity * t2.net_value AS PO_AMOUNT, t2.delivered_quantity*t2.net_value AS DESPATCH_AMOUNT, t2.quantity-t2.delivered_quantity AS BALANCED_QUANTITY
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                LEFT JOIN despatch_pl t4 ON t2.po_pl_id = t4.po_pl_id
                WHERE t1.po_id = '$po_id'  ";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
//        echo json_encode($response);
        return $response;

    }
    public function detailedpendingpoitem($po_id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT t1.po_id,t3.item AS item, t3.make, t2.quantity,t2.net_value,t2.delivered_quantity, t2.quantity * t2.net_value AS PO_AMOUNT, t4.quantity*t4.net_value AS DESPATCH_AMOUNT, t2.quantity-t4.quantity AS BALANCED_QUANTITY
                FROM purchase_order t1
                JOIN po_pl t2 ON t1.po_id = t2.po_id
                JOIN product t3 ON t2.product_id = t3.product_id
                JOIN despatch_pl t4 ON t2.po_pl_id = t4.po_pl_id
                WHERE t1.po_id = '$po_id' GROUP BY t3.item ORDER BY t1.po_id ASC";

        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
//        echo json_encode($response);
        return $response;

    }
    public function printtheprods($product_id,$quantity)
    {
        $main_arr = array();
        $merge_array = array();
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }

        for($i=0;$i<count($response);$i++){
            $response[$i]['quantity'] = $quantity;
        }



        return $response;


    }
    public function printtheprodsquotation($product_id,$quantity,$value,$delivery)
    {
        $main_arr = array();
        $merge_array = array();
        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM product WHERE product_id = '$product_id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }

        for($i=0;$i<count($response);$i++){
            $response[$i]['quantity'] = $quantity;
            $response[$i]['value'] = $value;
            $response[$i]['delivery'] = $delivery;
        }
        return $response;


    }
    public function quotationprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation WHERE quotation_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function getqno($qno)
    {

//        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM new_quotation WHERE quotation_no = '$qno'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $response=$row['quotation_id'];
//            exit();
//            array_push($response, $row);
        }
        return $response;

    }
    public function getpono($po_no)
    {

//        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_no = '$po_no'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $response=$row['po_id'];
//            exit();
//            array_push($response, $row);
        }
        return $response;

    }
    public function getpo($po)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM purchase_order WHERE po_no= '$po'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {

            array_push($response, $row);
        }
        return $response;

    }
    public function ocprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM order_confirmation WHERE oc_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }
    public function indentprint($id)
    {

        $response = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM indent WHERE indent_id = '$id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            array_push($response, $row);
        }
        return $response;

    }

//print pdf function ends

//delete functions starts
    public function deleteproductlist($id)
    {
        $con=$this->__construct();
        $query = "DELETE FROM enq_pl WHERE enq_pl_id = '$id'";
        $execute = mysqli_query($con,$query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteproductlistpo($id)
    {
        $con=$this->__construct();
        $query = "DELETE FROM po_pl WHERE po_pl_id = '$id'";
        $execute = mysqli_query($con,$query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteproductlistoc($id)
    {
        $con=$this->__construct();
        $query = "DELETE FROM oc_pl WHERE oc_pl_id = '$id'";
        $execute = mysqli_query($con,$query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteproductlistindent($id)
    {
        $con=$this->__construct();
        $query = "DELETE FROM indent_pl WHERE indent_pl_id = '$id'";
        $execute = mysqli_query($con,$query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteproductlistquotation($id)
    {
        $con=$this->__construct();
        $query = "DELETE FROM quotation_pl WHERE quotation_pl_id = '$id'";
        $execute = mysqli_query($con,$query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

//delete functions ends

//count section starts

    public function countenq1($year, $month)
    {
        $con=$this->__construct();
        $query = "SELECT * from new_enq where MONTH(actual_date)=$month AND YEAR(actual_date)=$year";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countpo1($year, $month)
    {
        $con=$this->__construct();
        $query = "SELECT * from purchase_order where MONTH(po_date)=$month AND YEAR(po_date)=$year";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countdespatch1($year, $month)
    {
        $con=$this->__construct();
        $query = "SELECT * from despatch where MONTH(invoice_date)=$month AND YEAR(invoice_date)=$year";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countoutstanding1($year, $month)
    {
        $con=$this->__construct();
        $query = "
                  SELECT *
                  FROM `purchase_order` as t1
                  JOIN po_pl t2 ON t1.po_id = t2. po_id
                  WHERE abs(t2.quantity - t2.delivered_quantity)>0 AND MONTH(po_date)=$month AND YEAR(po_date)=$year";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countenq()
    {
        $con=$this->__construct();
        $query = "SELECT * FROM new_enq WHERE visibility = 'Enabled'";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countquotation()
    {
        $con=$this->__construct();
        $query = "SELECT * FROM new_quotation WHERE visibility = 'Enabled'";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countpo()
    {
        $con=$this->__construct();
        $query = "SELECT * FROM purchase_order WHERE visibility = 'Enabled'";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
    public function countindent()
    {
        $con=$this->__construct();
        $query = "SELECT * FROM indent WHERE visibility = 'Enabled'";
        $result = mysqli_query($con,$query);
        $rows = mysqli_num_rows($result);
        return $rows;
    }
//count section ends


    public function adddespatch($inovice_no,$invoice_date,$hawb,$hawb_date,$mawb,$mawb_date,$flight1,$flight1_date,$flight2,$flight2_date,$company,$supplier,$report)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO despatch(invoice_no, invoice_date, hawb_no, hawb_date, mawb_no, mawb_date, flight1, flight1_date, flight2, flight2_date, company, supplier,upload, created) VALUES ('$inovice_no','$invoice_date','$hawb'," . ($hawb_date==NULL ? "NULL" : "'$hawb_date'") . ",'$mawb'," . ($mawb_date==NULL ? "NULL" : "'$mawb_date'") . ",'$flight1'," . ($flight1_date==NULL ? "NULL" : "'$flight1_date'") . ",'$flight2'," . ($flight2_date==NULL ? "NULL" : "'$flight2_date'") . ",'$company','$supplier','$report', NOW())";
        $result = mysqli_query($con,$sql);
        $last_id = mysqli_insert_id($con);
        if ($result) {
            return $last_id;
        } else {
            return false;
        }
    }
    public function adddespatchpl($despatch_id,$po_pl_id,$quantity,$net_value,$commission)
    {
        $con = $this->__construct();
        $sql = "INSERT INTO despatch_pl(despatch_id, po_pl_id, quantity, net_value, commission, created) VALUES ('$despatch_id','$po_pl_id','$quantity','$net_value','$commission', NOW())";
        $execute = mysqli_query($con,$sql);

        if ($execute) {
            return true;
        } else {
            return false;
        }
    }






    public function updatequantity($po_pl_id, $quantity)
    {
        $con = $this->__construct();
        $sql1 = "SELECT * FROM po_pl WHERE po_pl_id = '$po_pl_id'";
        $execute1 = mysqli_query($con,$sql1);
        $row = mysqli_fetch_array($execute1, 1);
        $old=$row['delivered_quantity'];

        $old = (int)$old;
        $quantity = (int)$quantity;
        $original = $old+$quantity;

        $sql = "UPDATE po_pl SET delivered_quantity = '$original' WHERE po_pl_id = '$po_pl_id'";

        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }
    // **************************** edit functions***********************************

// ********************************delete functions********************
    public function deletecompany($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `company` SET `visibility` = 'Deleted' WHERE `c_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteenquiry($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `new_enq` SET `visibility` = 'Deleted' WHERE `enq_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletedespatch($id)
    {
        $con = $this->__construct();
        $sql = "DELETE FROM `despatch` WHERE `despatch_id` = '$id'";
        $execute = mysqli_query($con,$sql);

        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletedespatch_pl($po_pl_id,$quantity,$despatch_pl)
    {
        $response = array();
        $response1 = array();
        $con = $this->__construct();
        $sql = "SELECT * FROM po_pl WHERE po_pl_id = '$po_pl_id'; ";
        $result = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_array($result, 1)) {
            $oldquantity = (int)$row['delivered_quantity'] ;
            array_push($response, $row);
        }

        $quantity = (int)$quantity;
        $original = $oldquantity - $quantity;
        $response1['original_quantity']= $original;
        $response1['po_pl_id']= $po_pl_id;
        $sql12 = "DELETE FROM `despatch_pl` WHERE `despatch_pl_id` = '$despatch_pl'";
        $execute12 = mysqli_query($con,$sql12);
        if ($execute12) {
            return $response1;
        } else {
            return false;
        }

    }
    public function despatchquant($id)
    {
        $response = array();
        $con = $this->__construct();
        $sql1 = "SELECT * from despatch_pl WHERE `despatch_id` = '$id'";
        $execute1 = mysqli_query($con,$sql1);

        while ($row = mysqli_fetch_array($execute1, 1)) {
            array_push($response, $row);
        }

        if ($execute1 != "") {
            return $response;
        } else {
            return false;
        }
    }
    public function updatepoquantity($po_pl_id,$quantity)
    {

        $con = $this->__construct();
        $sql = "UPDATE `po_pl` SET `delivered_quantity` = '$quantity' WHERE `po_pl_id` = '$po_pl_id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteindent($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `indent` SET `visibility` = 'Deleted' WHERE `indent_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletepo($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `purchase_order` SET `visibility` = 'Deleted' WHERE `po_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteoc($id)
    {
        $con = $this->__construct();
        echo $sql = "UPDATE `order_confirmation` SET `visibility` = 'Deleted' WHERE `oc_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletelc($id)
    {
        $con = $this->__construct();
        echo $sql = "UPDATE `lc_details` SET `visibility` = 'Deleted' WHERE `lc_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletequotation($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `new_quotation` SET `visibility` = 'Deleted' WHERE `quotation_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function cancelenquiry($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `new_enq` SET `visibility` = 'Cancelled' WHERE `enq_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function cancelindent($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `indent` SET `visibility` = 'Cancelled' WHERE `indent_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function cancelpo($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `purchase_order` SET `visibility` = 'Cancelled' WHERE `po_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function canceloc($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `order_confirmation` SET `visibility` = 'Cancelled' WHERE `oc_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function cancellc($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `lc_details` SET `visibility` = 'Cancelled' WHERE `lc_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function cancelquotation($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `new_quotation` SET `visibility` = 'Cancelled' WHERE `quotation_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteterritory($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `territory` SET `visibility` = 'Deleted' WHERE `t_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletecustomer($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `customer` SET `visibility` = 'Deleted' WHERE `cust_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletecurrency($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `currency_master` SET `visibility` = 'Deleted' WHERE `CURRENCY_ID` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletemake($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `make_master` SET `visibility` = 'Deleted' WHERE `make_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteproduct($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `product` SET `visibility` = 'Deleted' WHERE `product_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deletesubgroup($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `subgroup_master` SET `visibility` = 'Deleted' WHERE `subg_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteprincipal($id)
    {
        $con = $this->__construct();
        $sql = "UPDATE `principal` SET `visibility` = 'Deleted' WHERE `p_id` = '$id'";
        $execute = mysqli_query($con,$sql);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    //************************************** random string function**************************************
    public function random_string($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }
    public function getdatetime()
    {
        $now = DateTime::createFromFormat('U.u', microtime(true));
        $date = $now->format("mdYHisu");
        return $date;
    }
    /*--------------------------------------fondoooooo stufff---------------------*/
    //List All Events


}
