<?php
date_default_timezone_set("Asia/Kolkata");

/**
 * Created by PhpStorm.
 * User: Homepage-DSK1
 * Date: 11-06-2016
 * Time: 18:28
 */
class fondo1
{
    public $conn;

    public function __construct()
    {
        $this->conn = mysql_connect(HOST, USER, PASS) or die("<br/>Could not connect to MySQL server");
        mysql_select_db(DB, $this->conn) or die("<br/>Could not select the indicated database");

        return $this->conn;
    }

    public function __destruct()
    {
        mysql_close($this->conn);
    }
    public function totalevent()
    {
        $query = "SELECT * FROM eventinfo";
        $execute = mysql_query($query);
        $rows = mysql_num_rows($execute);
        return $rows;
    }

    public function geteventdetails($eventid)
    {
        $query = "SELECT * FROM eventinfo WHERE `id` = '$eventid'";
        $execute = mysql_query($query);
        $response = array();
        while ($row = mysql_fetch_array($execute, 1)) {
            $venueid = $row['venueid'];
            $catid = $row['category'];
            $windowtime = $row['windowtime'];
            $category = $this->getcategoryname($catid);
            $venuedetail = $this->getvenuedetail($venueid);
            $ticketdetail = $this->getticketdetail($eventid, $windowtime);
            $special = $this->getspecial($eventid);
            $row1 = array(
                "event" => $row,
                "category" => $category,
                "venuedetail" => $venuedetail,
                "ticketdetail" => $ticketdetail,
                "special" => $special
            );
            array_push($response, $row1);
        }
        return $response;

    }


    public function getcategoryname($catid)
    {
        $query = "SELECT catname FROM categories where id = '$catid'";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $response = $row['catname'];
        return $response;
    }

    public function getvenuedetail($venueid)
    {
        $query = "SELECT * FROM venue where venueid = '$venueid'";
        $execute = mysql_query($query);
        $response = array();
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }

    public function getticketdetail($eventid, $windowtime)
    {

        $query = "SELECT * FROM eventtickets where eventid = '$eventid'";
        $execute = mysql_query($query);
        $response = array();
        while ($row = mysql_fetch_array($execute, 1)) {
            $eticketid = $row['id'];
            $data = $this->gettickets($eventid, $eticketid, $windowtime);
            //echo json_encode($data);
            $iarray = array("eventticket" => $row, "tickets" => $data);

            array_push($response, $iarray);
        }
        return $response;
    }

    public function gettickets($eventid, $eticketid, $windowtime)
    {

        $query = "SELECT * FROM tickets WHERE eventid = '$eventid' AND eventticketid = '$eticketid' AND parking != 'unparked'";
        $execute = mysql_query($query);
        $response = array();
        while ($row = mysql_fetch_array($execute, 1)) {
            // array_push($response, $row);
            $ticketid = $row['ticketid'];
            $parking = $row['parking'];
            $created = $row['created'];
            //echo $created;
            //echo $windowtime;
            //echo json_encode($parking);
            if ($parking == 'booked') {
                $data1 = $this->getassigned($ticketid);
                $splitpay_status=$this->getsplitpay1($ticketid);
                $marray = array("ticket" => $row, "status" => $parking, "splitpay"=> $splitpay_status,"assigned" => $data1);
                array_push($response, $marray);
            } else if ($parking == 'parked') {
                $add_min = date("Y-m-d H:i:s", strtotime($created . "+$windowtime minutes"));
                $NOW = date("Y-m-d H:i:s");
                if ($add_min < $NOW) {
                    $data1 = $this->getassigned($ticketid);
                    $splitpay_status=$this->getsplitpay1($ticketid);
                    $marray = array("ticket" => $row, "status" => $parking, "splitpay"=>$splitpay_status, "assigned" => $data1);
                    array_push($response, $marray);
                }
            }

        }
        return $response;
    }

    public function getassigned($ticketid)
    {
        $query = "SELECT * FROM assigned where ticketid = '$ticketid' AND status !='returned'";
        $execute = mysql_query($query);
        $response = array();
        while ($row = mysql_fetch_array($execute, 1)) {
            $phone = $row['assignedby'];
            $username = $this->getusername($phone);
            $marray = array("username" => $username, 'tid' => $row['tid'], 'tickphone' => $row['tickphone'], 'ticketid' => $row['ticketid'], 'status' => $row['status'], 'assignedby' => $row['assignedby'], 'modified' => $row['modified']);
            array_push($response, $marray);

        }
        return $response;
    }

    public function getspecial($eventid)
    {
        $query = "SELECT special FROM `special` WHERE `eventid` = '$eventid' ";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $response = $row['special'];
        return $response;
    }

    public function getusername($phone)
    {
        $query = "SELECT name AS username FROM `userprofile` WHERE `phone` = '$phone' ";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $response = $row['username'];
        return $response;
    }

    //Listing Homescreen
    public function listhomescreen($homeid)
    {
        $response = array();
        $query = "SELECT * FROM homescreen WHERE id = '$homeid'";
        $execute = mysql_query($query);
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }

//                          Updating the special names 29/06/2016
    public function getspecial1($special, $eventid)
    {
        $query = "SELECT * FROM homescreen WHERE id = $special";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $specialname = $row['name'];
        $update = $this->updatespecial($eventid, $specialname, $special);
        if ($update == "true") {
            return true;
        } else {
            return false;
        }
    }

    public function updatespecial($eventid, $specialname, $special)
    {
        $query = "UPDATE `special` SET `homeid` = '$special', `special` = '$specialname' WHERE `eventid` = '$eventid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }




//
    //updating Event
    public function editeventinfo($eventid, $eventname, $venue, $image, $description, $category, $windowtime, $note, $priority, $visibility, $special)
    {
        $specialinsert = $this->getspecial1($special, $eventid);
        $query = "UPDATE `eventinfo` SET `name` = '$eventname', `venueid` = '$venue', `coverimage` = '$image', `description`='$description', `category`='$category',`windowtime`='$windowtime',`note`='$note',`visibility` = '$visibility', `priority` = '$priority' WHERE `id` = '$eventid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function editeventinfo1($eventid, $eventname, $venue, $description, $category, $windowtime, $note, $priority, $visibility, $special)
    {
        $specialinsert = $this->getspecial1($special, $eventid);
        $query = "UPDATE eventinfo SET name = '$eventname', venueid = '$venue', description = '$description', category = '$category', windowtime = '$windowtime', note = '$note',visibility = '$visibility', priority = '$priority' WHERE id = '$eventid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    //Updating Homescreen
    public function edithomescreen($homename, $image, $visibility, $priority, $sponsored, $homeid)
    {
        $query = "UPDATE `homescreen` SET `name` = '$homename', `image` = '$image', `visibility` = '$visibility', `priority` = '$priority', `sponsored` = '$sponsored' WHERE `id` = '$homeid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function edithomescreen1($homename, $visibility, $priority, $sponsored, $homeid)
    {
        $query = "UPDATE `homescreen` SET `name` = '$homename',`visibility` = '$visibility', `priority` = '$priority', `sponsored` = '$sponsored' WHERE `id` = '$homeid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    //listing category
    public function listcategories($catid)
    {
        $response = array();
        $query = "SELECT * FROM categories WHERE id ='$catid'";
        $execute = mysql_query($query);
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }

    //update category
    public function editcategories($catname, $visibility, $priority, $catid)
    {
        $query = "UPDATE `categories` SET `catname` = '$catname', `visibility` = '$visibility', `priority` = '$priority' WHERE `id` = '$catid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    //List Venue
    public function listvenue($venueid)
    {
        $response = array();
        $query = "SELECT * FROM venue WHERE venueid = '$venueid'";
        $execute = mysql_query($query);
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }

    //Update Venue
    public function editvenue($venuename, $venueadd, $venuelat, $venuelong, $image, $venuecity, $venueid)
    {
        $query = "UPDATE `venue` SET `venuename` = '$venuename', venuecity = '$venuecity', `venueaddress` = '$venueadd', `venuelat` = '$venuelat', `venuelong` = '$venuelong', `venueimage` = '$image' WHERE `venueid` = '$venueid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function editvenue1($venuename, $venueadd, $venuelat, $venuelong, $venuecity, $venueid)
    {
        $query = "UPDATE `venue` SET `venuename` = '$venuename', venuecity = '$venuecity', `venueaddress` = '$venueadd', `venuelat` = '$venuelat', `venuelong` = '$venuelong' WHERE `venueid` = '$venueid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function random_string($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

//                                    EVENT FUNCTIONS
    public function displayeventticket($eventticketid)
    {
        $response = array();
        $query = "SELECT * FROM eventtickets WHERE id = '$eventticketid'";
        $execute = mysql_query($query);
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }

    public function deleteeventticket($eventticket)
    {
        $query = "UPDATE `eventtickets` SET `visibility` = '2' WHERE `id` = '$eventticket'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function editeventticket($ticketname, $ticketdesc, $ticketprice, $nooftickets, $eventdatetime, $visibility, $ticketid)
    {

        $query = "UPDATE `eventtickets` SET `ticketname` = '$ticketname', `ticketdesc` = '$ticketdesc', `ticketprice` = '$ticketprice', `nooftickets`='$nooftickets', `eventdatetime`='$eventdatetime',`visibility` = '$visibility' WHERE `id` = '$ticketid'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function updatesponsoredhome($requestno, $status)
    {

        $query = "UPDATE `homescreen` SET `sponsored` = '$status' WHERE `id` = '$requestno'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }

    public function createbanner($eventid, $itype, $image)
    {
        $query = "INSERT INTO eventbannerimages ( eventid, itype, image, visibility ,created) VALUES ( '$eventid','$itype','$image', '0', NOW());";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }
    }
    public function getcreated($splitid)
    {
        $query = "SELECT * FROM `tickets` WHERE `splitid` = '$splitid'";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $created = $row['created'];
        return $created;
    }
    public function getwindowtime($splitid)
    {
        $query = "SELECT * FROM tickets where splitid = '$splitid'";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $eventid = $row['eventid'];
        $data1 = $this->windowtime($eventid);
        return $data1;
    }
    public function windowtime($eventid)
    {
        $query = "SELECT * FROM eventinfo where eventid = '$eventid'";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $data = $row['windowtime'];
        return $data;
    }
     public function getsplitpay1($ticketid)
    {
        $query = "SELECT * FROM splitpay WHERE ticketid = '$ticketid'";
        $execute = mysql_query($query);
        $row = mysql_fetch_array($execute, 1);
        $response=$row['status'];
        return $response;
    }
    public function getsplitpay($splitid)
    {
        $response = array();
        $query = "SELECT * FROM splitpay WHERE splitid = '$splitid' AND status ='paid' ";
        $execute = mysql_query($query);
        while ($row = mysql_fetch_array($execute, 1)) {
            array_push($response, $row);
        }
        return $response;
    }
    public function updaterefund($requestno, $status)
    {

        $query = "UPDATE `splitpay` SET `refund` = '$status' WHERE `id` = '$requestno'";
        $execute = mysql_query($query);
        if ($execute) {
            return true;
        } else {
            return false;
        }

    }
}