<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/8/18
 * Time: 2:33 AM
 */

include "config/config.php";
include "class/agency.php";

include "section/checksession.php";


$obj = new agency();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Quotation to customer</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">


                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <?php
                            $q_no = $_REQUEST['id'];
                            //                          echo $enq_no;
                            $data = $obj->quotationdetails($q_no);
                            //                                                                                    echo json_encode($data);

                            foreach ($data as $things){
                                $enq_no = $things['enq_no'];
                                $quotation_id = $things['quotation_id'];
                            }


                            $enq_id = $obj->sendenqid($enq_no);




                            ////////new stufff



                            $prod_data1 = $obj->showproductlistquotation1($quotation_id);
                            //                            echo json_encode($prod_data1);

                            foreach($data as $test){
                                $prod_data = $obj->showproductlist($quotation_id);
                                foreach ($prod_data1 as $something) {

                                    $so[] = $something['product_id'];
                                    $tot_amount[] = $something['TOTAL_AMOUNT'];
                                }
                                $tot_amount = array_sum($tot_amount);

                                $comma_separated = implode(",", $so);
                                $alltheprods = $obj->listproduct1($comma_separated);

//                                echo json_encode($prod_data);
                                $test_decode=json_decode($test['product_list']);
                                $data1=array();
                                foreach ($prod_data1 as $things){
                                    $quotation_pl_id=$things['quotation_pl_id'];
                                    $product_id=$things['product_id'];
                                    $quantity=$things['quantity'];
                                    $value1 =$things['net_value'];
                                    $delivery1 =$things['delivery'];

                                    $real_prod_data []= $obj->showproductdetails($product_id,$quantity,$value1,$delivery1,$quotation_pl_id);
                                }
                            }



                            $customer_data= $obj->listcustomer();
                            $company_data= $obj->listcompany();
                            $supplier_data= $obj->listprincipal();




                            ?>
                            <div class="x_content">

                                <?php
                                foreach ($data as $data1) {



                                ?>

                                <form id="addquotationcustomer" name="addquotationcustomer" method="post"  class="form-horizontal form-label-left" action="./adminapi/quotation/add_quotation_customer.php">
                                    <input id="q_id" class="form-control col-md-3 col-xs-3" name="q_id" value="<?php echo $data1['quotation_id']; ?>" type="hidden">
                                    <input id="enq_id" class="form-control col-md-3 col-xs-3" name="enq_id" value="<?php echo $enq_id ?>" type="hidden">

                                    <span class="section"><h3>Send Quotation to Customer </h3></span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="quotation_no">Quotation No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="q_no" class="form-control col-md-3 col-xs-3" name="q_no" value="<?php echo $data1['quotation_no']; ?>" required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' name="date1" value="<?php echo $data1['q_date']; ?>" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-3 col-xs-3" name="enq_no" value="<?php echo $data1['enq_no']; ?>" required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" value="<?php echo $data1['enq_date']; ?>" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="principal_qtn_ref">Principal Qtn Ref <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="principal_qtn_ref" class="form-control col-md-3 col-xs-3" name="principal_qtn_ref" value="<?php echo $data1['principal_Qtn_Ref']; ?>" required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker3'>
                                            <input type='text' name="date3" class="form-control" value="<?php echo $data1['principal_qtn_ref_date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="validity">Validity <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="validity" class="form-control col-md-3 col-xs-3" name="validity" value="<?php echo $data1['validity']; ?>" required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="despatch">Despatch <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="despatch" class="form-control col-md-3 col-xs-3" name="despatch" value="<?php echo $data1['despatch']; ?>" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment">Payment<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="payment" class="form-control col-md-7 col-xs-12" name="payment" value="<?php echo $data1['payment']; ?>" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer">Customer<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="customer" class="form-control">
                                                <?php

                                                foreach ($customer_data as $cust)
                                                { ?>


                                                    <option  value='<?php  echo $cust['name']; ?>' <?php if($cust['name'] == $data1['customer']) { ?> selected <?php } ?>><? echo $cust['name'];?> </option>

                                                <? } ?>
                                            </select>                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="company" class="form-control">
                                                <?php

                                                foreach ($company_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data1['company']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                             </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="supplier" class="form-control">
                                                <?php

                                                foreach ($supplier_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data1['supplier']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                                </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="note">Note</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <textarea id="note" name="note" class="form-control col-md-3 col-xs-3"><?php echo $data1['note']; ?></textarea>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="remark">Remark
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <textarea id="remark"  name="remark" class="form-control col-md-3 col-xs-3"><?php echo $data1['remark']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="project">Project
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="project" class="form-control col-md-3 col-xs-3" name="project" value="<?php echo $data1['project']; ?>" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Due Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                            <input type='text' name="date4" class="form-control" value="<?php echo $data1['due_date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>

                                    <?php
                                    }
                                    ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Rate Per Unit</th>
                                        <th class="column-title">Delivery</th>
                                        <th class="column-title">Manage</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php


                                    //                                    echo json_encode($real_prod_data);

                                    foreach($real_prod_data as $item){


                                        ?>
                                        <tr>


                                            <td>
                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $item[0]['pl_id']; ?>" type="hidden">
                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $item[0]['product_id']; ?>" type="hidden">
                                                <?php echo $item[0]['make']; ?></td>
                                            <td><?php echo $item[0]['item']; ?></td>
                                            <td><?php echo $item[0]['unit']; ?></td>
                                            <td><?php echo $item[0]['currency']; ?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" name="quantity[]" value="<?php echo $item[0]['quantity']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" name="value[]" value="<?php echo $item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" name="delivery[]" value="<?php echo $item[0]['delivery']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><a href="#" data-id="<?php echo $item[0]['pl_id']; ?>" id="bb1"
                                                   class="btn bb1"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                                <h1>Total Quotation Amount :- <?php echo number_format($tot_amount); ?></h1><br>
                                <h2>Add new product</h2>
                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>

                                        </th>
                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Rate Per Unit</th>
                                        <th class="column-title">Delivery</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($alltheprods as $data){ ?>
                                        <tr>
                                            <td><input type="checkbox" name="prodid[]" value="<?echo $data['product_id'];?>"></td>
                                            <td><?php echo $data['make'];?>  </td>
                                            <td><?php echo $data['item'];?></td>
                                            <td><?php echo $data['unit'];?></td>
                                            <td><?php echo $data['currency'];?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" name="quantity[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" name="value[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" name="delivery[]" type="text">
                                                </div>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-10">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addquotationcustomer").on('submit', (function (e) {
        var form = document.getElementById("addquotationcustomer");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/quotation/editquotationcustomer.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data != 'success') {
                    toastr["success"]("Successfully Updated Quotation", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './quotationcustomer_list.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));

    $(".bb1").click(function (e) {
        e.preventDefault();
        var val1 = $(this).data('id');
        var tr = $(this).closest('tr');

        if (val1 != "") {
            $.ajax({
                type: "POST",
                url: './adminapi/quotation/delete_productlist.php',
                data: ({idinfo: val1}),
                success: function (data) {
                    console.log(data);
                    if (data == "success") {
                        //alert(data);
                        tr.remove();
                        toastr["success"]("Successfully Deleted Product", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Product", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Product missing", "Agency Administrator");
        }
    });


</script>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });


</script>
</body>
</html>

