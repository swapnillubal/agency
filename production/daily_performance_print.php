<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/1/19
 * Time: 2:17 AM
 */
include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$date = $_REQUEST['date1'];


$dateprint = new DateTime($date);
$dateprint1= $dateprint->format('d.m.y');

$currentdate = date('d.m.y');
$currenttime = date('H:i:s');


$company = 'APEX PRECISION AGENCIES';
$supplier = 'THK CO.,LTD';

$enquiry = $obj->dailyenq($date);
$quotationp=$obj->dailyqp($date);
$quotationc=$obj->dailyqc($date);
$po=$obj->dailypo($date);
$oc_p=$obj->dailyoc_p($date);
$oc_c=$obj->dailyoc_c($date);
$indent=$obj->dailyindent($date);
$lc=$obj->dailylc($date);
$despatch=$obj->dailydespatch($date);


//echo 'enq'.json_encode($enquiry);
//echo 'qp'.json_encode($quotationp);
//echo 'qc'.json_encode($quotationc);
//echo 'po'.json_encode($po);
//echo 'oc p'.json_encode($oc_p);
//echo 'oc c'.json_encode($oc_c);
//echo 'indent'.json_encode($indent);
//echo 'lc'.json_encode($lc);
//echo 'despatch'.json_encode($despatch);

class PDF extends FPDF{




    // Page header
    public function Header()
    {

        $this->SetFont('Arial','B',12);
        $this->Cell(80 ,5,'',0,1);
        $this->Cell(80 ,5,$GLOBALS['company'],0,0);
        $this->Cell(100 ,5,'Daily Performance Report',0,1,'R');
        $this->SetFont('Arial','',8);
//        $this->Cell(50 ,5,'NET :-',0,0,'L');
        $this->SetFont('Arial','B',10);
        $this->Cell(40 ,5,'Date : '.$GLOBALS['dateprint1'],0,0,'L');
        $this->Cell(70 ,6,$GLOBALS['supplier'],0,0,'C');
        $this->Cell(80 ,5,'Date :'.$GLOBALS['currentdate'].'    '.'Time : '.$GLOBALS['currenttime'],0,1,'C');
//        $this->Cell(180 ,5,'',0,1);

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}




$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

//Cell(width , height ,text,border ,end line , [align])


//$pdf->Cell(20 ,8,'',1,0);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20 ,5,'',0,1);

//$x = $pdf->GetX();
//$y = $pdf->GetY();
//
////            $pdf->SetXY($x + 149, $y);
//
//$pdf->Line($x, $y, $x+180,$y);

$pdf->Cell(30 ,5,'Ref. No.',0,0);
$pdf->Cell(70 ,5,'Name of Customer',0,0);
$pdf->Cell(40 ,5,'Total Amount',0,0,'C');
$pdf->Cell(40 ,5,'Reference',0,1);

$x = $pdf->GetX();
$y = $pdf->GetY();

$pdf->Line($x, $y, $x+180,$y);
$pdf->Cell(180 ,5,'',0,1);


if(!empty($enquiry)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Enquiry',0,1);
    $pdf->SetFont('Arial','',9);
    foreach ($enquiry as $enquiry1){
    $pdf->Cell(30 ,5,$enquiry1['enq_no'],0,0);
    $pdf->Cell(70 ,5,$enquiry1['customer'],0,0);
    $pdf->Cell(40 ,5,'00',0,1,'C');
        $pdf->Cell(180 ,5,'',0,1);
    }
}
if(!empty($quotationp)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Qtn From Principal',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($quotationp);$i++){

        $sumqp[]=(int)$quotationp[$i]['TOTAL_AMOUNT'];

    }
    $sum1qp=array_sum($sumqp);
    foreach ($quotationp as $quotationp1){
        $pdf->Cell(30 ,5,$quotationp1['quotation_no'],0,0);
        $pdf->Cell(70 ,5,$quotationp1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($quotationp1['TOTAL_AMOUNT']),0,0,'C');

        $pdf->Cell(30 ,5,$quotationp1['enq_no'],0,1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->Line($x+100, $y, $x+130,$y);
        $pdf->Cell(100 ,5,'',0,0);
        $pdf->Cell(40 ,5,$sum1qp,0,1,'C');
        $pdf->Line($x+100, $y+5, $x+130,$y+5);

        $pdf->Cell(180 ,5,'',0,1);
    }
}
if(!empty($quotationc)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Qtn To Customer',0,1);
    $pdf->SetFont('Arial','',9);

    for ($i=0;$i<count($quotationc);$i++){

         $sumqc[]=(int)$quotationc[$i]['TOTAL_AMOUNT'];

    }
    $sum1qc=array_sum($sumqc);

    foreach ($quotationc as $quotationc1){
        $pdf->Cell(30 ,5,$quotationc1['quotation_no'],0,0);
        $pdf->Cell(70 ,5,$quotationc1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($quotationc1['TOTAL_AMOUNT']),0,0,'C');
        $pdf->Cell(30 ,5,$quotationc1['enq_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
//        $pdf->Cell(110 ,5,'',0,1);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1qc),0,1,'C');

    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($po)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Purchase Order',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($po);$i++){

        $sumpo[]=(int)$po[$i]['TOTAL_AMOUNT'];

    }
    $sum1po=array_sum($sumpo);
    foreach ($po as $po1){
        $pdf->Cell(30 ,5,$po1['po_no'],0,0);
        $pdf->Cell(70 ,5,$po1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($quotationc1['TOTAL_AMOUNT']),0,0,'C');
        $pdf->Cell(30 ,5,$po1['principal_qtn_ref'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1po),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($oc_p)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'OC-Principal',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($oc_p);$i++){

        $sumocp[]=(int)$oc_p[$i]['TOTAL_AMOUNT'];

    }
    $sum1ocp=array_sum($sumocp);
    foreach ($oc_p as $oc_p1){
        $pdf->Cell(30 ,5,$oc_p1['order_no'],0,0);
        $pdf->Cell(70 ,5,$oc_p1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($oc_p1['TOTAL_AMOUNT']),0,0,'C');
        $pdf->Cell(30 ,5,$oc_p1['po_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1ocp),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($oc_c)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'OC-Customer',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($oc_c);$i++){

        $sumocc[]=(int)$oc_c[$i]['TOTAL_AMOUNT'];

    }
    $sum1occ=array_sum($sumocc);
    foreach ($oc_c as $oc_c1){
        $pdf->Cell(30 ,5,$oc_c1['order_no'],0,0);
        $pdf->Cell(70 ,5,$oc_c1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($oc_c1['TOTAL_AMOUNT']),0,0,'C');
        $pdf->Cell(30 ,5,$oc_c1['po_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1occ),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($indent)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Indent',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($indent);$i++){

        $sumindent[]=(int)$indent[$i]['TOTAL_AMOUNT'];

    }
    $sum1indent=array_sum($sumindent);
    foreach ($indent as $indent1){
        $pdf->Cell(30 ,5,$indent1['indent_no'],0,0);
        $pdf->Cell(70 ,5,$indent1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($indent1['TOTAL_AMOUNT']),0,0,'C');

        $pdf->Cell(30 ,5,$indent1['po_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1indent),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($lc)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'LC Details',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($lc);$i++){

        $sumlc[]=(int)$lc[$i]['value'];

    }
    $sum1lc=array_sum($sumlc);
    foreach ($lc as $lc1){
        $pdf->Cell(30 ,5,$lc1['lc_no'],0,0);
        $pdf->Cell(70 ,5,$lc1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($lc1['value']),0,0,'C');
        $pdf->Cell(30 ,5,$lc1['po_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1lc),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);

    $pdf->Cell(180 ,5,'',0,1);
}
if(!empty($despatch)){
    $pdf->SetFont('Arial','BU',10);
    $pdf->Cell(30 ,5,'Despatch',0,1);
    $pdf->SetFont('Arial','',9);
    for ($i=0;$i<count($despatch);$i++){

        $sumdespatch[]=(int)$despatch[$i]['TOTAL_AMOUNT'];

    }
    $sum1despatch=array_sum($sumdespatch);
    foreach ($despatch as $despatch1){
        $pdf->Cell(30 ,5,$despatch1['invoice_no'],0,0);
        $pdf->Cell(70 ,5,$despatch1['customer'],0,0);
        $pdf->Cell(40 ,5,number_format($despatch1['TOTAL_AMOUNT']),0,0,'C');
        $pdf->Cell(30 ,5,$despatch1['po_no'],0,1);
    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    $pdf->Line($x+100, $y, $x+140,$y);
    $pdf->Cell(100 ,5,'',0,0);
    $pdf->Cell(40 ,5,number_format($sum1despatch),0,1,'C');
    $pdf->Line($x+100, $y+5, $x+140,$y+5);
    $pdf->Cell(180 ,5,'',0,1);
}

$pdf->Output();

?>
