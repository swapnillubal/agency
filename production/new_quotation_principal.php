<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/6/18
 * Time: 10:29 PM
 */

include "config/config.php";
include "class/agency.php";

include "section/checksession.php";


$obj = new agency();




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quotation</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">


                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <?php






                            $enq_no = $_REQUEST['id'];
                            $data = $obj->showenquiry1($enq_no);
//                            echo json_encode($data);
                            foreach ($data as $ite) {
                                $enq_id = $ite['enq_id'];
                                $enq_no = $ite['enq_no'];
                            }

                        //    echo $enq_id;
                            $prod_data = $obj->showproductlist($enq_id);


                            $alldata = $obj->listproduct();
//                            echo json_encode($prod_data);
                            $prod_data = $obj->showenquiry($enq_no);
                            //                                echo json_encode($data);
                            $drawing=$obj->showdrawing($enq_no);
                            //echo json_encode($drawing);

                            foreach($data as $test){
                                $enq_id = $test['enq_id'];
                                $prod_data = $obj->showproductlist($enq_id);
                                foreach ($prod_data as $something) {

                                     $so[] = $something['product_id'];
                                }
                                 $comma_separated = implode(",", $so);
$alltheprods = $obj->listproduct1($comma_separated);

//                                echo json_encode($prod_data);

                                $data1=array();
                                foreach ($prod_data as $things){
                                    $pl_id=$things['quotation_pl_id'];
                                    $product_id=$things['product_id'];
                                    $quantity=$things['quantity'];
                                    $value1 =$things['net_value'];


                                    $real_prod_data []= $obj->showproductdetailsenq($product_id,$quantity,$pl_id);
                                }
                            }





                            $customer_data= $obj->listcustomer();
                            $company_data= $obj->listcompany();
                            $supplier_data= $obj->listprincipal();


                            $enq_no = $_REQUEST['id'];
                            //                          echo $enq_no;
//                            $data = $obj->showenquiry($enq_no);
                            //                            echo json_encode($data);
                            ?>
                            <div class="x_content">


                                <?php
//echo json_encode($data);
                                foreach ($data as $data1) {
//echo $data1['enq_id'];

                                ?>

                                <form id="addquotationprincipal" name="addquotationprincipal" method="post" action="./adminapi/quotation/add_quotation.php" class="form-horizontal form-label-left" >

                                    <span class="section"><h3>Send Quotation from Principal</h3></span>



                                    <input id="enq_id" class="form-control col-md-3 col-xs-3" name="enq_id"  value="<?php echo $data1['enq_id']; ?>" type="hidden">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="qtn_no">Principal Quotation Ref No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="p_qtn_no" class="form-control col-md-3 col-xs-3" name="p_qtn_no" required="required" type="text" onkeyup="checkname();" ><span id="name_status"></span>
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' id="date" name="date" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-3 col-xs-3" name="enq_no" value="<?php echo $data1['enq_no']; ?>" required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' id="date2" name="date2" class="form-control"  value="<?php echo $data1['date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>



                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="customer">Customer <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="customer" class="form-control">
                                                <?php

                                                foreach ($customer_data as $cust)
                                                { ?>


                                                    <option  value='<?php  echo $cust['name']; ?>' <?php if($cust['name'] == $data1['customer']) { ?> selected <?php } ?>><? echo $cust['name'];?> </option>

                                                <? } ?>
                                            </select>                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="company">Company <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="company" class="form-control">
                                                <?php

                                                foreach ($company_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data1['company']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                            </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="supplier" class="form-control">
                                                <?php

                                                foreach ($supplier_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data1['supplier']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                            </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="despatch">Despatch<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="customer" class="form-control col-md-7 col-xs-12" name="despatch" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment">Payment<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="customer" class="form-control col-md-7 col-xs-12" name="payment" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="remark" name="remark" class="form-control col-md-7 col-xs-12"></textarea>
                                        </div>
                                    </div>



                                    <?php
                                    }
                                    ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">Sr no. </th>
                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Rate Per Unit</th>
                                        <th class="column-title">Delivery</th>
                                        <th class="column-title">Manage</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    //echo json_encode($real_prod_data);
                                    //echo json_encode($prod_data);
                                    $i=0;
                                    foreach($real_prod_data as $new_item){
                                        $i++;
                                        ?>
                                        <tr>

                                            <td><?php echo $i; ?></td>
                                            <td>
                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $new_item[0]['pl_id']; ?>" type="hidden">
                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $new_item[0]['product_id']; ?>" type="hidden">
                                                <?php echo $new_item[0]['make']; ?></td>
                                            <td><?php echo $new_item[0]['item']; ?></td>
                                            <td><?php echo $new_item[0]['unit']; ?></td>
                                            <td><?php echo $new_item[0]['currency']; ?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" required name="quantity[]" value="<?php echo $new_item[0]['quantity']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" required name="value[]" value="<?php echo $new_item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" required name="delivery[]" value="<?php echo $new_item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><a href="#" data-id="<?php echo $new_item[0]['pl_id']; ?>" id="bb1"
                                                   class="btn bb1"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                                <h2>Add new product</h2>
                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>

                                        </th>
                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Rate per unit</th>
                                        <th class="column-title">Delivery </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($alltheprods as $data){ ?>
                                        <tr>
                                            <td><input type="checkbox" name="prodid[]" value="<?echo $data['product_id'];?>"></td>
                                            <td><?php echo $data['make'];?>  </td>
                                            <td><?php echo $data['item'];?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" name="quantity[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" name="value[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" name="delivery[]" type="text">
                                                </div>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-10">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addquotationprincipal").on('submit', (function (e) {
        var form = document.getElementById("addquotationprincipal");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/quotation/add_quotation.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Added New Quotation", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './quotation_from_principal.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));

    $(".bb1").click(function (e) {
        e.preventDefault();
        var val1 = $(this).data('id');
        var tr = $(this).closest('tr');
        tr.remove();
        toastr["success"]("Successfully Removed Product", "Agency Administrator");

    });
</script>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>
<script type="text/javascript">

    function checkname()
    {
        var p_qtn_no=document.getElementById( "p_qtn_no" ).value;

        if(p_qtn_no)
        {
            $.ajax({
                type: 'post',
                url: './adminapi/quotation/check_quotation_p.php',
                data: {
                    p_qtn_no:p_qtn_no
                },
                success: function (response) {
                    $( '#name_status' ).html(response);
                    if(response=="OK")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        }
        else
        {
            $( '#name_status' ).html("");
            return false;
        }
    }
</script>


</body>
</html>

