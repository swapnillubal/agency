<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/1/19
 * Time: 10:38 PM
 */


include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$year= $_REQUEST['year'];
$territory= $_REQUEST['territory'];

//$yearrange = date($year);
$year1 = $year+1;

$territory= $_REQUEST['territory'];

$company = 'APEX PRECISION AGENCIES';
$supplier = 'THK CO.,LTD';

//echo date( "m Y", strtotime( "January 2019 -1 month" ) );


$newTs = strtotime('-1 month', '1-1-2018');




$tag=$year.'-'.$year1;

//$months = array();
//$month = 'March 2018';
//$n = date('m',strtotime($month));
//
//
//for ($x = $n; $x < $n+ 12; $x++) {
//    $months[] = date('F'.$year, mktime(0, 0, 0, $x, 1));
//}
//echo json_encode($months);


$yearint = (int)$year;
$yearint1 = (int)$year1;

$startdateee = (int)$yearint.'-05-01';
$enddateee = (int)$yearint1.'-04-01';
//echo gettype($dateee);
 $start = $month = strtotime($startdateee);
$end = strtotime($enddateee);
while($month < $end)
{
    $months[] =date('F Y', $month);
    $month = strtotime("+1 month", $month);
}
//echo json_encode($months);

foreach ($months as $month){
    $data1[]=$obj->annualreportpo($month,$tag,$territory);

}

//echo json_encode($data123);





$data123=$obj->annualreportprint($tag,$territory);
//echo json_encode($data123);


class PDF extends FPDF{




    // Page header
    public function Header()
    {

        $this->SetFont('Arial','B',12);
        $this->Cell(80 ,5,'',0,1);
        $this->Cell(80 ,5,$GLOBALS['company'],0,0);
        $this->Cell(100 ,5,'Annual Orders & Despatch',0,1,'R');
        $this->SetFont('Arial','',8);
//        $this->Cell(50 ,5,'NET :-',0,0,'L');
        $this->SetFont('Arial','B',10);
        $this->Cell(60 ,5,"YEAR: ".$GLOBALS['year'].'-'.$GLOBALS['year1'],0,0);
        $this->Cell(90 ,5,$GLOBALS['supplier'],0,1,'R');

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}




$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

//Cell(width , height ,text,border ,end line , [align])


//$pdf->Cell(20 ,8,'',1,0);
$pdf->SetFont('Arial','',9);
$pdf->Cell(20 ,5,'',0,1);


$pdf->Cell(30 ,5,'Month',1,0);
$pdf->Cell(30 ,5,'Opening Balance',1,0);
$pdf->Cell(30 ,5,'Total Orders',1,0);
$pdf->Cell(30 ,5,'Total Despatch',1,0);
$pdf->Cell(30 ,5,'Balance',1,1);



foreach ($data123 as $data){
    $pdf->Cell(30 ,5,$data['month'],1,0);
    $pdf->Cell(30 ,5,number_format($data['opening']),1,0);
    $pdf->Cell(30 ,5,number_format($data['total_po']),1,0);
    $pdf->Cell(30 ,5,number_format($data['total_despatch']),1,0);
    $pdf->Cell(30 ,5,number_format($data['closing']),1,1);

}








$pdf->Output();



?>