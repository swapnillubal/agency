<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/6/18
 * Time: 3:49 PM
 */
include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();

$currency_name=$obj->listcurrency();
$make_name=$obj->listmake();
$subgroup_name=$obj->listsubgroup();
?>
<!DOCTYPE html>
<html lang="en">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>New Enquiry</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Enquiry</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
                                <div id="addproduct" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Modal Header</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form id="addproduct1" name="addproduct1"  method="post" class="form-horizontal form-label-left" novalidate>

                                                    <span class="section">Information</span>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="make">Make <span class="required">*</span>
                                                        </label>
                                                        <select class="col-md-6 col-sm-6 col-xs-12" name="make">
                                                            <?php
                                                            foreach ($make_name as $m_name){
                                                                ?>
                                                                <option value="<?php echo $m_name['make_name']; ?>"><?php echo $m_name['make_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Item">Item <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input id="Item" class="form-control col-md-7 col-xs-12" name="Item" required="required" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Unit">Unit <span class="required">*</span>
                                                        </label>
                                                        <select class="col-md-6 col-sm-6 col-xs-12" name="Unit">
                                                            <option value="KG">KG</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SET">SET</option>

                                                        </select>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subgroup">Subgroup<span class="required">*</span>
                                                        </label>
                                                        <select class="col-md-6 col-sm-6 col-xs-12" name="subgroup">
                                                            <?php
                                                            foreach ($subgroup_name as $subg_name){
                                                                ?>
                                                                <option value="<?php echo $subg_name['subg_name']; ?>"><?php echo $subg_name['subg_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="currency">Currency <span class="required">*</span>
                                                        </label>
                                                        <select class="col-md-6 col-sm-6 col-xs-12" name="currency">
                                                            <?php
                                                            foreach ($currency_name as $curr_name){
                                                                ?>
                                                                <option value="<?php echo $curr_name['CURRENCY_CODE']; ?>"><?php echo $curr_name['CURRENCY_CODE'].'-'.$curr_name['CURRENCY_NAME'].'-'.$curr_name['CURRENCY_SYMBOL']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description1">Description1 <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <textarea id="Description1" required="required" name="Description1" class="form-control col-md-7 col-xs-12"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description2">Description2 <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <textarea id="Description2" required="required" name="Description2" class="form-control col-md-7 col-xs-12"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description3">Description3 <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <textarea id="Description3" required="required" name="Description3" class="form-control col-md-7 col-xs-12"></textarea>
                                                        </div>
                                                    </div>



                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <?php
                                $data = $obj->showproductmake();
                                $customer_data= $obj->listcustomer();
                                $company_data= $obj->listcompany();
                                $supplier_data= $obj->listprincipal();
                                ?>
                                <form id="myForm" name="myForm" method="POST" enctype="multipart/form-data" class="form-horizontal form-label-left" >

                                    <span class="section">Information</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-7 col-xs-12" name="enq_no"  required="required" type="text" onkeyup="checkname();" ><span id="name_status"></span>
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date1">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' id="date1" name="date1" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>


                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer">Customer<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                           <select name="customer" class="form-control">
                                                <?php

                                                foreach ($customer_data as $cust)
                                            {?>
                                            <option value='<?php  echo $cust['name']; ?>'><? echo $cust['name'];?> </option>


                                            <? } ?>
                                           </select>
                                                           </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="remark" name="remark" class="form-control col-md-7 col-xs-12"></textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="company" class="form-control">
                                                <?php

                                                foreach ($company_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>'><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="supplier" class="form-control">
                                                <?php

                                                foreach ($supplier_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>'><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="subgroup">Enter Subgroup <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="subgroup" class="form-control col-md-7 col-xs-12" name="subgroup" type="text">
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date2">Actual Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' id="date2" name="date2" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>




                                        <?php
                                        $data1 = $obj->listproduct();


                                       // echo json_encode($data1);
//                                         echo json_encode($customer_data);
//                                         echo json_encode($company_data);
//                                         echo json_encode($supplier_data);


                                        ?>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addproduct">Add New</button>
                                                <div class="x_content">


                                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>

                                                            </th>
                                                            <th class="column-title">Make </th>
                                                            <th class="column-title">Item </th>
                                                            <th class="column-title">Unit </th>
                                                            <th class="column-title">Currency</th>
                                                            <th class="column-title">Quantity</th>


                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($data1 as $data){ ?>
                                                            <tr>
                                                                <td><input type="checkbox" name="check[]" value="<?echo $data['product_id'];?>"></td>
                                                                <td><?php echo $data['make'];?>  </td>
                                                                <td><?php echo $data['item'];?></td>
                                                                <td><?php echo $data['unit'];?></td>
                                                                <td><?php echo $data['currency'];?></td>
                                                                <td><div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <input id="quantity" class="form-control col-md-12 col-xs-12" name="quantity[]" type="text">
                                                                    </div>
                                                                </td>

                                                            </tr>

                                                        <?php } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-9 col-md-offset-9">
                                            <button type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#myForm").on('submit', (function (e) {
        var form = document.getElementById("myForm");

        e.preventDefault();

        $.ajax({

            url: "./adminapi/enquiry/add_enquiry.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Added New Enquiry", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './index.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>
<script>
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>
<!--<script>-->
<!--    $(document).ready(function() {-->
<!--        $('#example').DataTable( {-->
<!--            columnDefs: [ {-->
<!--                orderable: false,-->
<!--                className: 'select-checkbox',-->
<!--                targets:   0-->
<!--            } ],-->
<!--            select: {-->
<!--                style:    'os',-->
<!--                selector: 'td:first-child'-->
<!--            },-->
<!--            order: [[ 1, 'asc' ]]-->
<!--        } );-->
<!--    } );-->
<!--</script>-->
<script>
    $("#addproduct1").on('submit', (function (e) {
        var form = document.getElementById("addproduct1");

        e.preventDefault();
        $.ajax({
            url: "./adminapi/product/add_product.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully Added New Product", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './new_enquiry.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>

<script type="text/javascript">

    function checkname()
    {
        var enq_no=document.getElementById( "enq_no" ).value;

        if(enq_no)
        {
            $.ajax({
                type: 'post',
                url: './adminapi/enquiry/check_enq_no.php',
                data: {
                    enq_no:enq_no
                },
                success: function (response) {
                    $( '#name_status' ).html(response);
                    if(response=="OK")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        }
        else
        {
            $( '#name_status' ).html("");
            return false;
        }
    }
</script>





</body>
</html>

