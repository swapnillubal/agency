<?php

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$id=$_POST['id'];
$margin=$_POST['margin'];


//echo json_encode($signdetails);
$prod_data = $obj->enqprodprint($id);
//echo json_encode($prod_data);


$merge_array=array();
for($i=0;$i<count($prod_data);$i++){
    $merge_array[$i] =
        ['product_id'=>$prod_data[$i]['product_id'],
            'quantity'=>$prod_data[$i]['quantity'],

        ];
}

//echo json_encode($merge_array);

foreach ($merge_array as $things){
    $product_id = $things['product_id'];
    $quantity = $things['quantity'];

    $real_prod_data[] = $obj->printtheprods($product_id,$quantity);

}


//echo json_encode($real_prod_data);





foreach ($prod_data as $item_value) {


    $prodid[] = $item_value['product_id'];

}


$data1=$obj->enqprint($id);
//echo json_encode($data1['product_list']);
foreach ($data1 as $item) {
    $company = $item['company'];
    $supplier = $item['supplier'];
    $customer = $item['customer'];
    $enq_no= $item['enq_no'];
    $enq_date= $item['date'];

}





$data2=$obj->listcompany1($company);
foreach ($data2 as $item2) {
    $comp_address = $item2['address'];
    $comp_email = $item2['email'];
    $comp_website= $item2['website'];
    $comp_Tel= $item2['telephone'];
    $comp_fax= $item2['fax'];

}



$data3=$obj->listprincipal1($supplier);
foreach ($data3 as $item3) {
    $principal_name=$item3['name'];
    $principal_address=$item3['address'];
}
$data4=$obj->listcustomer1($customer);
foreach ($data4 as $item4) {
    $customer_name=$item4['name'];
    $customer_address=$item4['Address'];
}


class PDF extends FPDF{




    // Page header
    public function Header()
    {


        // $this->Cell(40 ,5,'',0,1);


        // Logo
        $this->SetFont('Arial','B',20);

//Cell(width , height ,text,border ,end line , [align])
        $this->Cell(40 ,5,'',0,0);
        $this->Cell(150 ,8,$GLOBALS['company'],0,1);
//$pdf->Cell(20 ,8,'',1,0);
        $this->SetFont('Arial','',10);
        $this->Cell(20 ,5,'',0,0);

        $this->Cell(190 ,5,$GLOBALS['comp_address'],0,1);

        if(strlen($GLOBALS['comp_email'])<30) {

            $this->Cell(60, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }else{
            $this->Cell(42, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }
        if(strlen($GLOBALS['comp_website'])<30){
            $this->Cell(60 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');
        }else{
            $this->Cell(40 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');

        }
        $strlen = strlen($GLOBALS['comp_fax']);
        if(0<$strlen && $strlen<20) {
            $this->Cell(42, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'] . ', Fax: ' . $GLOBALS['comp_fax'], 0, 1);
        }
        else{
            $this->Cell(62, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'], 0, 1);
        }
//        $this->Cell(180 ,5,'',0,1);

        $x = $this->GetX();
        $y = $this->GetY();
//
        $this->SetXY($x + 149, $y);

        $this->Line($x, $y, $x+180,$y);


        $this->Cell(180 ,5,'',0,1);

    }

    // Page footer
    function Footer()
    {

        $x = $this->GetX();
        $y = $this->GetY();

        $this->SetXY($x, $y);
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}


$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,25);

//$pdf->Line(10, 55, 195,55);
$x = $pdf->GetX();
$y = $pdf->GetY();

$pdf->SetXY($x, $y);
$pdf->Cell(140 ,5,'',0,1);
$pdf->Cell(140 ,5,'',0,0);
$pdf->SetFont('Arial','BU',18);

$pdf->Cell(40 ,5,'ENQUIRY',0,1,'R');
$pdf->SetFont('Arial','B',10);

$pdf->Cell(140 ,5,'',0,1);
$y = $pdf->GetY();
$pdf->SetFont('Arial','',10);
$pdf->Cell(40 ,5,'To,',0,1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40 ,5,$principal_name,0,1);

$pdf->SetFont('Arial','',10);
$pdf->MultiCell(90,5,$principal_address,0,'L');
//$pdf->Cell(180 ,8,'Our Ref: '.$q_no,1,1,'R');
$x = $pdf->GetX();
//$y = $pdf->GetY();
$date = new DateTime($enq_date);
$pdf->SetXY($x+90, $y);
$pdf->Cell(90 ,5,'OUR REF: '.$enq_no,0,1,'R');
$pdf->Cell(180 ,5,'DATE: '.$date->format('d.m.y'),0,1,'R');
$pdf->Cell(180 ,5,'',0,1);
$pdf->Cell(180 ,5,'',0,1);

$pdf->Cell(130 ,8,'Customer:    '.$customer_name,0,1);
$pdf->Cell(20 ,8,'',0,0);
$pdf->MultiCell(70,5,$customer_address,0,'L');

//$pdf->Line(10, 150, 195,150);
//$pdf->Line(10, 160, 195,160);
$pdf->Ln(4);

// $pdf->Cell(20 ,5,'',0,1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);

$pdf->Cell(20 ,5,'Sr No.',1,0);
$pdf->Cell(130 ,5,'Product Description',1,0);
$pdf->Cell(35 ,5,'Quantity',1,1);


$i=0;


//echo json_encode($real_prod_data);
foreach ($real_prod_data as $prod1){


//    echo $prod1[0]['description1'];
//
//  echo  $y = (string)$prod1[0]['make'];
// echo  $z = (string)$prod1[0]['item'];
//    echo $string = "$y"."-"."$z";
//    exit();


    $i++;
    if(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2']) && !empty($prod1[0]['description3'])) {
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(20, 5, $i."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+20, $y-20);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(130, 5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n". $string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-20);
        $pdf->MultiCell(35, 5, number_format($prod1[0]['quantity'])."\n\n\n\n", 1);
    }elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description3'])){
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(20, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+20, $y-15);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(130, 5,$prod1[0]['description1']."\n".$string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-15);
        $pdf->MultiCell(35, 5, number_format($prod1[0]['quantity'])."\n\n\n", 1);
    }
    elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2'])) {

        $y = $pdf->GetY();

        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(20, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->SetXY($x+20, $y-15);
        $pdf->MultiCell(130 ,5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n".$string,1);

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetXY($x+150 , $y-15);
        $pdf->MultiCell(35 ,5,number_format($prod1[0]['quantity'])."\n\n\n",1);

    }
    elseif(!empty($prod1[0]['description1'])){
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(20, 5, $i."\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+20, $y-10);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(130 ,5,$prod1[0]['description1']."\n".$string."\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-10);
        $pdf->MultiCell(35 ,5,number_format($prod1[0]['quantity'])."\n\n",1);
    }else{
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->Cell(20, 5, $i, 1);
        $pdf->Cell(130, 5, $prod1[0]['item'], 1, 0);
        $pdf->Cell(35, 5, number_format($prod1[0]['quantity']), 1, 1);
    }


//    $pdf->Cell(180 ,8,'total'.$prod1[0]['quantity']*$prod1[0]['value'],1,1);





//    original code starts here

//
//    if(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2']) && !empty($prod1[0]['description3'])) {
//        $y = $pdf->GetY();
//        if($y>240){
//            $pdf->Ln(100);
//        }
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x, $y);
//        $pdf->MultiCell(20, 5, $i."\n\n\n\n", 1);
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x+20, $y-20);
//        $string = $prod1[0]['make'].",".$prod1[0]['item'];
//        $pdf->MultiCell(130, 5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n". $string ."\n".$prod1[0]['description3'], 1);
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x+150, $y-20);
//        $pdf->MultiCell(35, 5, $prod1[0]['quantity']."\n\n\n\n", 1);
//
//    }elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2'])) {
//
//        $y = $pdf->GetY();
//
//        if($y>240){
//            $pdf->Ln(100);
//        }
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x, $y);
//        $pdf->MultiCell(20, 5, $i."\n\n\n", 1);
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//
//        $string = $prod1[0]['make'].",".$prod1[0]['item'];
//        $pdf->SetXY($x+20, $y-15);
//        $pdf->MultiCell(130 ,5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n".$string,1);
//
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//
//        $pdf->SetXY($x+150 , $y-15);
//        $pdf->MultiCell(35 ,5,$prod1[0]['quantity']."\n\n\n",1);
//
//    }
//    elseif(!empty($prod1[0]['description1'])){
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x, $y);
//        $pdf->MultiCell(20, 5, $i."\n\n", 1);
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x+20, $y-10);
//        $string = $prod1[0]['make'].",".$prod1[0]['item'];
//        $pdf->MultiCell(130 ,5,$prod1[0]['description1']."\n".$string."\n",1);
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//
//        $pdf->SetXY($x+150 , $y-10);
//        $pdf->MultiCell(35 ,5,$prod1[0]['quantity']."\n\n",1);
//
//    }else{
//        $x = $pdf->GetX();
//        $y = $pdf->GetY();
//        $pdf->SetXY($x, $y);
//        $pdf->Cell(10, 5, $i, 1);
//        $pdf->Cell(80, 5, $prod1[0]['item'], 1, 0);
//        $pdf->Cell(20, 5, $prod1[0]['quantity'], 1, 0);
//
//    }
//
//
//
//    original code ends here

}


//$pdf->AddPage();

$pdf->Output();



?>