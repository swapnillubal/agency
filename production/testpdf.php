<?php

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$id=$_GET['id'];

$data1=$obj->enqprint($id);
//echo json_encode($data1['product_list']);
foreach ($data1 as $item) {
    $company = $item['company'];
    $supplier = $item['supplier'];
    $customer = $item['customer'];
    $enq_no= $item['enq_no'];
    $enq_date= $item['date'];
    $prod[] = $item['product_list'];
}

//echo json_encode($data1);
//
//echo $customer;
//echo $supplier;
//echo $company;

$data2=$obj->listcompany1($company);
foreach ($data2 as $item2) {
    $comp_address = $item2['address'];
    $comp_email = $item2['email'];
    $comp_website= $item2['website'];
    $comp_Tel= $item2['telephone'];

}



$data3=$obj->listprincipal1($supplier);
foreach ($data3 as $item3) {
    $principal_name=$item3['name'];
    $principal_address=$item3['address'];
}
$data4=$obj->listcustomer1($customer);
foreach ($data4 as $item4) {
    $customer_name=$item4['name'];
    $customer_address=$item4['Address'];
}


$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',20);

//Cell(width , height ,text,border ,end line , [align])
$pdf->Cell(40 ,5,'',0,0);
$pdf->Cell(150 ,8,$company,0,1);
//$pdf->Cell(20 ,8,'',1,0);
$pdf->SetFont('Arial','',10);
$pdf->Cell(20 ,8,'',0,0);

$pdf->Cell(190 ,8,$comp_address,0,1);
$pdf->Cell(30 ,8,'',0,0);

$pdf->Cell(70 ,8,'Email: '.$comp_email,0,0);
$pdf->Cell(70 ,8,'Website: '.$comp_website,0,1);
$pdf->Cell(40 ,8,'',0,0);
$pdf->Cell(100 ,8,'Tel: '.$comp_Tel,0,1);

$pdf->Line(10, 55, 195,55);

$pdf->Cell(140 ,8,'',0,1);
$pdf->Cell(140 ,8,'',0,1);
$pdf->Cell(140 ,8,'',0,1);
$pdf->Cell(140 ,8,'',0,0);
$pdf->SetFont('Arial','BU',20);

$pdf->Cell(40 ,8,'ENQUIRY',0,1,'R');
$pdf->SetFont('Arial','B',10);

$pdf->Cell(180 ,8,'Enquiry No: '.$enq_no,0,1,'R');
$pdf->Cell(180 ,8,'Enquiry Date: '.$enq_date,0,1,'R');

$pdf->Cell(20 ,8,'To,',0,1);
$pdf->Cell(130 ,8,$principal_name,0,1);
//$pdf->Cell(50 ,8,$principal_address,1,1);
$pdf->MultiCell(60,5,$principal_address,0);
$pdf->Cell(130 ,8,'Customer: '.$customer_name,0,1);
$pdf->Cell(20 ,8,'',0,0);
$pdf->MultiCell(70,5,$customer_address,0,'L');

$pdf->Line(10, 150, 195,150);
$pdf->Line(10, 160, 195,160);

//$pdf->Cell(20 ,8,'',0,1);
$pdf->Cell(20 ,8,'',0,1);

$pdf->Cell(20 ,8,'Sr No.',0,0);
$pdf->Cell(130 ,8,'Product Description',0,0);
$pdf->Cell(50 ,8,'Quantity',0,1);

//echo json_encode($prod);

foreach($data1 as $test){
    $test_decode=json_decode($test['product_list']);
    $data1=array();
    foreach ($test_decode as $tmp=>$value){
        $product_id=$value->product_id;
        $quantity=$value->quantity;

        $dataprod[]= $obj->showproductdetails($product_id,$quantity);
    }
}

$i=0;
foreach ($dataprod as $prod1){
//echo $prod1[0]['make'];
    $i++;
        $pdf->Cell(20 ,8,$i,0,0);
        $pdf->Cell(130 ,8,$prod1[0]['item'],0,0);
        $pdf->Cell(50 ,8,$prod1[0]['quantity'],0,1);

}

$pdf->Output();



?>