<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/6/18
 * Time: 10:31 PM
 */




include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>New PO</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Purchase Order</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <?php
                            $q_no = $_REQUEST['id'];


                            $data1=$obj->quotationdetails($q_no);
//                            echo json_encode($data1);



                            foreach ($data1 as $things){
                                $enq_no = $things['enq_no'];
                                $quotation_id = $things['quotation_id'];
                            }


                            $enq_id = $obj->sendenqid($enq_no);





                            ////////new stufff




                            foreach($data1 as $test){
                                $prod_data = $obj->showproductlistquotation($quotation_id);
//                                echo json_encode($prod_data);
                                foreach ($prod_data as $something) {

                                    $so[] = $something['product_id'];
                                }

                                $comma_separated = implode(",", $so);

                                $alltheprods = $obj->listproduct1($comma_separated);

//                                echo json_encode($alltheprods);

                                foreach ($prod_data as $things){
                                    $quotation_pl_id=$things['quotation_pl_id'];
                                    $product_id=$things['product_id'];
                                    $quantity=$things['quantity'];
                                    $value1 =$things['net_value'];
                                    $delivery1 =$things['delivery'];

                                    $real_prod_data []= $obj->showproductdetailspurchaseorder($product_id,$quantity,$value1,$delivery1,$quotation_pl_id);
                                }
                            }




                            $customer_data= $obj->listcustomer();
                            $company_data= $obj->listcompany();
                            $supplier_data= $obj->listprincipal();




                            foreach ($data1 as $data){


                             ?>

                        <div class="x_content">

                                <form id="addPO" name="addPO" method="post"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="./adminapi/purchase_order/add_PO.php">
                                    <input id="q_id" class="form-control col-md-3 col-xs-3" name="q_id"  value="<?php echo $quotation_id; ?>" type="hidden">
                                    <span class="section">Information</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="po_no">PO No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="po_no" class="form-control col-md-3 col-xs-3" name="po_no"  required="required" type="text" onkeyup="checkname();" ><span id="name_status"></span>
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker'>
                                            <input type='text' name="date1" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-3 col-xs-3" name="enq_no" required="required" type="text" value="<?php echo $data['enq_no'];?>" >
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" class="form-control" value="<?php echo $data['enq_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="princi_qtn_ref_no">Principal's Qtn. Ref. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="princi_qtn_ref_no" class="form-control col-md-3 col-xs-3" name="princi_qtn_ref_no"  required="required" type="text" value="<?php echo $data['principal_Qtn_Ref'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker3'>
                                            <input type='text' name="date3" class="form-control" value="<?php echo $data['principal_qtn_ref_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="q_no">Our Qtn Ref. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-7 col-xs-12" name="q_no"  required="required" type="text" value="<?php echo $data['quotation_no'];?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                            <input type='text' name="date4" class="form-control" value="<?php echo $data['q_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
<!--customer-->
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="customer">Customer <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="customer" class="form-control">
                                                <?php

                                                foreach ($customer_data as $cust)
                                                { ?>


                                                    <option  value='<?php  echo $cust['name']; ?>' <?php if($cust['name'] == $data['customer']) { ?> selected <?php } ?>><? echo $cust['name'];?> </option>

                                                <? } ?>
                                            </select>                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="company">Company <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="company" class="form-control">
                                                <?php

                                                foreach ($company_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data['company']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                            </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <select name="supplier" class="form-control">
                                                <?php

                                                foreach ($supplier_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data['supplier']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>                                            </div>
                                    </div>

                                    <!--end-->

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="validity">Validity<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="validity" class="form-control col-md-3 col-xs-3" name="validity" type="text" value="<?php echo $data['validity'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="despatch">Despatch<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="despatch" class="form-control col-md-3 col-xs-3" name="despatch" type="text" value="<?php echo $data['despatch'];?>" >
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="payment">Payment<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="payment" class="form-control col-md-6 col-xs-3" name="payment" type="text" value="<?php echo $data['payment'];?>" >
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date5">Actual Receipt Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker5'>
                                            <input type='text' name="date5" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="remark">Remark
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <textarea id="remark" name="remark" class="form-control col-md-3 col-xs-3"><?php echo $data['remark'];?></textarea>
                                        </div>

                                    <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date6">Intimation Date<span class="required">*</span>
                                    </label>
                                    <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker6'>
                                        <input type='text' name="date6" class="form-control" />
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="upload">Upload<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="report" class="form-control col-md-7 col-xs-12" name="report" type="file">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md- col-sm-3 col-xs-3" for="date6">ETD 1
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker7'>
                                            <input type='text' name="date7" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="date6">ETD 2
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker8'>
                                            <input type='text' name="date8" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3" for="date6">ETD 3
                                    </label>
                                    <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker9'>
                                        <input type='text' name="date9" class="form-control" />
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Value</th>
                                        <th class="column-title">Delivery</th>
                                        <th class="column-title">Manage</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php


                                    //                                    echo json_encode($real_prod_data);

                                    foreach($real_prod_data as $item){


                                        ?>
                                        <tr>


                                            <td>
                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $item[0]['pl_id']; ?>" type="hidden">
                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $item[0]['product_id']; ?>" type="hidden">
                                                <?php echo $item[0]['make']; ?></td>
                                            <td><?php echo $item[0]['item']; ?></td>
                                            <td><?php echo $item[0]['unit']; ?></td>
                                            <td><?php echo $item[0]['currency']; ?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" name="quantity[]" value="<?php echo $item[0]['quantity']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" name="value[]" value="<?php echo $item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" name="delivery[]" value="<?php echo $item[0]['delivery']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><a href="#" data-id="<?php echo $item[0]['pl_id']; ?>" id="bb1"
                                                   class="btn bb1"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                                <h2>Add new product</h2>
                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>

                                        </th>
                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Rate Per Unit</th>
                                        <th class="column-title">Delivery</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($alltheprods as $data){ ?>
                                        <tr>
                                            <td><input type="checkbox" name="prodid[]" value="<?echo $data['product_id'];?>"></td>
                                            <td><?php echo $data['make'];?>  </td>
                                            <td><?php echo $data['item'];?></td>
                                            <td><?php echo $data['unit'];?></td>
                                            <td><?php echo $data['currency'];?></td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="quantity[]" class="form-control col-md-12 col-xs-12" name="quantity[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="value[]" class="form-control col-md-12 col-xs-12" name="value[]" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                    <input id="delivery[]" class="form-control col-md-12 col-xs-12" name="delivery[]" type="text">
                                                </div>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-10">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addPO").on('submit', (function (e) {
        var form = document.getElementById("addPO");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/purchase_order/add_PO.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                //alert(data);

                /// ***issue***
                if (data != "success") {
                    toastr["success"]("Successfully Added New Purchase Order", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './purchase_order.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
    $(".bb1").click(function (e) {
        e.preventDefault();
        var val1 = $(this).data('id');
        var tr = $(this).closest('tr');
        tr.remove();
        toastr["success"]("Successfully Removed Product", "Agency Administrator");

    });
</script>




<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker5').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker6').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker7').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker8').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker9').datetimepicker({
        format: 'YYYY-MM-DD'
    });


</script>

<script type="text/javascript">

    function checkname()
    {
        var po_no=document.getElementById( "po_no" ).value;

        if(po_no)
        {
            $.ajax({
                type: 'post',
                url: './adminapi/purchase_order/check_po.php',
                data: {
                    po_no:po_no
                },
                success: function (response) {
                    $( '#name_status' ).html(response);
                    if(response=="OK")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        }
        else
        {
            $( '#name_status' ).html("");
            return false;
        }
    }
</script>

</body>
</html>
