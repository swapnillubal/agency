<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/1/19
 * Time: 2:56 PM
 */


include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();


$from=$_REQUEST['date3'];
$to=$_REQUEST['date4'];
$territory=$_REQUEST['territory'];

$date = new DateTime($from);
$date1 = new DateTime($to);

$from1=$date->format('d.m.y');
$to1=$date1->format('d.m.y');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Commission Statement Customerwise</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!---->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.css"/>
    <!-- -->
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                    <h3>Commission Statement Customerwise Report</h3>

                    </div>


                </div>



                <div class="clearfix"></div>



                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Information Table</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <h2><?php echo 'From: '.$from1; ?></h2>
                                <h2><?php echo 'To: '.$to1; ?></h2>


                                <table id="example" class="display nowrap" width="100%">
                                    <?php
                                    $data1 = $obj->commstatementcustomerwiseprint($from,$to,$territory);
                                    //echo json_encode($data1);
                                    for($i=0;$i<count($data1);$i++) {
                                        $hi[] = ($data1[$i]['total_amount']);

                                    }
                                    for($i=0;$i<count($data1);$i++) {
                                        $hi1[] = ($data1[$i]['Commission']);

                                    }
                                    //                                echo json_encode(array_sum($hi))


                                    ?>
                                    <thead>
                                    <tr>


                                        <th>Party</th>
                                        <th>Total Amount</th>
                                        <th>Total Commission</th>


                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php


                                    foreach ($data1 as $item) {
                                        $curr = $item['Currency'];
                                    }
                                    $sum1 =  json_encode(array_sum($hi));
                                    $sum12 =  json_encode(array_sum($hi1));
                                    $sum = round($sum1);
                                    $sum = number_format($sum,2);
                                    $sum12 = round($sum12);
                                    $sum12 = number_format($sum12,2);

                                    $i=0;
                                    foreach ($data1 as $data){
                                        $i++;
//echo  json_encode($result);

                                        $date = new DateTime($data['po_date']);
                                        $po_date = $date->format('d.m.y');


                                        ?>


                                        <tr>

                                            <td><?php echo $data['customer']; ?></td>

                                            <td><?php echo number_format($data['total_amount']); ?></td>
                                            <td><?php echo number_format($data['Commission']); ?></td>

                                        </tr>

                                    <?php } ?>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<script src="../vendors/moment/min/moment.min.js"></script>

<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>




<?php
$today = date("d.m.y");
$time = date("h:i:s");

?>














<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>

<script>




    //$(document).ready(function() {
    //        var from = "<?php //echo $from1; ?>//";
    //        var to = "<?php //echo $to1; ?>//";
    //        var company = "<?php //echo $company; ?>//";
    //        var principal = "<?php //echo $principal; ?>//";
    //        var time = "<?php //echo $time; ?>//";
    //        var date = "<?php //echo $today; ?>//";
    //        var curr = "<?php //echo $curr; ?>//";
    //        //var sum = "<?php ////echo $sum; ?>////";
    //
    //
    //        $('#example').DataTable( {
    //        dom: 'Bfrtip',
    //
    //        buttons:[
    //            {
    //                extend: 'pdfHtml5',
    //                title:'',
    //
    //                orientation: 'landscape',
    //                filename:'ordereditems_report',
    //                message: company+""+"\n"+
    //                    "From: "+from+"   "+"To: "+to+"              "+principal+"                                 "+"Date:  "+date+"  "+"Time:  "+time+'\n'+
    //                    '.'+'                                                                                                                                                                                     NET :-    '+curr,
    //                exportOptions: {
    //                        modifier: {
    //                            selected: null
    //                        }
    //                    }
    //                customize : function(doc) {
    //                    // doc.defaultStyle.alignment = 'center';
    //                    doc.styles.tableHeader.alignment = 'left';
    //                    doc.content[1].table.widths = [ '20%', '15%', '15%','10%','10%','15%'];
    //                    doc['header']=(function() {
    //                        return {
    //                            columns: [
    //                                {
    //                                    alignment: 'left',
    //                                    italics: true,
    //                                    text: 'dataTables',
    //                                    fontSize: 18,
    //                                    margin: [8,0]
    //                                },
    //                                {
    //                                    alignment: 'right',
    //                                    fontSize: 14,
    //                                    text: 'Ordered Items'
    //                                }
    //                            ],
    //                            margin: 20
    //                        }
    //                    });
    //
    //                }
    //
    //
    //            },
    //            {
    //                extend: 'excel'
    //
    //            },
    //            {
    //                extend: 'csv'
    //
    //            }
    //        ],select: true
    //    } );
    //
    //} );


    $(document).ready(function() {
        var from = "<?php echo $from1; ?>";
        var to = "<?php echo $to1; ?>";
        var company = "APEX PRECISION AGENCIES";
        var principal = "THK CO.,LTD";
        var time = "<?php echo $time; ?>";
        var date = "<?php echo $today; ?>";
        var curr = "<?php echo $curr; ?>";
        var sum = "<?php echo $sum; ?>";
        var sum12 = "<?php echo $sum12; ?>";
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    title:'Customerwise Commission Statement',

                    message:company+""+"<br>"+
                    "From: "+from+"&nbsp;&nbsp;&nbsp;&nbsp;"+"To:"+to+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+principal+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+"Date:  "+date+"&nbsp;&nbsp;&nbsp;"+"Time:  "+time,
                    messageBottom:"Net:-             "+curr+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+"Total Gross Amt:              "+sum+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+"Total Comm. Amt:&nbsp;" +sum12,

                    text: 'Print all',

                    exportOptions: {
                        stripHtml: false,
                        modifier: {
//
                            selected: null
                        }
                    }
                },
                {

                    extend: 'print',
                    title:'Customerwise Commission Statement',
                    message:company+""+"<br>"+
                    "From: "+from+"&nbsp;&nbsp;&nbsp;&nbsp;"+"To:"+to+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+principal+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+"Date:  "+date+"&nbsp;&nbsp;&nbsp;"+"Time:  "+time,

                    text: 'Print selected'

                }
            ],
            select: true
        } );
    } );




</script>
</body>
</html>




