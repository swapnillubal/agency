<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/8/19
 * Time: 5:58 PM
 */




include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Cancelled PO</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>View Cancelled Purchase Order</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <?php
                            $po_no = $_REQUEST['id'];

                            $data1=$obj->canpodetailsedit($po_no);
//                            echo json_encode($data1);

                            foreach ($data1 as $things){


                                $quotation_no = $things['quotation_no'];
                                $po_id = $things['po_id'];
                            }

                            $quotation_id=$obj->sendqid($quotation_no);


                            ////////new stufff




                            foreach($data1 as $test){
                                $prod_data = $obj->showproductlistpo1($po_id);
//                                echo json_encode($prod_data);
                                foreach ($prod_data as $something) {

                                    $so[] = $something['product_id'];
                                    $totamount[] = $something['TOTAL_AMOUNT'];
                                }
                                $totamount = array_sum($totamount);
                                $comma_separated = implode(",", $so);

                                $alltheprods = $obj->listproduct1($comma_separated);

//                                echo json_encode($alltheprods);

                                foreach ($prod_data as $things){
                                    $quotation_pl_id=$things['po_pl_id'];
                                    $product_id=$things['product_id'];
                                    $quantity=$things['quantity'];
                                    $value1 =$things['net_value'];
                                    $delivery1 =$things['delivery'];

                                    $real_prod_data []= $obj->showproductdetailspurchaseorder($product_id,$quantity,$value1,$delivery1,$quotation_pl_id);
                                }
                            }

                            $customer_data= $obj->listcustomer();
                            $company_data= $obj->listcompany();
                            $supplier_data= $obj->listprincipal();




                            foreach ($data1 as $data){


                            ?>

                            <div class="x_content">

                                <form id="addPO" name="addPO" method="post"  class="form-horizontal form-label-left" enctype="multipart/form-data" action="./adminapi/purchase_order/editpurchaseorder.php">
                                    <input id="q_id" class="form-control col-md-3 col-xs-3" name="q_id"  value="<?php echo $quotation_id; ?>" type="hidden">
                                    <input id="po_id" class="form-control col-md-3 col-xs-3" name="po_id"  value="<?php echo $po_id; ?>" type="hidden">
                                    <span class="section">Information</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="po_no">PO No.
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="po_no" class="form-control col-md-3 col-xs-3" readonly name="po_no" value="<?php echo $data['po_no']; ?>" required="required" type="text">
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker'>
                                            <input type='text' name="date1" class="form-control" readonly value="<?php echo $data['po_date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="enq_no">Enquiry No.
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-3 col-xs-3" readonly name="enq_no" required="required" type="text" value="<?php echo $data['enq_no'];?>" >
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" class="form-control" readonly value="<?php echo $data['enq_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="princi_qtn_ref_no">Principal's Qtn. Ref.
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="princi_qtn_ref_no" class="form-control col-md-3 col-xs-3" readonly name="princi_qtn_ref_no" required="required" type="text" value="<?php echo $data['principal_qtn_ref'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker3'>
                                            <input type='text' name="date3" class="form-control" readonly value="<?php echo $data['principal_qtn_ref_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="q_no">Our Qtn Ref.
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-7 col-xs-12" readonly name="q_no"  required="required" type="text" value="<?php echo $data['quotation_no'];?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                            <input type='text' name="date4" class="form-control" readonly value="<?php echo $data['q_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <!--customer-->
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="customer">Customer
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="customer" class="form-control col-md-3 col-xs-3" readonly name="customer" type="text" value="<?php echo $data['customer'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="company">Company
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="company" class="form-control col-md-3 col-xs-3" readonly name="company" type="text" value="<?php echo $data['company'];?>" >
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="supplier">Supplier
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="supplier" class="form-control col-md-3 col-xs-3" readonly name="supplier" type="text" value="<?php echo $data['supplier'];?>" >
                                        </div>
                                    </div>

                                    <!--end-->

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="validity">Validity
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="validity" class="form-control col-md-3 col-xs-3" readonly name="validity" type="text" value="<?php echo $data['validity'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="despatch">Despatch
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="despatch" class="form-control col-md-3 col-xs-3" readonly name="despatch" type="text" value="<?php echo $data['despatch'];?>" >
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="payment">Payment
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="payment" class="form-control col-md-6 col-xs-3" readonly name="payment" type="text" value="<?php echo $data['payment'];?>" >
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date5">Actual Receipt Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker5'>
                                            <input type='text' name="date5" class="form-control" readonly value="<?php echo $data['actual_receipt_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="remark">Remark
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <textarea id="remark" name="remark" readonly class="form-control col-md-3 col-xs-3"><?php echo $data['remark'];?></textarea>
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date6">Intimation Date
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker6'>
                                            <input type='text' name="date6" readonly class="form-control" value="<?php echo $data['intimation_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="upload">Upload
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <?php if($data['upload']!=""){?>

                                            <a href="<?php echo $data['upload']; ?>" target="_blank" class="btn "><i class="fa fa-print"></i>
                                                Document</a>

                                            <?php }else{ echo "NO document uploaded";  } ?>

                                        </div>


                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md- col-sm-3 col-xs-3" for="date7">ETD 1
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker7'>
                                            <input type='text' name="date7" readonly class="form-control" value="<?php echo $data['etd_1'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="date8">ETD 2
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker8'>
                                            <input type='text' name="date8" readonly class="form-control" value="<?php echo $data['etd_2'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="date9">ETD 3
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker9'>
                                            <input type='text' name="date9" readonly class="form-control" value="<?php echo $data['etd_3'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>





                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <h2>Product section</h2>
                            <div class="x_content">

                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th class="column-title">Make </th>
                                        <th class="column-title">Item </th>
                                        <th class="column-title">Unit </th>
                                        <th class="column-title">Currency</th>
                                        <th class="column-title">Quantity</th>
                                        <th class="column-title">Value</th>
                                        <th class="column-title">Delivery</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php


                                    //                                                                        echo json_encode($real_prod_data);

                                    foreach($real_prod_data as $item){


                                        ?>
                                        <tr>


                                            <td>
                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $item[0]['pl_id']; ?>" type="hidden">
                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $item[0]['product_id']; ?>" type="hidden">
                                                <?php echo $item[0]['make']; ?></td>
                                            <td><?php echo $item[0]['item']; ?></td>
                                            <td><?php echo $item[0]['unit']; ?></td>
                                            <td><?php echo $item[0]['currency']; ?></td>
                                            <td><div class="col-md-10 col-sm-10 col-xs-10">
                                                    <input class="form-control col-md-12 col-xs-12" readonly value="<?php echo $item[0]['quantity']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-10 col-sm-10 col-xs-10">
                                                    <input class="form-control col-md-12 col-xs-12" readonly value="<?php echo $item[0]['value']; ?>" type="text">
                                                </div>
                                            </td>
                                            <td><div class="col-md-10 col-sm-10 col-xs-10">
                                                    <input class="form-control col-md-12 col-xs-12" readonly value="<?php echo $item[0]['delivery']; ?>" type="text">
                                                </div>
                                            </td>

                                        </tr>

                                    <?php } ?>

                                    </tbody>

                                </table>
                                <h1>Total PO Amount :- <?php echo number_format($totamount); ?></h1><br>


                            </div>
                        </div>

                    </div>
                    <div class="ln_solid"></div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addPO").on('submit', (function (e) {
        var form = document.getElementById("addPO");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/purchase_order/editpurchaseorder.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                //alert(data);

                /// ***issue***
                if (data != "success") {
                    toastr["success"]("Successfully Edited Purchase Order", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './purchase_order_list.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
    $(".bb1").click(function (e) {
        e.preventDefault();
        var val123 = $(this).data('id');

        var tr = $(this).closest('tr');

        if (val123 != "") {
            $.ajax({
                type: "POST",
                url: './adminapi/purchase_order/delete_productlist.php',
                data: ({idinfo: val123}),
                success: function (data) {
                    console.log(data);
                    if (data != "success") {
                        //alert(data);
                        tr.remove();
                        toastr["success"]("Successfully Deleted Product", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Product", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Product missing", "Agency Administrator");
        }
    });
</script>





</body>
</html>

