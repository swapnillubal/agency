<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/11/18
 * Time: 2:27 PM
 */

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');
$from=$_REQUEST['date1'];
$to=$_REQUEST['date2'];
$company=$_REQUEST['company'];
$principal=$_REQUEST['principal'];
$data1 = $obj->consolidatedprint($from,$to,$company,$principal);
//echo json_encode($data1);



for($i=0;$i<count($data1);$i++) {
    $hi[] = ($data1[$i]['TOTAL_PO']);
//    echo $data1[$i]['TOTAL_PO'];
//    =array_sum($data1[$i]['TOTAL_PO']);

}
//echo json_encode(array_sum($hi));

for($i=0;$i<count($data1);$i++) {
    $hi1[] = ($data1[$i]['TOTAL_DESPATCH']);

}
//echo json_encode(array_sum($hi1));



$today = date("d.m.y");
$time = date("h:i:s");


$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);

//Cell(width , height ,text,border ,end line , [align])
$pdf->Cell(90 ,5,$company,0,0,'L');
$pdf->Cell(90 ,5,'Consolidated Performance',0,1,'R');
$pdf->Cell(180 ,5,'',0,1);
$pdf->SetFont('Arial','',10);

$pdf->Cell(60 ,5,'From :'.$from.'  To :'.$to,0,0);
$pdf->Cell(60 ,5,'Supplier :'.$principal,0,0);
$pdf->Cell(60 ,5,'Date :'.$today.'  '.'Time : '.$time,0,0);

//$pdf->Cell(20 ,8,'',0,1);
$pdf->Cell(20 ,8,'',0,1);

$pdf->Cell(15 ,8,'Sr No.',1,0);
$pdf->Cell(91 ,8,'Product Description',1,0);
$pdf->Cell(37 ,8,'Total PO',1,0);
$pdf->Cell(37 ,8,'Total Despatch',1,1);


$i=0;
foreach ($data1 as $data){

    $i++;

    $pdf->Cell(15 ,8,$i,1,0);

    $pdf->Cell(91 ,8,$data['customer'],1,0);
    $pdf->Cell(37 ,8,$data['TOTAL_PO'],1,0);
    $pdf->Cell(37 ,8,$data['TOTAL_DESPATCH'],1,1);


}
$pdf->Cell(180 ,5,'',0,1);

$pdf->Cell(106 ,8,'TOTAL',1,0);
$pdf->Cell(37 ,8,json_encode(array_sum($hi)),1,0);
$pdf->Cell(37 ,8, json_encode(array_sum($hi1)),1,0);

$pdf->Output();



?>