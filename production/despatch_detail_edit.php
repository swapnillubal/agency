<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/1/19
 * Time: 12:58 PM
 */


include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Despatch </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php
                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Despatch</h3>

                    </div>


                </div>

                <div class="clearfix"></div>


                <?php
                 $despatch_id = $_REQUEST['id'];
                $data1 = $obj->listdespatchdetailoriginal($despatch_id);
                $po_id_data = $obj->listdespatchdetail($despatch_id);











                $data12 = $obj->listpurchaseorder123();
//                echo json_encode($po_id_data);

                foreach ($po_id_data as $po_ids){
                    $po_idss[] = $po_ids['po_id'];
                }

                foreach ($po_idss as $po_id){
                $real_po_data = $obj->realpodetail($po_id);
                }



//                echo json_encode($real_po_data);


                $comma_separated = implode(",", $po_idss);
                $allthepos = $obj->listpo1($comma_separated);
//echo json_encode($allthepos);

                $customer_data= $obj->listcustomer();
                $company_data= $obj->listcompany();
                $supplier_data= $obj->listprincipal();

                ?>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
<?php
foreach ($data1 as $data) {


?>
                                <form id="adddespatch" name="adddespatch" method="post" action="./adminapi/despatch/add_despatch.php" class="form-horizontal form-label-left" >

                                    <span class="section">Add Details - Step 1</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="company" class="form-control">
                                                <?php

                                                foreach ($company_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data['company']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>


                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="supplier" class="form-control">
                                                <?php

                                                foreach ($supplier_data as $cust)
                                                {?>
                                                    <option value='<?php echo $cust['name']; ?>' <?php if($cust['name'] == $data['supplier']) { ?> selected <?php } ?> ><? echo $cust['name'];?> </option>


                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="invoiceno">Invoice No <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="invoiceno" class="form-control col-md-3 col-xs-3" name="invoiceno"  required="required" type="text" value="<?php echo $data['invoice_no'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date1"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' name="date1" class="form-control" value="<?php echo $data['invoice_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="hawb">HAWB No <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="hawb" class="form-control col-md-3 col-xs-3" name="hawb"  required="required" type="text" value="<?php echo $data['hawb_no'];?>">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" class="form-control" value="<?php echo $data['hawb_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="mawb">MAWB No <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="mawb" class="form-control col-md-3 col-xs-3" name="mawb"  required="required" type="text" value="<?php echo $data['mawb_no'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker3'>
                                            <input type='text' name="date3" class="form-control" value="<?php echo $data['mawb_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="flight1">Flight 1 <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="flight1" class="form-control col-md-3 col-xs-3" name="flight1"  required="required" type="text" value="<?php echo $data['flight1'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker4'>
                                            <input type='text' name="date4" class="form-control" value="<?php echo $data['flight1_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="flight2">Flight 2 <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="flight2" class="form-control col-md-3 col-xs-3" name="flight2"  required="required" type="text" value="<?php echo $data['flight2'];?>" >
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date"> Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker5'>
                                            <input type='text' name="date5" class="form-control" value="<?php echo $data['flight2_date'];?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="upload">Upload<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <a href="<?php echo $data['upload']; ?>" target="_blank" class="btn "><i class="fa fa-print"></i>
                                                Document</a>
                                            <input id="report" class="form-control col-md-7 col-xs-12" name="report" type="file">
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="x_panel">

                                                <div class="x_content">
                                                    <h2>Existing Purchase orders in the despatch</h2>

                                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>

                                                            <th class="column-title">Customer</th>
                                                            <th class="column-title">PO No </th>
                                                            <th class="column-title">PO Date </th>
                                                            <th class="column-title">Quotation No. </th>
                                                            <th class="column-title">Company</th>
                                                            <th class="column-title">Supplier</th>


                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($real_po_data as $data1234){ ?>
                                                            <tr>
                                                                <input type="hidden" name="check[]" value="<?php echo $data1234['po_id'];?>">
                                                                <td><?php echo $data1234['customer'];?></td>
                                                                <td><?php echo $data1234['po_no'];?>  </td>
                                                                <td><?php echo $data1234['po_date'];?></td>
                                                                <td><?php echo $data1234['quotation_no'];?></td>
                                                                <td><?php echo $data1234['company'];?></td>
                                                                <td><?php echo $data1234['supplier'];?></td>


                                                            </tr>

                                                        <?php } ?>

                                                        </tbody>
                                                    </table>
                                                    <h2>Add new Purchase Orders</h2>
                                                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>

                                                            </th>
                                                            <th class="column-title">Customer </th>
                                                            <th class="column-title">PO No.</th>
                                                            <th class="column-title">PO Date </th>
                                                            <th class="column-title">Quotation No</th>
                                                            <th class="column-title">Company</th>
                                                            <th class="column-title">Supplier</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($allthepos as $data){ ?>
                                                            <tr>
                                                                <td><input type="checkbox" name="check[]" value="<?echo $data['po_id'];?>"></td>
                                                                <td><?php echo $data['customer'];?>  </td>
                                                                <td><?php echo $data['po_no'];?></td>
                                                                <td><?php echo $data['po_date'];?></td>
                                                                <td><?php echo $data['quotation_no'];?></td>
                                                                <td><?php echo $data['company'];?></td>
                                                                <td><?php echo $data['supplier'];?></td>

                                                            </tr>

                                                        <?php } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <img id="loading"    width="300" height="300" src="file_upload/loader.gif" /> <!-- Loading Image-->
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-10">
                                                <button type="submit" class="btn btn-primary">Cancel</button>
                                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>

    $(document).ready(function(){

        $("#loading").hide();


        $("#adddespatch").on('submit', (function (e) {
            $("#loading").show();

            var form = document.getElementById("adddespatch");
            e.preventDefault();

            $.ajax({

                url: "./adminapi/despatch/edit_despatch.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    if (data == "success") {
                        toastr["success"]("Saved Details, Proceeding to Next Step ", "Agency Administrator");
                        form.reset();
                        setTimeout(function () {
                            window.location = './despatch_detail_edit2.php';
                        }, 2000);
                    } else {
                        toastr["error"](data, "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        }));


    });



</script>
<script>
    $('#myDatepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#myDatepicker5').datetimepicker({
        format: 'YYYY-MM-DD'
    });




</script>
</body>
</html>

