<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/6/18
 * Time: 10:29 PM
 */


include "config/config.php";
include "class/agency.php";
include "section/checksession.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Enquiry to Principal</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">


                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
                                <?php
                                $enq_no = $_REQUEST['id'];
                                //echo $enq_no;
                                $data = $obj->showenquiry($enq_no);
                                $alldata = $obj->listproduct();
                                $prod_data = $obj->showenquiry($enq_no);
//                                echo json_encode($data);
                                $drawing=$obj->showdrawing($enq_no);
                                //echo json_encode($drawing);

                                foreach($data as $test){
                                    $enq_id = $test['enq_id'];
                                    $prod_data = $obj->showproductlist($enq_id);
                                    foreach ($prod_data as $something) {

                                        $so[] = $something['product_id'];
                                    }
                                    $comma_separated = implode(",", $so);
                                    $alltheprods = $obj->listproduct1($comma_separated);
//echo json_encode($prod_data);
                                    $test_decode=json_decode($test['product_list']);
                                    $data1=array();
                                    foreach ($prod_data as $things){
                                        $pl_id=$things['enq_pl_id'];
                                        $product_id=$things['product_id'];
                                        $quantity=$things['quantity'];


                                        $data1 []= $obj->showproductdetailsenq($product_id,$quantity,$pl_id);
                                    }
                                }
                                //                                echo json_encode($data1);



                                //  echo json_encode($data);

                                ?>
                                <? foreach($data as $fdata){  ?>
                                <form id="addenqprincipal" name="addenqprincipal" method="post" class="form-horizontal form-label-left" >
                                    <input id="enq_id" class="form-control col-md-12 col-xs-12" name="enq_id" value="<?php echo $fdata['enq_id']; ?>" type="hidden">
                                    <span class="section"><h3>Send Enquiry to Principal</h3></span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-3 col-xs-3" name="enq_no" value="<?php echo $fdata['enq_no']; ?>" readonly required="required" type="text">
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date" class="form-control" value="<?php echo $fdata['date']; ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>



                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer">Customer<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="customer" class="form-control col-md-7 col-xs-12" name="customer" type="text" value="<?php echo $fdata['customer']; ?>" >
                                        </div>
                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="company" class="form-control col-md-7 col-xs-12" name="company" type="text" value="<?php echo $fdata['company'] ?>" >
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="supplier" class="form-control col-md-7 col-xs-12" name="supplier" type="text" value="<?php echo $fdata['supplier'] ?>" >
                                        </div>
                                    </div>
                                    <?php } ?>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <h2>Product section</h2>
                                            <div class="x_content">

                                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>

                                                        <th class="column-title">Make </th>
                                                        <th class="column-title">Item </th>
                                                        <th class="column-title">Unit </th>
                                                        <th class="column-title">Currency</th>
                                                        <th class="column-title">Quantity</th>

                                                        <th class="column-title">Manage</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php
                                                    foreach($data1 as $item){

                                                        ?>
                                                        <tr>


                                                            <td>
                                                                <input id="check[]" class="form-control col-md-12 col-xs-12" name="check[]" value="<?php echo $item[0]['pl_id']; ?>" type="hidden">
                                                                <input id="prodid[]" class="form-control col-md-12 col-xs-12" name="prodid[]" value="<?php echo $item[0]['product_id']; ?>" type="hidden">
                                                                <?php echo $item[0]['make']; ?></td>
                                                            <td><?php echo $item[0]['item']; ?></td>
                                                            <td><?php echo $item[0]['unit']; ?></td>
                                                            <td><?php echo $item[0]['currency']; ?></td>
                                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <input id="value" class="form-control col-md-12 col-xs-12" name="quantity[]" value="<?php echo $item[0]['quantity']; ?>" type="text">
                                                                </div>
                                                            </td>

                                                            <td><a href="#" data-id="<?php echo $item[0]['pl_id']; ?>" id="bb1"
                                                                   class="btn bb1"><i class="fa fa-trash"></i> Delete</a>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>

                                                    </tbody>
                                                </table>
                                                <h2>Add new product</h2>
                                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>

                                                        </th>
                                                        <th class="column-title">Make </th>
                                                        <th class="column-title">Item </th>
                                                        <th class="column-title">Unit </th>
                                                        <th class="column-title">Currency</th>
                                                        <th class="column-title">Quantity</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($alltheprods as $data){ ?>
                                                        <tr>
                                                            <td><input type="checkbox" name="prodid[]" value="<?echo $data['product_id'];?>"></td>
                                                            <td><?php echo $data['make'];?>  </td>
                                                            <td><?php echo $data['item'];?></td>
                                                            <td><?php echo $data['unit'];?></td>
                                                            <td><?php echo $data['currency'];?></td>
                                                            <td><div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <input id="quantity" class="form-control col-md-12 col-xs-12" name="quantity[]" type="text">
                                                                </div>
                                                            </td>

                                                        </tr>

                                                    <?php } ?>

                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <h2>Drawings section</h2>
                                            <div class="x_content">

                                                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>

                                                        <th class="column-title">Item </th>
                                                        <th class="column-title">File</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php
                                                    foreach($drawing as $draw){

                                                        ?>
                                                        <tr>

                                                            <td><?php echo $draw['item_name']; ?></td>
                                                            <td><a target='_blank' href="<?php echo $draw['file_upload']; ?>" class="btn bb1"><i class="fa fa-eye"></i>
                                                                    View</a>

                                                            </td>

                                                        </tr>

                                                    <?php } ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-md-offset-10">
                                            <button type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<script>
    $("#addenqprincipal").on('submit', (function (e) {
        var form = document.getElementById("addenqprincipal");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/enquiry/addenqtoprinci.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data == "success") {
                    toastr["success"]("Successfully sent to principal", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './enq_principal.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>
<script>

    $(document).on("click", "#detail", function () {
        var myBookId = $(this).data('id');
        setTimeout(function () {
            window.location = './product_masterdetails.php?id=' + myBookId;
        }, 1);
    });


    $(".bb1").click(function (e) {
        e.preventDefault();
        var val1 = $(this).data('id');
        var tr = $(this).closest('tr');
        if (val1 != "") {
            $.ajax({
                type: "POST",
                url: './adminapi/enquiry/delete_productlist.php',
                data: ({idinfo: val1}),
                success: function (data) {
                    console.log(data);
                    if (data == "success") {
                        //alert(data);
                        tr.remove();
                        toastr["success"]("Successfully Deleted Product", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Product", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Product missing", "Agency Administrator");
        }
    });
</script>

</body>
</html>

