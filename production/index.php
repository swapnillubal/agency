<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/5/18
 * Time: 4:02 AM
 */
include "config/config.php";

include "section/checksession.php";

include "class/agency.php";

$obj = new agency();

$data1=$obj->countenq();
$data2=$obj->countquotation();
$data3=$obj->countpo();
$data4=$obj->countindent();


for($i=1;$i<13;$i++) {


    $enqdata[]= $obj->countenq1(year, $i);

}

for($i=1;$i<13;$i++) {


    $podata[]= $obj->countpo1(year, $i);

}

for($i=1;$i<13;$i++) {


    $despdata[]= $obj->countdespatch1(year, $i);

}

for($i=1;$i<13;$i++) {


    $despdata[]= $obj->countdespatch1(year, $i);

}
for($i=1;$i<13;$i++) {


    $outstanddata[]= $obj->countoutstanding1(year, $i);

}




$dataPoints1 = array(
    array("label"=> "Total Enquiries", "y"=> $data1),
    array("label"=> "Total Quotations", "y"=> $data2),
    array("label"=> "Total Purchase Order", "y"=> $data3),
    array("label"=> "Total Indents", "y"=> $data4),
);

$dataPoints2 = array(
    array("label"=> "January", "y"=> $enqdata[0]),
    array("label"=> "February", "y"=> $enqdata[1]),
    array("label"=> "March", "y"=> $enqdata[2]),
    array("label"=> "April", "y"=> $enqdata[3]),
    array("label"=> "May", "y"=> $enqdata[4]),
    array("label"=> "June", "y"=> $enqdata[5]),
    array("label"=> "July", "y"=> $enqdata[6]),
    array("label"=> "August", "y"=> $enqdata[7]),
    array("label"=> "September", "y"=> $enqdata[8]),
    array("label"=> "October", "y"=> $enqdata[9]),
    array("label"=> "November", "y"=> $enqdata[10]),
    array("label"=> "December", "y"=> $enqdata[11]),
);
$dataPoints3 = array(
    array("label"=> "January", "y"=> $podata[0]),
    array("label"=> "February", "y"=> $podata[1]),
    array("label"=> "March", "y"=> $podata[2]),
    array("label"=> "April", "y"=> $podata[3]),
    array("label"=> "May", "y"=> $podata[4]),
    array("label"=> "June", "y"=> $podata[5]),
    array("label"=> "July", "y"=> $podata[6]),
    array("label"=> "August", "y"=> $podata[7]),
    array("label"=> "September", "y"=> $podata[8]),
    array("label"=> "October", "y"=> $podata[9]),
    array("label"=> "November", "y"=> $podata[10]),
    array("label"=> "December", "y"=> $podata[11]),
);
$dataPoints4 = array(
    array("label"=> "January", "y"=> $despdata[0]),
    array("label"=> "February", "y"=> $despdata[1]),
    array("label"=> "March", "y"=> $despdata[2]),
    array("label"=> "April", "y"=> $despdata[3]),
    array("label"=> "May", "y"=> $despdata[4]),
    array("label"=> "June", "y"=> $despdata[5]),
    array("label"=> "July", "y"=> $despdata[6]),
    array("label"=> "August", "y"=> $despdata[7]),
    array("label"=> "September", "y"=> $despdata[8]),
    array("label"=> "October", "y"=> $despdata[9]),
    array("label"=> "November", "y"=> $despdata[10]),
    array("label"=> "December", "y"=> $despdata[11]),
);
$dataPoints5 = array(
    array("label"=> "January", "y"=> $outstanddata[0]),
    array("label"=> "February", "y"=> $outstanddata[1]),
    array("label"=> "March", "y"=> $outstanddata[2]),
    array("label"=> "April", "y"=> $outstanddata[3]),
    array("label"=> "May", "y"=> $outstanddata[4]),
    array("label"=> "June", "y"=> $outstanddata[5]),
    array("label"=> "July", "y"=> $outstanddata[6]),
    array("label"=> "August", "y"=> $outstanddata[7]),
    array("label"=> "September", "y"=> $outstanddata[8]),
    array("label"=> "October", "y"=> $outstanddata[9]),
    array("label"=> "November", "y"=> $outstanddata[10]),
    array("label"=> "December", "y"=> $outstanddata[11]),
);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Agency Biz </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

<!--    <style>-->
<!--        .site_title {-->
<!--            /*position: relative;*/-->
<!--            position: absolute;-->
<!--            left: -18px;-->
<!--            width: 118%;-->
<!--            height: 250px;-->
<!--        }-->
<!--    </style>-->



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->

                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->


            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->

        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Welcome To Agency Business</h3>
                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row top_tiles" style="margin: 10px 0;">
                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                <span>Total Enquiries</span>
                                <h2><?php
                                    $data1=$obj->countenq();
                                    echo $data1; ?></h2>
                                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                <span>Total Quotations</span>
                                <h2><?php
                                    $data1=$obj->countquotation();
                                    echo $data1; ?></h2>
                                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                <span>Total Purchase Orders</span>
                                <h2><?php
                                    $data1=$obj->countpo();
                                    echo $data1; ?></h2>
                                <span class="sparkline_two" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                <span>Total Indents</span>
                                <h2><?php
                                    $data1=$obj->countindent();
                                    echo $data1; ?></h2>
                                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                            </div>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bar graph <small>Sessions</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bar graph <small>Sessions</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bar graph <small>Sessions</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bar graph <small>Sessions</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                        </div>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bar graph <small>Sessions</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id="chartContainer5" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->



<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<script>
    window.onload = function () {

        var chart1 = new CanvasJS.Chart("chartContainer1", {
            animationEnabled: true,
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Statistics till 2019"
            },
            axisY: {
                title: "Number ",
                includeZero: false
            },
            data: [{
                type: "column",
                dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
            }]
        });
        var chart2 = new CanvasJS.Chart("chartContainer2", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Number Of Enquiries(Monthly)"
            },
            axisY: {
                title: "Number ",
                includeZero: false
            },
            data: [{
                type: "column",
                dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
            }]
        });
        var chart3 = new CanvasJS.Chart("chartContainer3", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Number Of Purchase Orders(Monthly)"
            },
            axisY: {
                title: "Number ",
                includeZero: false
            },
            data: [{
                type: "column",
                dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
            }]
        });
        var chart4 = new CanvasJS.Chart("chartContainer4", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Number Of Despatches(Monthly)"
            },
            axisY: {
                title: "Number ",
                includeZero: false
            },
            data: [{
                type: "column",
                dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>
            }]
        });
        var chart5 = new CanvasJS.Chart("chartContainer5", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Number Of Outstanding Products(Monthly)"
            },
            axisY: {
                title: "Number ",
                includeZero: false
            },
            data: [{
                type: "column",
                dataPoints: <?php echo json_encode($dataPoints5, JSON_NUMERIC_CHECK); ?>
            }]
        });



        chart1.render();
        chart2.render();
        chart3.render();
        chart4.render();
        chart5.render();

    }
</script>

</body>
</html>

