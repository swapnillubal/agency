<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/6/18
 * Time: 3:49 PM
 */
include "config/config.php";
include "class/agency.php";

$obj = new agency();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        function ajaxFunction(choice)
        {

            var httpxml;
            try
            {
                // Firefox, Opera 8.0+, Safari
                httpxml=new XMLHttpRequest();
            }
            catch (e)
            {
                // Internet Explorer
                try
                {
                    httpxml=new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e)
                {
                    try
                    {
                        httpxml=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch (e)
                    {
                        alert("Your browser does not support AJAX!");
                        return false;
                    }
                }
            }
            function stateChanged()
            {
                if(httpxml.readyState==4)
                {
//alert(httpxml.responseText);
                    var myObject = JSON.parse(httpxml.responseText);

                    for(j=document.myForm.state.options.length-1;j>=0;j--)
                    {
                        document.myForm.state.remove(j);
                    }

                    var state1=myObject.value.state1;

                    var optn = document.createElement("OPTION");
                    optn.text = 'Select State';
                    optn.value = '';
                    document.myForm.state.options.add(optn);
                    for (i=0;i<myObject.state.length;i++)
                    {
                        var optn = document.createElement("OPTION");
                        optn.text = myObject.state[i];
                        optn.value = myObject.state[i];
                        document.myForm.state.options.add(optn);

                        if(optn.value==state1){
                            var k= i+1;
                            document.myForm.state.options[k].selected=true;
                        }
                    }

//////////////////////////
                    for(j=document.myForm.city.options.length-1;j>=0;j--)
                    {
                        document.myForm.city.remove(j);
                    }
                    var city1=myObject.value.city1;
//alert(city1);
                    for (i=0;i<myObject.city.length;i++)
                    {
                        var optn = document.createElement("OPTION");
                        optn.text = myObject.city[i];
                        optn.value = myObject.city[i];
                        document.myForm.city.options.add(optn);
                        if(optn.value==city1){
                            document.myForm.city.options[i].selected=true;
                        }

                    }


///////////////////////////
                    document.getElementById("txtHint").style.background='#00f040';
                    document.getElementById("txtHint").innerHTML='done';
//setTimeout("document.getElementById('txtHint').style.display='none'",3000)
                }
            }

            var url="ajax-dd3ck.php";
            var country=myForm.country.value;
            if(choice != 's1'){
                var state=myForm.state.value;
                var city=myForm.city.value;
            }else{
                var state='';
                var city='';
            }
            url=url+"?country="+country;
            url=url+"&state="+state;
            url=url+"&city="+city;
            url=url+"&id="+Math.random();
            myForm.st.value=state;
//alert(url);
            document.getElementById("txtHint2").innerHTML=url;
            httpxml.onreadystatechange=stateChanged;
            httpxml.open("GET",url,true);
            httpxml.send(null);
            document.getElementById("txtHint").innerHTML="Please Wait....";
            document.getElementById("txtHint").style.background='#f1f1f1';
        }
    </script>

    <script language="javascript">
        function addRow(tableID) {

            var table = document.getElementById(tableID);
            //alert(document.getElementById('tab').rows.length);
            var rowCount = table.rows.length;

            var row = table.insertRow(rowCount);

            var colCount = table.rows[0].cells.length;

            for(var i=0; i<colCount; i++) {

                var newcell	= row.insertCell(i);

                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                //alert(newcell.childNodes);
                switch(newcell.childNodes[0].type) {
                    case "text":
                        newcell.childNodes[0].value = "";
                        break;
                    case "checkbox":
                        newcell.childNodes[0].checked = false;
                        break;
                    case "select-one":
                        newcell.childNodes[0].selectedIndex = 0;
                        break;
                }
            }
        }

        function deleteRow(tableID) {
            try {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;

                for(var i=0; i<rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox = row.cells[0].childNodes[0];
                    if(null != chkbox && true == chkbox.checked) {
                        if(rowCount <= 1) {
                            alert("Cannot delete all the rows.");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }


                }
            }catch(e) {
                alert(e);
            }
        }

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>New Enquiry</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="glyphicon glyphicon-fire"></i> <span>Agency Biz !</span></a>
                </div>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Enquiry</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
                                <?php

                                $data = $obj->showproductmake();
                                //                                echo 'abc'.$data[1];

                                ?>

                                <form id="addenquiry" name="addenquiry" method="post" action="./adminapi/enquiry/add_enquiry.php" class="form-horizontal form-label-left" >

                                    <span class="section">Information</span>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enq_no">Enquiry No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="enq_no" class="form-control col-md-7 col-xs-12" name="enq_no"  required="required" type="text">
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date1">Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker1'>
                                            <input type='text' name="date1" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>


                                    </div>


                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customer">Customer<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="customer" class="form-control col-md-7 col-xs-12" name="customer" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="remark" required="required" name="remark" class="form-control col-md-7 col-xs-12"></textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="company">Company<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="company" class="form-control col-md-7 col-xs-12" name="company" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="supplier" class="form-control col-md-7 col-xs-12" name="supplier" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="subgroup">Enter Subgroup <span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <input id="subgroup" class="form-control col-md-7 col-xs-12" name="subgroup" type="text">
                                        </div>


                                        <label class="control-label col-md-1 col-sm-1 col-xs-1" for="date2">Actual Date <span class="required">*</span>
                                        </label>
                                        <div class='col-md-3 col-sm-3 col-xs-3 input-group date' id='myDatepicker2'>
                                            <input type='text' name="date2" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <input type="button" value="Add Product" onclick="addRow('tab')" />

                                    <input type="button" value="Delete Product" onclick="deleteRow('tab')" />



                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th>

                                            </th>
                                            <th class="column-title">Make </th>
                                            <th class="column-title">Item </th>
                                            <th class="column-title">Unit </th>
                                            <th class="column-title">Currency </th>
                                            <th class="column-title">Subgroup </th>

                                            <th class="column-title">Quantity </th>


                                        </tr>
                                        </thead>


                                        <tbody id="tab">
                                        <tr>
                                            <td><input type="checkbox" name="chk[]"/></td>
                                            <input type=hidden name=st value=0>

                                            <td>

                                                <select name=make id='s1' onchange=ajaxFunction('s1');>
                                                    <option value=''>Select One</option>
                                                    <?php
                                                    $dbo = mysqli_connect(HOST, USER, PASS, DB) or die("<br/>Could not connect to MySQL server");
                                                    $sql="select distinct make from product ";
                                                    foreach ($dbo->query($sql) as $row) {
                                                        echo "<option value=$row[make]>$row[make]</option>";
                                                    } ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name=item onchange=ajaxFunction('s2');>
                                                    <option value=''>Select One</option></select>
                                            </td>
                                            <td>
                                                <select name=unit onchange=ajaxFunction('s3');>
                                                    <option value=''>Select One</option></select>
                                            </td>
                                            <td>
                                                <input id="quantity" class="form-control col-md-3 col-xs-3" name="currency" type="text">
                                            </td>
                                            <td>
                                                <input id="quantity" class="form-control col-md-3 col-xs-3" name="subgroup" type="text">
                                            </td>


                                            <td>
                                                <input id="quantity" class="form-control col-md-3 col-xs-3" name="quantity" type="text">
                                            </td>
                                        </tr>

                                        </tbody>

                                    </table>


                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Custom Theme Scripts -->
<!--<script src="../jquery-3.3.1.min.js"></script>-->
<script src="../build/js/custom.min.js"></script>
<!--<script>-->
<!--    $("#addenquiry").on('submit', (function (e) {-->
<!--        var form = document.getElementById("addenquiry");-->
<!--       // alert(form.value);-->
<!--        e.preventDefault();-->
<!---->
<!--        $.ajax({-->
<!---->
<!--            url: "./adminapi/enquiry/add_enquiry.php",-->
<!--            type: "POST",-->
<!--            data: new FormData(this),-->
<!--            contentType: false,-->
<!--            cache: false,-->
<!--            processData: false,-->
<!--            success: function (data) {-->
<!--                console.log(data);-->
<!--                if (data == "success") {-->
<!--                    toastr["success"]("Successfully Added New Company", "Agency Administrator");-->
<!--                    form.reset();-->
<!--                    setTimeout(function () {-->
<!--                        window.location = './company_master.php';-->
<!--                    }, 2000);-->
<!--                } else {-->
<!--                    toastr["error"](data, "Agency Administrator");-->
<!--                }-->
<!--            },-->
<!--            error: function () {-->
<!--            }-->
<!--        });-->
<!--    }));-->
<!--</script>-->
<script>
    $('#myDatepicker1').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });

</script>


</body>
</html>

