<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/8/18
 * Time: 4:12 AM
 */


include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$id=$_POST['id'];
$margin=$_POST['margin'];


$sign=$_POST['sign'];
$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);


if(isset($sign)){
    $signdetails = $obj->signdetails($sign);

    foreach ($signdetails as $sign_items){
        $sign_image = $sign_items['upload'];
    }
}

$data1=$obj->ocprint($id);
//echo json_encode($data1);
$prod_data_quotation = $obj->ocprodprint($id);




$merge_array=array();
for($i=0;$i<count($prod_data_quotation);$i++){
    $merge_array[$i] =
        ['product_id'=>$prod_data_quotation[$i]['product_id'],
            'quantity'=>$prod_data_quotation[$i]['quantity'],
            'value'=>$prod_data_quotation[$i]['net_value'],
            'delivery'=>$prod_data_quotation[$i]['delivery'],
        ];
}

//echo json_encode($merge_array);

foreach ($merge_array as $things){
    $product_id = $things['product_id'];
    $quantity = $things['quantity'];
    $value = $things['value'];
    $delivery = $things['delivery'];

    $real_prod_data[] = $obj->printtheprodsquotation($product_id,$quantity, $value,$delivery );

}





foreach ($data1 as $item) {
    $company = $item['company'];
    $payment = $item['payment'];
    $supplier = $item['supplier'];
    $customer = $item['customer'];
    $enq_no= $item['enq_no'];
    $q_no= $item['quotation_no'];
    $q_date= $item['q_date'];
    $enq_date= $item['enq_date'];
    $order_no= $item['order_no'];
    $po_no= $item['po_no'];
    $note= $item['note'];
    $order_date= $item['order_date'];
    $po_date= $item['po_date'];
    $our_ref_no = $item['our_ref_no'];
    $our_ref_date = $item['our_ref_date'];
    $prod[] = $item['product_list'];
}

//echo json_encode($data1);
//
//echo $customer;
//echo $supplier;
//echo $company;

$data2=$obj->listcompany1($company);
foreach ($data2 as $item2) {
    $comp_address = $item2['address'];
    $comp_email = $item2['email'];
    $comp_website= $item2['website'];
    $comp_Tel= $item2['telephone'];
    $comp_fax= $item2['fax'];

}



$data3=$obj->listprincipal1($supplier);
foreach ($data3 as $item3) {
    $principal_name=$item3['name'];
    $principal_address=$item3['address'];
    $currency=$item3['Currency'];
    $banker_address=$item3['Banker_Details'];
    $supplier_note=$item3['Note_OC'];
}
$data4=$obj->listcustomer1($customer);
foreach ($data4 as $item4) {
    $customer_name=$item4['name'];
    $customer_address=$item4['Address'];
    $customer_name_person=$item4['person_name'];
    $customer_name_department=$item4['Department'];
}

$data5 = $obj->getpo($po_no);
foreach ($data5 as $item5) {
    $quotation_no=$item5['quotation_no'];
    $quotation_date=$item5['qdate'];

}
//echo json_encode($data5);

class PDF extends FPDF{




    // Page header
    public function Header()
    {



        // Logo
        $this->SetFont('Arial','B',20);

//Cell(width , height ,text,border ,end line , [align])
        $this->Cell(40 ,5,'',0,0);
        $this->Cell(150 ,8,$GLOBALS['company'],0,1);
//$pdf->Cell(20 ,8,'',1,0);
        $this->SetFont('Arial','',10);
        $this->Cell(20 ,5,'',0,0);

        $this->Cell(190 ,5,$GLOBALS['comp_address'],0,1);

        if(strlen($GLOBALS['comp_email'])<30) {

            $this->Cell(60, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }else{
            $this->Cell(42, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }
        if(strlen($GLOBALS['comp_website'])<30){
            $this->Cell(60 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');
        }else{
            $this->Cell(40 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');

        }
        $strlen = strlen($GLOBALS['comp_fax']);
        if(0<$strlen && $strlen<20) {
            $this->Cell(42, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'] . ', Fax: ' . $GLOBALS['comp_fax'], 0, 1);
        }
        else{
            $this->Cell(62, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'], 0, 1);
        }
        $this->Cell(180 ,5,'',0,1);

        $x = $this->GetX();
        $y = $this->GetY();

        $this->SetXY($x + 149, $y);

        $this->Line($x, $y, $x+180,$y);

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}




$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,25);

$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetFont('Arial','BU',18);
$pdf->Cell(180 ,5,'',0,1);

$pdf->Cell(180 ,5,'ORDER CONFIRMATION',0,1,'C');

$pdf->SetXY($x + 149, $y+5);

$pdf->Cell(180 ,5,'',0,1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(180 ,5,'',0,1);


//
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y+1);
$pdf->Cell(180 ,5,$customer_name,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(90,5,$customer_address,0);
$pdf->Cell(180 ,5,'',0,1);

$pdf->Cell(180 ,5,'Attn:'.$customer_name_person,0,1);
$pdf->Cell(180 ,5,$customer_name_department,0,1);

$pdf->Cell(180 ,5,'',0,1);

$x = $pdf->GetX();
//$y = $pdf->GetY();
$pdf->SetXY($x , $y);
//$pdf->Ln(0);
$date = new DateTime($our_ref_date);
$date1 = new DateTime($po_date);


$pdf->Cell(180 ,5,'Our Ref: '.$our_ref_no,0,1,'R');
$pdf->Cell(180 ,5,'Date: '.$date->format('d.m.y'),0,1,'R');
$pdf->Cell(180 ,5,'Your Ref: '.$po_no,0,1,'R');
$pdf->Cell(180 ,5,'Date: '.$date1->format('d.m.y'),0,1,'R');

//
$x = $pdf->GetX();
$y = $pdf->GetY();

$pdf->SetXY($x , $y+5);





//$pdf->Cell(90 ,5,$customer_name,0,1);
//$pdf->MultiCell(70,5,$customer_address,0,'L');
////$pdf->Cell(180 ,8,'Our Ref: '.$q_no,1,1,'R');
//$pdf->Cell(180 ,5,'Our Ref: '.$q_no,0,1,'R');
//$pdf->Cell(180 ,5,'Date: '.$q_date,0,1,'R');
//$pdf->Cell(180 ,5,'Your Ref: '.$enq_no,0,1,'R');
$pdf->Cell(180 ,5,'',0,1);
$pdf->Cell(180 ,5,'',0,1);
$pdf->Cell(180 ,5,'We thank you for your order:- '.$po_no.'   Dated:- '.$date1->format('d.m.y'),0,1,'L');
$pdf->Cell(180 ,5,'We accept your order on behalf of '.$principal_name.' under following terms :-',0,1,'L');
$pdf->Cell(180 ,5,'',0,1);
$pdf->SetFont('Arial','B',10);

$pdf->Cell(10 ,5,'No.',1,0);
$pdf->Cell(80 ,5,'Product Description',1,0);
$pdf->Cell(20 ,5,'Quantity',1,0);
$pdf->Cell(10 ,5,'Unit',1,0);
$pdf->Cell(30 ,5,'Rate Per Unit',1,0);
$pdf->Cell(30 ,5,'Delivery',1,1);
$pdf->SetFont('Arial','',10);


$i=0;
foreach ($real_prod_data as $prod1){
    $i++;
    if(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2']) && !empty($prod1[0]['description3'])) {
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-20);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80, 5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n". $string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-20);
        $pdf->MultiCell(20, 5, $prod1[0]['quantity']."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-20);
        $pdf->MultiCell(10, 5, $prod1[0]['unit']."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-20);
        $pdf->MultiCell(30, 5, number_format($prod1[0]['value'])."\n\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-20);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n\n\n", 1);
    }
    elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description3'])){
        $y = $pdf->GetY();
        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-15);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80, 5,$prod1[0]['description1']."\n". $string ."\n".$prod1[0]['description3'], 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-15);
        $pdf->MultiCell(20, 5, $prod1[0]['quantity']."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-15);
        $pdf->MultiCell(10, 5, $prod1[0]['unit']."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-15);
        $pdf->MultiCell(30, 5, number_format($prod1[0]['value'])."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-15);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n\n", 1);
    }
    elseif(!empty($prod1[0]['description1']) && !empty($prod1[0]['description2'])) {

        $y = $pdf->GetY();

        if($y>240){
            $pdf->Ln(100);
        }
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->SetXY($x+10, $y-15);
        $pdf->MultiCell(80 ,5,$prod1[0]['description1']."\n".$prod1[0]['description2']."\n".$string,1);

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetXY($x+90 , $y-15);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+110 , $y-15);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n\n",1);
//
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+120 , $y-15);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();


        $pdf->SetXY($x+150 , $y-15);
        $pdf->MultiCell(30 ,5,$prod1[0]['delivery']." \n\n\n",1);



    }
    elseif(!empty($prod1[0]['description1'])){
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->MultiCell(10, 5, $i."\n\n", 1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+10, $y-10);
        $string = $prod1[0]['make'].",".$prod1[0]['item'];
        $pdf->MultiCell(80 ,5,$prod1[0]['description1']."\n".$string."\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+90, $y-10);
        $pdf->MultiCell(20 ,5,$prod1[0]['quantity']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+110, $y-10);
        $pdf->MultiCell(10 ,5,$prod1[0]['unit']."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+120, $y-10);
        $pdf->MultiCell(30 ,5,number_format($prod1[0]['value'])."\n\n",1);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x+150, $y-10);
        $pdf->MultiCell(30, 5, $prod1[0]['delivery']."\n\n", 1);
    }else{
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetXY($x, $y);
        $pdf->Cell(10, 5, $i, 1);
        $pdf->Cell(80, 5, $prod1[0]['item'], 1, 0);
        $pdf->Cell(20, 5, $prod1[0]['quantity'], 1, 0);
        $pdf->Cell(10, 5, $prod1[0]['unit'], 1, 0);
        $pdf->Cell(30, 5, number_format($prod1[0]['value']), 1, 0);
        $pdf->Cell(30, 5, $prod1[0]['delivery'], 1, 1);
    }
    $total[] = $prod1[0]['quantity']*$prod1[0]['value'];

}
$grand_total = array_sum($total);
//$text_val =  $f->format($grandtotal);
$words_val = $obj->convert_number_to_words($grand_total);
if(strlen($words_val)>70)
{
    $pdf->SetFont('Arial','B',7);

}else{

    $pdf->SetFont('Arial','B',9);
}
$pdf->Cell(180 ,5,'IN Words: '.$words_val.'      NET '.$currency.'   Grand total: '.number_format($grand_total),1,1,'R');
$pdf->SetFont('Arial','B',10);

for($i=0;$i<$margin;$i++)
{

    $pdf->Cell(180 ,5,'',0,1);
}

$pdf->Cell(110 ,5,'',0,1);
$pdf->Cell(180 ,5,"Payment :- ".$payment,0,1);

$pdf->Cell(180 ,5,"You are now requested to :-" ,0,1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);
$pdf->MultiCell(180,5,$supplier_note.'',0,'L');


//
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x + 110, $y);
$pdf->MultiCell(70,5,'',0,'L');

$pdf->SetXY($x + 149, $y);
$pdf->Ln(0);



$pdf->Cell(180 ,5,"" ,0,1);
$pdf->Cell(180 ,5,"" ,0,1);
$pdf->Cell(90 ,5,'For '.$company,0,1,'L');
if(isset($sign_image)){
    $x = $pdf->GetX();
    $y = $pdf->GetY();
    $pdf->Image($sign_image,$x,$y,55,22);
}
$pdf->Cell(180 ,15,'',0,1);

$pdf->Cell(70 ,5,"" ,0,1);



$x = $pdf->GetX();
$y = $pdf->GetY();

$pdf->SetXY($x + 149, $y);

$pdf->Line($x, $y, $x+180,$y);

$pdf->Cell(180 ,5,"" ,0,1);



$pdf->SetFont('Arial','BU',10);

$pdf->Cell(90 ,5,'PRINCIPAL ADDRESS',0,0,'L');
$pdf->Cell(90 ,5,"PRINCIPAL'S BANKER" ,0,1);
$pdf->SetFont('Arial','',10);


$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->SetXY($x, $y);
$pdf->SetFont('Arial','B',10);

$pdf->Cell(90 ,5,$principal_name,0,1,'L');
$pdf->SetFont('Arial','',10);

$pdf->MultiCell(90,5,$principal_address,0);
$x = $pdf->GetX();
//$y = $pdf->GetY();
$pdf->SetXY($x+90 , $y);
//$pdf->Ln(0);

$pdf->MultiCell(90,5,$banker_address,0);

//



$pdf->Output();



?>
