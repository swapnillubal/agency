<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/2/19
 * Time: 12:39 AM
 */

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$from = $_REQUEST['date1'];
$to= $_REQUEST['date2'];
$territory= $_REQUEST['territory'];
$customer= $_REQUEST['customer'];
$company = "APEX PRECISION AGENCIES";
$supplier = "THK CO.,LTD";
$current_date = date("d.m.Y");
$time = date("h:i:s");
$data2 = array();
$real_data = array();

//$data1=$obj->podetailedprint1($from,$to,$territory,$customer);
//foreach ($data1 as $item){
//    $po_id[]= $item['po_id'];
//}
////echo json_encode($po_id);
//
//
//foreach ($po_id as $desp){
//    $data2[] =$obj->detailedpoitem($desp);
//}
//


$real_data =$obj->pendingpodetailedprintfinal($from,$to,$customer,$territory);

//    echo json_encode($real_data);
//    print_r($real_data);


class PDF extends FPDF{




    // Page header
    public function Header()
    {

        $this->SetFont('Arial','B',12);
        $this->Cell(80 ,5,'',0,1);
        $this->Cell(80 ,5,$GLOBALS['company'],0,0);
        $this->Cell(100 ,5,'DETAILED ORDERS STATUS',0,1,'R');
        $this->SetFont('Arial','',8);
//        $this->Cell(50 ,5,'NET :-',0,0,'L');
        $this->SetFont('Arial','B',10);
        $this->Cell(100 ,5,$GLOBALS['supplier'],0,0,'R');
        $this->Cell(80 ,5,"Date: ".$GLOBALS['current_date']."    "."Time: ".$GLOBALS['time'],0,1,'R');

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}




$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);

//Cell(width , height ,text,border ,end line , [align])


//$pdf->Cell(20 ,8,'',1,0);
$pdf->SetFont('Arial','',8);
$pdf->Cell(20 ,5,'',0,1);


$pdf->Cell(90 ,5,'Item Description',1,0);
$pdf->Cell(25 ,5,'Rate',1,0);
$pdf->Cell(25 ,5,'Ordered Qt',1,0);
$pdf->Cell(20 ,5,'Delivered Qt',1,0);
$pdf->Cell(20 ,5,'Balanced Qt',1,1);



//echo count($real_data);

//for ($a=0;$a<count($real_data);$a++) {
//    for ($b=0;$b<count($real_data[$b]['product']);$b++) {
//        $hello[] = (int)$real_data[$b]['product'][$a]['AMOUNT'];
//    }
//}
//
//echo json_encode($hello);

//foreach($data2 as $itemss){
////        echo json_encode($itemss);
//    $some[] =  $itemss[0]['PO_AMOUNT'];
//}
//echo array_sum($some);


for($i=0;$i<count($real_data);$i++){
    $pdf->Cell(45 ,5,'PO NO.:'.$real_data[$i]['po_no'],0,0);
    $pdf->Cell(45 ,5,'PO Date.:'.$real_data[$i]['po_date'],0,0);
    $pdf->Cell(90 ,5,'Customer:'.$real_data[$i]['customer'],0,1);
//    $real_data[$i]['product'];
//        echo $real_data[$i]['product'];

//    foreach($real_data['product'] as $so){
//        echo "test".json_encode($so);
//    }

//    exit();

    foreach($real_data[$i]['product'] as $so){
        $pdf->Cell(90 ,5,$so['item'],0,0);
        $pdf->Cell(25 ,5,number_format($so['net_value']),0,0);
        $pdf->Cell(25 ,5,number_format($so['quantity']),0,0);
        $pdf->Cell(20 ,5,number_format($so['delivered_quantity']),0,0);
        $pdf->Cell(20 ,5,number_format($so['BALANCED_QUANTITY']),0,1);

    }
    $x = $pdf->GetX();
    $y = $pdf->GetY();

//            $pdf->SetXY($x + 149, $y);

    $pdf->Line($x, $y, $x+180,$y);
    $pdf->Cell(45 ,5,'Pending Order Value:'.number_format($real_data[$i]['PO_AMOUNT']),0,0);
    $pdf->Cell(45 ,5,'ETD 1:'.$real_data[$i]['etd_1'],0,0);
    $pdf->Cell(45 ,5,'ETD 2:'.$real_data[$i]['etd_2'],0,0);
    $pdf->Cell(45 ,5,'ETD 3:'.$real_data[$i]['etd_3'],0,1);

    $pdf->Cell(45 ,5,'Pending Despatch Value:'.number_format($real_data[$i]['DESPATCH_AMOUNT']),0,1);
//    $pdf->Cell(90 ,5,'Balance:'.number_format($real_data[$i]['BALANCED_AMOUNT']),0,1);
    $x = $pdf->GetX();
    $y = $pdf->GetY();

//            $pdf->SetXY($x + 149, $y);

    $pdf->Line($x, $y, $x+180,$y);


//echo json_encode($some);

    $x = $pdf->GetX();
    $y = $pdf->GetY();

//            $pdf->SetXY($x + 149, $y);

    $pdf->Line($x, $y, $x+180,$y);
//        $pdf->Cell(90 ,5,'',0,0);
//        $pdf->Cell(30 ,5,'sum'.$amount_sum,1,0);
//        $pdf->Cell(30 ,5,'',0,0);
//        $pdf->Cell(30 ,5,'commission sum',1,1);

    $posum[] = $real_data[$i]['PO_AMOUNT'];
    $despsum[] = $real_data[$i]['DESPATCH_AMOUNT'];
    $balsum[] = $real_data[$i]['BALANCED_AMOUNT'];


}

$po_sum = array_sum($posum);
$desp_sum = array_sum($despsum);
$bal_sum = array_sum($balsum);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(180 ,5,'Total Gross:- ',0,1);
$pdf->Cell(60 ,5,'Order Value:- '.number_format($po_sum),0,0);
$pdf->Cell(60 ,5,'Despatch Value:- '.number_format($desp_sum),0,0);
$pdf->Cell(60 ,5,'Balance Value:- '.number_format($bal_sum),0,0);


$pdf->Output();



?>