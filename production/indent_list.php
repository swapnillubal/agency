<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/7/18
 * Time: 10:19 PM
 */


include "config/config.php";
include "class/agency.php";

$obj = new agency();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <script src="../jquery-3.3.1.min.js"></script>
    <title>Indent</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Indent</h3>

                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addindent2">Add New</button>
                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="x_title">
                                    <h2>Sent Indents</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">


                                    <table id="datatable-responsive2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Indent No</th>
                                            <th>Indent Date</th>
                                            <th>PO No.</th>
                                            <th>PO Date</th>
                                            <th>Banker</th>
                                            <th>Manage</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $data1 = $obj->listingindent();
//                                                                                echo json_encode($data1);
                                        foreach ($data1 as $data){
                                            ?>
                                            <tr>
                                                <td><?php echo $data['customer']; ?></td>
                                                <td><?php echo $data['indent_no']; ?></td>
                                                <td><?php echo $data['indent_date']; ?></td>
                                                <td><?php echo $data['po_no']; ?></td>
                                                <td><?php echo $data['po_date']; ?></td>
                                                <td><?php echo $data['banker']; ?></td>

                                                <td>
                                                    <a href="#"  id="detail" data-id="<?php echo $data['indent_id']; ?>" class="btn bb1"><i class="fa fa-pencil"></i>
                                                        Edit</a>
                                                    <a href="#" id="bb1" data-id="<?php echo $data['indent_id']; ?>" class="btn"><i class="fa fa-trash"></i>
                                                        Delete</a>
                                                    <a href="#" id="cancel" data-id="<?php echo $data['indent_id']; ?>" class="btn"><i class="fa fa-times-circle"></i>
                                                        Cancel</a>
                                                </td>
                                            </tr>

                                        <?php } ?>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>

        </footer>
        <!-- /footer content -->
    </div>
</div>
<!--modal starts here-->
<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="../vendors/toastr/toastr.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>


<script>
    $('#datatable-responsive1').dataTable();
    $('#datatable-responsive2').dataTable();
</script>
<script>
    $(document).on("click", "#bb1", function (e)  {
        e.preventDefault();
        var val1 = $(this).data('id');
        var tr = $(this).closest('tr');

        if (val1 != "") {

            $.ajax({
                type: "POST",
                url: './adminapi/indent/delete_indent.php',
                data: ({idinfo: val1}),
                success: function (data) {
                    console.log(data);
                    if (data == "success") {

                        tr.remove();
                        toastr["success"]("Successfully Deleted Quotation to Principal Entry", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Deleting Enquiry Entry", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Deleting Enquiry missing", "Agency  Administrator");
        }
    });
</script>
<script>

    $(document).on("click", "#cancel", function ()  {
//        e.preventDefault();
        var val11 = $(this).data('id');
        var tr = $(this).closest('tr');
        if (val11 != "") {

            $.ajax({
                type: "POST",
                url: './adminapi/indent/cancel_indent.php',
                data: ({idinfo: val11}),
                success: function (data) {
                    console.log(data);
                    if (data == "success") {

                        tr.remove();
                        toastr["success"]("Successfully Cancelled Quotation to Principal Entry", "Agency Administrator");

                    } else {
                        toastr["error"]("Error in Cancelled Enquiry Entry", "Agency Administrator");
                    }
                },
                error: function () {
                }
            });
        } else {
            toastr["error"]("Error in Cancelling Enquiry missing", "Agency  Administrator");
        }
    });
</script>
<script>
    $(document).on("click", "#detail", function () {
        var myBookId = $(this).data('id');

        setTimeout(function () {
            window.location = './indent_detail.php?id=' + myBookId;
        }, 1);
    });
</script>
</body>
</html>

