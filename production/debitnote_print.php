<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/1/19
 * Time: 10:07 PM
 */

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');


$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);


$from=$_REQUEST['from'];
$to=$_REQUEST['to'];
$territory=$_REQUEST['territory'];
$current=$_REQUEST['rate'];
$invoice=$_REQUEST['invoice_no'];

$company = $obj->getcompany();
$comm = $obj->debitnotecommissionamount($from,$to,$territory,$current);

//$image=imagepath."logo.jpeg";

$to_date = new DateTime();
$to_date =$to_date->format('d.m.y');


$from_date = new DateTime($from);
$from_date=$from_date->format('d.m.y');


$date = new DateTime();
$cur_date=$date->format('d.m.y');


//echo json_encode($company);
//echo $from;
//echo $territory;
//echo $to;
//echo $current;
//echo $invoice;

//$data1 = $obj->actionpaper_report_print_new($id);

$current_date = date("Y.m.d");

$dataterr = $obj->listterritory1($territory);

//echo json_encode($dataterr);

$data2=$obj->listcompany1($company);
foreach ($company as $item2) {
    $comp_name = $item2['name'];
    $comp_address = $item2['address'];
    $comp_email = $item2['email'];
    $comp_website= $item2['website'];
    $comp_Tel= $item2['telephone'];
    $comp_fax= $item2['fax'];
    $comp_gst= $item2['gst'];
    $comp_pan= $item2['pan'];
    $comp_bank= $item2['bank_details'];
    $comp_ac= $item2['ac_no'];
    $comp_ifsc= $item2['ifsc'];

}

//echo json_encode($dataterr);
foreach ($dataterr as $terr){
    $terr_address = $terr['address'];
    $terr_gst = $terr['territory_gst'];
}

class PDF extends FPDF{




    // Page header
    public function Header()
    {



        // Logo
        $this->SetFont('Arial','B',20);

//Cell(width , height ,text,border ,end line , [align])
        $this->Cell(40 ,5,'',0,0);
        $this->Cell(150 ,8,$GLOBALS['comp_name'],0,1);
//        $this->Image($GLOBALS['image'],175,5,30,20);
        $this->SetFont('Arial','',10);
        $this->Cell(20 ,5,'',0,0);

        $this->Cell(190 ,5,$GLOBALS['comp_address'],0,1);

        if(strlen($GLOBALS['comp_email'])<30) {

            $this->Cell(60, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }else{
            $this->Cell(42, 5, '', 0, 0);

            $this->Cell(70, 5, 'Email: ' . $GLOBALS['comp_email'], 0, 1);
        }
        if(strlen($GLOBALS['comp_website'])<30){
            $this->Cell(60 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');
        }else{
            $this->Cell(40 ,5,'',0,0);

            $this->Cell(70 ,5,'Website: '.$GLOBALS['comp_website'],0,1,'L');

        }
        $strlen = strlen($GLOBALS['comp_fax']);
        if(0<$strlen && $strlen<20) {
            $this->Cell(42, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'] . ', Fax: ' . $GLOBALS['comp_fax'], 0, 1);
        }
        else{
            $this->Cell(62, 5, '', 0, 0);
            $this->Cell(100, 5, 'Tel: ' . $GLOBALS['comp_Tel'], 0, 1);
        }
        $this->Cell(180 ,5,'',0,1);

        $x = $this->GetX();
        $y = $this->GetY();

        $this->SetXY($x + 149, $y);

        $this->Line($x, $y, $x+180,$y);

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}


$pdf = new PDF('P','mm','A4');
$pdf->AddPage();

$pdf->SetFont('Times','B',18);
$pdf->Cell(180 ,6,'',0,1);
$pdf->Cell(180 ,8,"AGENCY COMMISSION INVOICE",1,1,'C');
$pdf->SetFont('Times','',11);

$pdf->Cell(90 ,6,"THK INDIA Pvt. Ltd.",1,0,'C');
$pdf->Cell(30 ,6,"Invoice no.",1,0,'R');
$pdf->Cell(60 ,6,$invoice,1,1);
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x+90,$y);

$pdf->Cell(30 ,6,"Date:",1,0,'R');
$pdf->Cell(60 ,6,$cur_date,1,1);
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x+90,$y);
$pdf->Cell(30 ,6,"Pan No.",1,0,'R');
$pdf->Cell(60 ,6,$comp_pan,1,1);
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x+90,$y);
$pdf->Cell(30 ,6,"GST No.",1,0,'R');
$pdf->Cell(60 ,6,$comp_gst,1,1);


$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x,$y-18);
$pdf->SetFont('Times','',10);

$pdf->MultiCell(90 ,6,$terr_address,1,'C');
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x,$y);
$pdf->Cell(90 ,6,"GST NO:- ".$terr_gst,1,0);
$pdf->Cell(90 ,6,'',1,1);
$pdf->Cell(10 ,6,"SR",1,0);
$pdf->Cell(120 ,6,"DESCRIPTION",1,0,'C');
$pdf->Cell(50 ,6,"TOTAL AMOUNT",1,1,'C');
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x,$y);
$pdf->MultiCell(10 ,5,"\n1\n\n\n\n\n\n",1,'L');
$x=$pdf->GetX();
$pdf->SetXY($x+10,$y);
//echo $territory;
if($territory == "PUN"){
//    echo 'test';
    $pdf->MultiCell(120 ,5,"\nCOMMISSION:- \nFOR THE PERIOD\n".$from_date." TO ".$to_date."\n\nCGST @9%\nSGST@9%",1,1);
}else{

    $pdf->MultiCell(120 ,5,"\nCOMMISSION:- \nFOR THE PERIOD\n".$from_date." TO ".$to_date."\n\n\nIGST @18%",1,1);
}

$x=$pdf->GetX();
$pdf->SetXY($x+130,$y);

$comm = (int)$comm;
if($territory == "PUN"){

    $sgst =round($comm*9/100);
    $cgst =round($comm*9/100);
    $grandtotal = $cgst+$sgst+$comm;

    $pdf->MultiCell(50 ,5,"INR\n\n".number_format($comm)."\n\n\n".number_format($sgst)."\n".number_format($cgst),1,'R');
}else{

    $gst =round($comm*18/100);
    $pdf->MultiCell(50 ,5,"INR\n\n".number_format($comm)."\n\n\n\n".number_format($gst),1,'R');
    $grandtotal = $gst+$comm;
}

$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x,$y);



$pdf->Cell(130 ,6,"Total",1,0,'R');
$pdf->Cell(50 ,6,number_format($grandtotal),1,1,'R');
$text_val =  $f->format($grandtotal);
$pdf->Cell(180 ,6,"",0,1,'L');
$pdf->Cell(180 ,6,"(Rupees ".$text_val.")",0,1,'L');
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->SetXY($x,$y);
$pdf->Ln(5);
$pdf->MultiCell(180 ,5,"BANK DETAILS\n".$comp_name."\n".$comp_bank,0,'C');
$pdf->Ln(2);
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->Ln(2);
$pdf->Line($x,$y,$x+180,$y);
$pdf->Cell(180 ,6,"A/C NO.".$comp_ac,0,1,'C');
$pdf->Cell(180 ,6,"IFSC NO.".$comp_ifsc,0,1,'C');

$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->Ln(2);

$pdf->Line($x,$y,$x+180,$y);


$pdf->Cell(170 ,6,"For :".$comp_name,0,1,'R');
$pdf->Ln(20);

$pdf->Cell(170 ,6,"Authorised Signatory",0,1,'R');
$pdf->Ln(5);
$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->Line($x,$y,$x,$y-97);
$pdf->Line($x+180,$y,$x+180,$y-97);

$x=$pdf->GetX();
$y=$pdf->GetY();
$pdf->Line($x,$y,$x+180,$y);

$pdf->Output();



?>