<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16/12/18
 * Time: 11:54 PM
 */
include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');



// if((isset($_REQUEST['date1']))|| (isset($_REQUEST['date2']))|| (isset($_REQUEST['territory']))||(isset($_REQUEST['customer']))){
// if($_REQUEST['date1']==""){$from='NULL';}else {$from=$_REQUEST['date1'];}
// if($_REQUEST['date2']==""){$to='NULL';}else {$to=$_REQUEST['date2'];}
//
// if($_REQUEST['territory']==""){$territory='';}else {$territory=$_REQUEST['territory'];}
// if($_REQUEST['customer']==""){$customer='';}else {$customer=$_REQUEST['customer'];}
// }





$from = $_REQUEST['date1'];
$to= $_REQUEST['date2'];
$territory= $_REQUEST['territory'];
$customer= $_REQUEST['customer'];
$company = "APEX PRECISION AGENCIES";
$supplier = "THK CO.,LTD";

$data1=$obj->invoicewisecommprint1($from,$to,$customer,$territory);




//}

class PDF extends FPDF{




    // Page header
    public function Header()
    {

        $this->SetFont('Arial','B',12);
        $this->Cell(80 ,5,'',0,1);
        $this->Cell(80 ,5,$GLOBALS['company'],0,0);
        $this->Cell(100 ,5,'INVOICEWISE COMMISSION STATEMENT',0,1,'R');
        $this->SetFont('Arial','',8);
//        $this->Cell(50 ,5,'NET :-',0,0,'L');
        $this->SetFont('Arial','B',10);
        $this->Cell(160 ,5,$GLOBALS['supplier'],0,1,'R');

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}




$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);


$pdf->SetFont('Arial','',8);
$pdf->Cell(20 ,5,'',0,1);


$pdf->Cell(90 ,5,'Item Description',1,0);
$pdf->Cell(30 ,5,'Amount',1,0);
$pdf->Cell(30 ,5,'Commission(%)',1,0);
$pdf->Cell(30 ,5,'Commission Amount',1,1);






 $invoice_var = $data1[0]['inv_no'];


 $pdf->Cell(45 ,5,'Invoice No.:'.$data1[0]['inv_no'],0,0);
 $pdf->Cell(45 ,5,'Invoice Date.:'.$data1[0]['invoice_date'],0,0);
 $pdf->Cell(90 ,5,'Customer:'.$data1[0]['customer'],0,1);
 $x = $pdf->GetX();
    $y = $pdf->GetY();
  $pdf->Line($x, $y, $x+180,$y);





foreach ($data1 as $real_data) {

  $commRate          = $real_data['commRate'];
  $despatch_value    = $real_data['despatch_VALUE'];
  $total_commission  = $real_data['Total_Commission'];


//global variable for total of all invoice

$despatch_value_grandtotal     += $despatch_value;
$total_commission_grandtotal   += $total_commission  ;



if  ($invoice_var == $real_data['inv_no']){

  $pdf->Cell(90 ,5,$real_data['item'],0,0);
           $pdf->Cell(30 ,5,number_format($real_data['despatch_VALUE']),0,0);
          $pdf->Cell(30 ,5,number_format($real_data['commRate']),0,0);
          $pdf->Cell(30 ,5,number_format($real_data['Total_Commission']),0,1);

          // echo $despatch_value;
          $despatch_value_total     += $despatch_value;
          $total_commission_total   += $total_commission  ;

      // $pdf->Cell(45 ,5,'Invoice No.:'.$real_data['inv_no'],0,0);
      // $pdf->Cell(45 ,5,'Invoice Date.:'.$real_data['invoice_date'],0,0);
      // $pdf->Cell(90 ,5,'Customer:'.$real_data['customer'],0,1);

}
else  {
  $x = $pdf->GetX();
     $y = $pdf->GetY();
   $pdf->Line($x, $y, $x+180,$y);
  $pdf->Cell(90 ,5,"Total",0,0);
 $pdf->Cell(30 ,5,number_format($despatch_value_total),0,0);
 $pdf->Cell(30 ,5,"",0,0);

$pdf->Cell(30 ,5,number_format($total_commission_total),0,1);
$x = $pdf->GetX();
   $y = $pdf->GetY();
 $pdf->Line($x, $y, $x+180,$y);
 $pdf->Line($x, $y+0.5, $x+180,$y+0.5);
$despatch_value_total = 0;
$total_commission_total = 0;
$pdf->Cell(45 ,5,'Invoice No.:'.$real_data['inv_no'],0,0);
$pdf->Cell(45 ,5,'Invoice Date.:'.$real_data['invoice_date'],0,0);
$pdf->Cell(90 ,5,'Customer:'.$real_data['customer'],0,1);
$x = $pdf->GetX();
   $y = $pdf->GetY();
 $pdf->Line($x, $y, $x+180,$y);

$pdf->Cell(90 ,5,$real_data['item'],0,0);
         $pdf->Cell(30 ,5,number_format($real_data['despatch_VALUE']),0,0);
        $pdf->Cell(30 ,5,number_format($real_data['commRate']),0,0);
        $pdf->Cell(30 ,5,number_format($real_data['Total_Commission']),0,1);
        $despatch_value_total     += $despatch_value;
        $total_commission_total   += $total_commission;

        $invoice_var          = $real_data['inv_no'];


}



}


$x = $pdf->GetX();
   $y = $pdf->GetY();
 $pdf->Line($x, $y, $x+180,$y);
$pdf->Cell(90 ,5,"Total",0,0);
$pdf->Cell(30 ,5,number_format($despatch_value_total),0,0);
$pdf->Cell(30 ,5,"",0,0);

$pdf->Cell(30 ,5,number_format($total_commission_total),0,1);




$pdf->SetFont('Arial','B',14);
$x = $pdf->GetX();
   $y = $pdf->GetY();
$pdf->Line($x, $y, $x+180,$y);
 $pdf->Line($x, $y+1, $x+180,$y+1);
 $x = $pdf->GetX();
    $y = $pdf->GetY();
    $pdf->Cell(90 ,5,"",0,1);
$pdf->Cell(90 ,5,"Grand Total",0,0);
$pdf->Cell(30 ,5,number_format($despatch_value_grandtotal),0,0);
$pdf->Cell(30 ,5,"",0,0);

$pdf->Cell(30 ,5,number_format($total_commission_grandtotal),0,1);



$pdf->Output();



?>
