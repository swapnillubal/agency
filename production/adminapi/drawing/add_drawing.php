<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/7/18
 * Time: 11:00 AM
 */



include "../../config/config.php";
include "../../class/agency.php";

$obj = new agency();

$enq_no = $_REQUEST['enq_no'];
$item = $_REQUEST['item_name'];

if (is_uploaded_file($_FILES['report']['tmp_name'])) {
    $filename = basename($_FILES['report']['name']);
    $imagename = $obj->getdatetime();
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    $sourcePath = $_FILES['report']['tmp_name'];
    $targetPath = "../../file_upload/drawings/" . $imagename . "." . $ext;
    $report = imagepath."drawings/" . $imagename . "." . $ext;
    move_uploaded_file($sourcePath, $targetPath);


    if ($enq_no != "") {
        $data = $obj->adddrawing($enq_no,$item,$report);

        if ($data == true) {
            echo "success";
        } else {
            echo "Opps! Something Went Wrong, Please try again after sometime";
        }
    } else {
        echo "Opps! Something Went Wrong";
    }
}
