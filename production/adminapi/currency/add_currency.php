<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30/10/18
 * Time: 10:42 PM
 */




include "../../config/config.php";
include "../../class/agency.php";

$obj = new agency();

$currency_code = $_REQUEST['currency_code'];
$currency_name = $_REQUEST['currency_name'];
$symbol = $_REQUEST['currency_symbol'];

if($currency_code != "") {
    $data = $obj->addcurrency($currency_code,$currency_name,$symbol);

    if ($data == "true") {
        echo "success";
    } else {
        echo "Opps! Something Went Wrong, Please try again after sometime";
    }
} else {
    echo "Opps! Something Went Wrong";
}