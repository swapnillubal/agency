<?php
/**
 * Created by PhpStorm.
 * User: swapnil
 * Date: 23/5/18
 * Time: 4:05 AM
 */?>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="index.html">Dashboard</a></li>
                    <li><a href="index2.html">Dashboard2</a></li>
                    <li><a href="index3.html">Dashboard3</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> Masters <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="./company_master.php">Company</a></li>
                    <li><a href="./principal_master.php">Supplier</a></li>
                    <li><a href="./customer_master.php">Customer</a></li>
                    <li><a href="./territory_master.php">Territory</a></li>
                    <li><a href="./product_master.php">Product</a></li>
                    <li><a href="./currency_master.php">Currency</a></li>
<!--                    <li><a href="./country_master.php">Country</a></li>-->
                    <li><a href="./make_master.php">Make</a></li>
                    <li><a href="./subgroup_master.php">Subgroup</a></li>
                    <li><a href="./signature_master.php">Signature</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> New <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="./new_enquiry.php">New Enquiry</a></li>
                    <li><a href="./drawing.php">Drawing</a></li>

                    <li><a href="./enq_principal.php">Send Enquiry to Principal</a></li>
                    <li><a href="./quotation_from_principal.php">Quotation from Principal</a></li>
                    <li><a href="./quotation_customer.php">Quotation to Customer</a></li>
                    <li><a href="./purchase_order.php">Purchase Order</a></li>
                    <li><a href="./indent.php">Indent</a></li>
                    <li><a href="./OC_principal.php">Order Confirmation from Principal</a></li>
                    <li><a href="./OC_customer.php">Order Confirmation to Customer</a></li>
                    <li><a href="./lc_details.php">LC/Payment Details</a></li>
                    <li><a href="./despatch.php">Despatch Details</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> Modify <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="./enq_list.php">Enquiry</a></li>
                    <li><a href="./quotationprinci_list.php">Quotation From principal</a></li>
                    <li><a href="./quotationcustomer_list.php">Quotation to Customer</a></li>
                    <li><a href="./purchase_order_list.php">Purchase Order</a></li>
                    <li><a href="./indent_list.php">Indent</a></li>
                    <li><a href="./oc_list_princi.php">Order Confirmation From Principal</a></li>
                    <li><a href="./oc_list_customer.php">Order Confirmation To Customer</a></li>
                    <li><a href="./lcdetails_list.php">LC/Payment</a></li>
                    <li><a href="./despatch_list.php">Despatch Details</a></li>

                </ul>
            </li>
<!--            <li><a><i class="fa fa-table"></i> Print <span class="fa fa-chevron-down"></span></a>-->
<!--                <ul class="nav child_menu">-->
<!--                    <li><a href="#">Enquiry</a></li>-->
<!--                    <li><a href="#">Quotation to customer</a></li>-->
<!--                    <li><a href="#">Indent</a></li>-->
<!--                    <li><a href="#">Order confirmation to customer</a></li>-->
<!---->
<!--                </ul>-->
<!--            </li>-->
            <li><a><i class="fa fa-desktop"></i> Reports <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="./consolidated.php">Consolidated Report</a></li>
                    <li><a href="./qfrompreport.php">Quotation from Principal</a></li>
                    <li><a href="./qtocreport.php">Quotation to customer</a></li>
                    <li><a href="./quotationitems.php">Quoted Items</a></li>
                    <li><a href="./poreport.php">Purchase Order</a></li>
                    <li><a href="./poitemreport.php">Purchase Order Items</a></li>
                    <li><a href="./despatchreport.php">Despatch</a></li>
                    <li><a href="./despatchitemreport.php">Despatch Items</a></li>
<!--                    <li><a href="#">Quotation from principal</a></li>-->
<!--                    <li><a href="#">Follow up</a></li>-->
                    <li><a href="./action_paper.php">Action paper</a></li>
                    <!--              seperation          -->
                    <li><a href="./daily_performance_report.php">Daily Performance</a></li>
<!--                    <li><a href="#">Monthly Reports</a></li>-->
                    <li><a href="./commission_statement.php">Commission Statement</a></li>
                    <li><a href="./invoicewisereport.php">Invoicewise Commission</a></li>
<!--                    <li><a href="./indentreport.php">Indent Register</a></li>-->
                    <li><a href="./annual_report.php">Annual Report of Order & Despatch</a></li>
                    <li><a href="./debitnote.php">Debit Note and Distributors Commission</a></li>
                    <li><a href="./detailedorder.php">Detailed Order Status</a></li>
                    <li><a href="./pendingorder.php">Pending Order Status</a></li>
                    <li><a href="./cancelledorder.php">Cancelled Order</a></li>



                </ul>
            </li>

            <!--    <div class="menu_section">-->
            <!--        <h3>Live On</h3>-->
            <!--        <ul class="nav side-menu">-->
            <!--            <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>-->
            <!--                <ul class="nav child_menu">-->
            <!--                    <li><a href="e_commerce.html">E-commerce</a></li>-->
            <!--                    <li><a href="projects.html">Projects</a></li>-->
            <!--                    <li><a href="project_detail.html">Project Detail</a></li>-->
            <!--                    <li><a href="contacts.html">Contacts</a></li>-->
            <!--                    <li><a href="profile.html">Profile</a></li>-->
            <!--                </ul>-->
            <!--           </li>-->
            <!--            <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>-->
            <!--                <ul class="nav child_menu">-->
            <!---->
            <!---->
            <!--                    <li><a href="./company_master.php">Enquiry</a>-->
            <!--                    <li><a href="./principal_master.php">Enquiry to principal</a>-->
            <!--                    <li><a href="./customer_master.php">Customer</a>-->
            <!--                    <li><a href="./product_master.php">Product</a>-->
            <!---->
            <!--                </ul>-->
            <!--            </li>-->


        </ul>
    </div>

</div>
