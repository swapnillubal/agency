<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/8/18
 * Time: 12:09 AM
 */

include "config/config.php";
include "section/checksession.php";
include "class/agency.php";

$obj = new agency();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Principal Tables</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet" media="screen">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <?php

                include "./section/logosection.php";
                ?>

                <div class="clearfix"></div>



                <br />

                <!-- sidebar menu -->
                <?php

                include "./section/sidebar.php";
                ?>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <?php
        include "./section/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Principal</h3>

                    </div>


                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">
                                <?php

                                $currency_name=$obj->listcurrency();


                                $principalid = $_REQUEST['id'];

                                $data1 = $obj->principaldetails($principalid);
//                                                                echo json_encode($data1);
                                foreach ($data1 as $data ){ ?>
                                    <form id="editprincipal" name="editprincipal"  method="POST" class="form-horizontal form-label-left" novalidate>
                                        <input name="qid" id="qid" value="<?php echo $data['p_id']; ?>"
                                               hidden="hidden">
                                        <span class="section">Information</span>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="name" class="form-control col-md-7 col-xs-12" name="name" placeholder="Name" required="required" type="text" value="<?php echo $data['name']; ?>">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Address">Address <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Address" required="required" name="Address" class="form-control col-md-7 col-xs-12"><?php echo $data['address']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Tel">Tel No.<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Tel" class="form-control col-md-7 col-xs-12" name="Tel" type="text" value="<?php echo $data['Tel']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Fax">Fax <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Fax" class="form-control col-md-7 col-xs-12" name="Fax" type="text" value="<?php echo $data['Fax']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> Email <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email" name="email" data-validate-linked="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $data['email']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name_person">Name of person<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Name_person" class="form-control col-md-7 col-xs-12" name="Name_person"  type="text" value="<?php echo $data['Name_person']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Department">Department<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Department" class="form-control col-md-7 col-xs-12" name="Department" type="text" value="<?php echo $data['Department']; ?>">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Banker_Details">Banker Details <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Banker_Details" required="required" name="Banker_Details" class="form-control col-md-7 col-xs-12"><?php echo $data['Banker_Details']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Currency">Currency <span class="required">*</span>
                                            </label>
                                            <select class="col-md-6 col-sm-6 col-xs-12" name="Currency">
                                                <?php
                                                foreach ($currency_name as $curr_name){
                                                    ?>
                                                    <option
                                                    <?php if ($curr_name['CURRENCY_CODE'] == $data['Currency'] ) echo 'selected' ;?>
                                                            value="<?php echo $curr_name['CURRENCY_CODE']; ?>"><?php echo $curr_name['CURRENCY_CODE'].'-'.$curr_name['CURRENCY_NAME'].'-'.$curr_name['CURRENCY_SYMBOL']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Note_Q">Note used on Quotation <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Note_Q" required="required" name="Note_Q" class="form-control col-md-7 col-xs-12"><?php echo $data['Note_Q']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Note_OC">Note used on O.C <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Note_OC" required="required" name="Note_OC" class="form-control col-md-7 col-xs-12"><?php echo $data['Note_OC']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Conditions_OC">Conditions used on O.C <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="Conditions_OC" required="required" name="Conditions_OC" class="form-control col-md-7 col-xs-12"><?php echo $data['Conditions_OC']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Country">Country <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Country" class="form-control col-md-7 col-xs-12" name="Country" type="text" value="<?php echo $data['Country']; ?>">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Decimal">Decimal <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="Decimal" class="form-control col-md-7 col-xs-12" name="Decimal" type="text" value="<?php echo $data['Decimal1']; ?>">
                                            </div>
                                        </div>


                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button type="submit" class="btn btn-primary">Cancel</button>
                                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<script src="../vendors/toastr/toastr.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>
<!--<script src="../jquery-3.3.1.min.js"></script>-->


<script>
    $("#editprincipal").on('submit', (function (e) {
        var form = document.getElementById("editprincipal");
        e.preventDefault();

        $.ajax({

            url: "./adminapi/principal/edit_principal.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data != "success") {
                    toastr["success"]("Successfully Updated principal", "Agency Administrator");
                    form.reset();
                    setTimeout(function () {
                        window.location = './principal_master.php';
                    }, 2000);
                } else {
                    toastr["error"](data, "Agency Administrator");
                }
            },
            error: function () {
            }
        });
    }));
</script>

</body>
</html>


