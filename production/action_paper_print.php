<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/1/19
 * Time: 10:45 AM
 */

include "config/config.php";
include "class/agency.php";

$obj = new agency();

require('./fpdf/fpdf.php');

$id=$_REQUEST['id'];
//echo $id;



//$data1 = $obj->actionpaper_report_print($id);
$data1 = $obj->actionpaper_report_print_new($id);
$calculation = $obj->actionpaper_calculation($id);

$invoice_details = $obj->actionpaper_invoice_details1($id);

// echo json_encode($invoice_details);

$po_sum =array_sum($calculation['po_amount']);
$despatch_sum =array_sum($calculation['despatch_amount']);
$comm_sum =array_sum($calculation['comm_amount']);

//
//foreach ($calculation as $cal){
//    echo $cal['PO_AMOUNT'];
//}

//for($i=0;$i<count($data1);$i++){
//    $sum[]=($data1[$i]['quantity'])*($data1[$i]['net_value']);
////    $value[]=($data1[$i]['net_value']);
////    echo json_encode($sum);
//    $real_sum=array_sum($sum);
//    echo $real_sum;
////    echo json_encode($value);
//}

$current_date = date("Y.m.d");

class PDF extends FPDF{




    // Page header
    public function Header()
    {



        // Logo
        $this->SetFont('Arial','B',20);



        $this->Cell(180 ,8,'Action Paper',0,1, 'C');
//$pdf->Cell(20 ,8,'',1,0);
        $this->SetFont('Arial','',12);

        $this->Cell(180 ,5,'Date:- '.$GLOBALS['current_date'].'     '.'Time:-  '.date("H:i:s"),0,1,'C');


    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);

        // Arial italic 8
        $this->SetFont('Arial','I',8);

        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
}


$pdf = new PDF('P','mm','A4');
$pdf->AddPage();

//$pdf->Line(10, 55, 195,55);



$pdf->SetFont('Times','',13);
//$x=$pdf->GetX();
//$y=$pdf->GetY();
//$pdf->SetXY($x,$y);
$pdf->Cell(180 ,5,'',0,1);
foreach ($data1 as $data){
    $data['actual_receipt_date'];
    $enq_date=new DateTime($data['date']);
    $enq_date = $enq_date->format('d.m.y');
    $princi_q_date=new DateTime($data['principal_qtn_ref_date']);
    $princi_q_date= $princi_q_date->format('d.m.y');
    $q_date=new DateTime($data['q_date']);
    $q_date=$q_date->format('d.m.y');
    $po_date=new DateTime($data['po_date']);
    $po_date=$po_date->format('d.m.y');
//    echo $po_receipt_date;
    if(!is_null($data['actual_receipt_date'])){
        $po_receipt_date = new DateTime($data['actual_receipt_date']);
        $po_receipt_date=$po_receipt_date->format('d.m.y');
    }
    if(!is_null($data['intimation_date'])){
        $po_intimation_date1=new DateTime($data['intimation_date']);
        $po_intimation_date=$po_intimation_date1->format('d.m.y');
    }
    if(!is_null($data['indent_date'])){
        $indent_date = new DateTime($data['indent_date']);
        $indent_date = $indent_date->format('d.m.y');
    }
    if(!is_null($data['order_date'])){
        $oc_date = new DateTime($data['order_date']);
        $oc_date=$oc_date->format('d.m.y');
    }
    if(!is_null($data['our_ref_date'])){
        $our_ref_date = new DateTime($data['our_ref_date']);
        $our_ref_date=$our_ref_date->format('d.m.y');
    }
    if(!is_null($data['invoice_date'])){
        $invoice_date = new DateTime($data['invoice_date']);
        $invoice_date=$invoice_date->format('d.m.y');
    }
    if(!is_null($data['expiry_date'])  && $data['expiry_date']!='0000-00-00' ){
        $payment_expiry_date=new DateTime($data['expiry_date']);
        $payment_expiry_date=$payment_expiry_date->format('d.m.y');
    }
    if(!is_null($data['etd_1'])){
        $etd_1=new DateTime($data['etd_1']);
        $etd_1=$etd_1->format('d.m.y');
    }
    if(!is_null($data['etd_2'])) {
        $etd_2 = new DateTime($data['etd_2']);
        $etd_2 = $etd_2->format('d.m.y');
    }

    if(!is_null($data['etd_3'])) {
        $etd_3 = new DateTime($data['etd_3']);
        $etd_3 = $etd_3->format('d.m.y');
    }


    $desp = $data['DESPATCH_AMOUNT'];
    $comm = $data['COMMISSION_AMOUNT'];

    $desp = (int)$desp;
    $comm = (int)$comm;
//    echo $desp;
//    echo $comm;
    if(!empty($desp) && !empty($comm)) {
        $compercentage = round(($comm * 100) / $desp);
    }

    $pdf->Cell(180 ,7,'Customer :-'.$data['name'],0,1);
    $pdf->Cell(100 ,7,'Territory :-'.$data['Territory'],0,0);
    $pdf->Cell(90 ,7,'Currency :-'.$data['Currency'],0,1);
    $pdf->Cell(190 ,7,'',0,1);

    $pdf->Cell(70 ,8,'Enquiry No. :',1,0);
    $pdf->Cell(90 ,8,$data['enq_no'].'  Dt: '.$enq_date,1,1);
    $pdf->Cell(70 ,8,'Receipt Date of Enquiry :',1,0);
    $pdf->Cell(90 ,8,$enq_date,1,1);
    $pdf->Cell(70 ,8,'Sent Date:',1,0);
    $pdf->Cell(90 ,8,'',1,1);
    $pdf->Cell(70 ,8,'Principal Qtn No.:',1,0);
    $pdf->Cell(90 ,8,$data['principal_Qtn_Ref'].' Dt: '.$princi_q_date,1,1);
    $pdf->Cell(70 ,8,'Qtn No.:',1,0);
    $pdf->Cell(90 ,8,$data['quotation_no'].' Dt: '.$q_date,1,1);
    $pdf->Cell(70 ,8,'PO No.:',1,0);
    $pdf->Cell(90 ,8,$data['po_no'].' Dt:'.$po_date,1,1);
    $pdf->Cell(70 ,8,'Receipt Date of PO:',1,0);
    $pdf->Cell(90 ,8,$po_receipt_date,1,1);
    $pdf->Cell(70 ,8,'Intimation to principal:',1,0);
    $pdf->Cell(90 ,8,$po_intimation_date,1,1);
    $pdf->Cell(70 ,8,'Indent No.:',1,0);
    $pdf->Cell(90 ,8,$data['indent_no'].' Dt: '.$indent_date,1,1);
    $pdf->Cell(70 ,8,'Manufacturing starts:',1,0);
    $pdf->Cell(90 ,8,$data['start_production'],1,1);
    $pdf->Cell(70 ,8,'O.A No.:',1,0);
    $pdf->Cell(90 ,8,$data['order_no'].' Dt: '.$oc_date,1,1);
    $pdf->Cell(70 ,8,'O.C No.:',1,0);
    $pdf->Cell(90 ,8,$data['our_ref_no'].' Dt: '.$our_ref_date,1,1);
    $pdf->Cell(40 ,8,'ETD:',1,0);
    $pdf->Cell(40 ,8,$etd_1,1,0);
    $pdf->Cell(40 ,8,$etd_2,1,0);
    $pdf->Cell(40 ,8,$etd_3,1,1);
    $pdf->Cell(70 ,8,'Payment details:',1,0);
    $pdf->Cell(90 ,8,'',1,1);
    $pdf->Cell(70 ,8,'Payment Expiry:',1,0);
    $pdf->Cell(90 ,8,$payment_expiry_date,1,1);
    $pdf->Cell(70 ,8,'Payment value:',1,0);
    $pdf->Cell(90 ,8,number_format($data['value'],2),1,1);
    $pdf->Cell(70 ,8,'P.O. Amt.:',1,0);
    $pdf->Cell(90 ,8,number_format($po_sum,2),1,1);


    foreach ($invoice_details as $inv_d) {
      // code...


      if(!is_null($inv_d['invoice_date'])){
          $invoice_date1 = new DateTime($inv_d['invoice_date']);
          $invoice_date1=$invoice_date1->format('d.m.y');
      }
    $pdf->Cell(70 ,8,'Invoice No:',1,0);
    $pdf->Cell(90 ,8,$inv_d['invoice_no'].' Dt: '.$invoice_date1,1,1);
    $pdf->Cell(70 ,8,'Despatch Amt.:',1,0);
    $pdf->Cell(90 ,8,number_format($inv_d['DESPATCH_AMOUNT'],2),1,1);
    $pdf->Cell(70 ,8,'Commission Percentage: '.$compercentage.'%',1,0);
    $pdf->Cell(90 ,8,'Comm. Amt.: '.number_format($inv_d['COMMISSION_AMOUNT'],2),1,1);
    }
//    $pdf->Cell(40 ,7,'ETD:',1,0);
//    $pdf->Cell(40 ,7,$etd_1,1,0);
//    $pdf->Cell(40 ,7,$etd_2,1,0);
//    $pdf->Cell(40 ,7,$etd_3,1,1);
}

//$pdf->AddPage();

$pdf->Output();



?>
